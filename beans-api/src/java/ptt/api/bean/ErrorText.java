package ptt.api.bean;

/**
 * 
 * @author Danil Krivopustov 
 *
 */
public interface ErrorText {

	public String AccessForbidden();
	
	public String CommonError();
	
	public String AccessViolation();
	
}
