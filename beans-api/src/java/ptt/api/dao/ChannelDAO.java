package ptt.api.dao;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.Ticket;

import ptt.api.net.slice.exceptions.ChannelNotExistsException;
import ptt.api.net.slice.exceptions.BadChannelDataException;
import ptt.api.net.slice.exceptions.AccessDeniedException;
import ptt.api.net.slice.exceptions.ChannelNameExistsException;

/**
 * 
 * @author Danil Krivopustov 
 * 
 */
/**
 * Data access object for channel domain
 */
public interface ChannelDAO extends GenericDAO {

  /**
   * Checks if password required
   */
  boolean isPasswordRequired(int channelId);

  /**
   * Checks if user is channel owner
   */
  boolean isChannelOwner(int channelId, int id) throws ChannelNotExistsException ;

  /**
   * Authorizes on channel
   */
  boolean authorize(int channelId, String password);

  /**
   * Creates new channel
   */
  int create(ChannelData info) throws BadChannelDataException, ChannelNameExistsException;

  /**
   * Checks if channel name is unique
   */ 
  boolean isUniq(String name);

  /**
   * Deletes user
   */
  boolean remove(int channelId, Ticket sid) throws ChannelNotExistsException, AccessDeniedException;

  /**
   * Gets users channels
   */
  List<ChannelData> get(Ticket sid);

  /**
   * Gets users channels by keyword
   */
  List<ChannelData> getAll(String keyword, Ticket sid);

  /**
   * Gets all channels by keyword
   */
  List<ChannelData> getAll(String keyword);

  /**
   * Subscribers user
   */
  boolean subscribe(int channelId, Ticket sid) throws ChannelNotExistsException, AccessDeniedException;

  /**
   * Unsubcribes user
   */
  boolean unsubscribe(int channelId, Ticket sid) throws ChannelNotExistsException;
  
  /**
   * Changes password
   */
  void change(ChannelData info) throws AccessDeniedException, ChannelNotExistsException, BadChannelDataException;

  /**
   * Mutes and unmutes channel
   */
  void mute(int channelId, Ticket sid, int state) throws ChannelNotExistsException;
  
  /**
   * Gets info about channel
   */
  ChannelData get(int channelId);

  /**
   * Gets users by channel id
   */
  List<UserInfo> getUsers(int channelId);
  /**
   * Gets info about channel by ticket
   */
  ChannelData get(int channelId, Ticket sid);

  /**
   * Checks if user can send messages
   */
  boolean canISpeak(int channelId, Ticket sid) throws ChannelNotExistsException;
}
