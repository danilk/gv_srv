package ptt.api.dao;

import com.ibatis.sqlmap.client.SqlMapClient;


/**
 * 
 * @author Danil Krivopustov
 * 
 */

/**
 * Common interface for all data access objects
 */
public interface GenericDAO {

  /**
   * Gets sql map client instance
   */
	SqlMapClient getSqlMapClient();

  /**
   * Sets sql map client instance
   */
	void setSqlMapClient(SqlMapClient value);
}
