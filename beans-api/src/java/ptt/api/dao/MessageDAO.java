package ptt.api.dao;

import java.util.List;

import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.Message;
/**
 * 
 * @author Danil Krivopustov 
 * 
 */
/**
 * Data access object for order domain
 */
public interface MessageDAO extends GenericDAO {
  /**
   * Gives free and interested messages for user 
   * @param ticket - current peasant mark
   * @return list of order instances 
   */
	List<Message> getMessages(Ticket ticket);
}
