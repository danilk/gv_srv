package ptt.api.dao;

import ptt.api.net.slice.Credential;
import ptt.api.net.slice.Ticket;

/**
 * 
 * @author Danil Krivopustov 
 * 
 */
/**
 * Data access object for ticket domain
 */
public interface TicketDAO extends GenericDAO {

	/**
	 * Authorizes peasant in the system and gives him the Ticket
   * can be callled several times 
	 * 
	 * @param Credential
   *          credetial info of peasant
   *
	 * @return Ticket for good peasants and null otherwise
	 */
	Ticket getTicketByCredential(Credential credentialInfo);
	
  /**
   * Removes the Ticket from the System
   * Should be used when peasant closes the Client and doesn't want get any notification from the System
   *
   * @param Ticket
   *         just the Ticket
   *
   * @return true if ticket was droppped and false otherise
   */
  boolean dropTicket(Ticket ticket);
	
  /**
   * Validates the Ticket
   *
   * @param ticket 
   *         just the Ticket
   *
   * @return true if ticket is valid
   */
	boolean isTicketValid(Ticket ticket);
	
	void refresh(Ticket ticket);

  /**
   * Sets identity with ticket
   */
  void setIdentity(Ticket ticket, String ident, String userIdent);

  /**
   * Gets identity by ticket
   */
  String getChannelIdentity(Ticket ticket);

  /**
   * Gets identity by ticket
   */
  String getUserIdentity(Ticket ticket);

  int getUserId(Ticket ticket);
}
