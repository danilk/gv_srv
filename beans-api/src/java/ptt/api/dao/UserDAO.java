package ptt.api.dao;

import java.lang.Integer;
import java.util.List;
import java.lang.String;

import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;

import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.slice.exceptions.LoginExistsException;
import ptt.api.net.slice.exceptions.BadUserInfoException;
/**
 * 
 * @author Danil Krivopustov 
 * 
 */
/**
 * Data access object for user domain
 */
public interface UserDAO extends GenericDAO {

  /**
   * Checks if user name is unique
   */ 
  boolean isLoginUniq(String userName);

  /**
   * Creates new user with given info
   */
  void create(UserInfo info) throws LoginExistsException, BadUserInfoException; 

  /**
   * Deletes user
   */
  void remove(int userId) throws UserNotExistsException;

  /**
   * Gets info
   */
  UserInfo get(Ticket sid) throws UserNotExistsException;

  /**
   * Gets info
   */
  UserInfo getUser(int id) throws UserNotExistsException;

  /**
   * Gets user contact list
   */
  List<UserInfo> getContacts(Ticket sid) throws UserNotExistsException;

  /**
   * Searches users
   */
  List<UserInfo> searchUser(String keyword);

 /**
  * Adds user to contacts
  */
  void addToContacts(Ticket sid, int id);

  /**
   * Deletes user from contacts
   */
  void deleteFromContacts(Ticket sid, int id);

  /**
   * Changes password
   */
  void changePassword(UserInfo info) throws BadUserInfoException, UserNotExistsException;

  /**
   * Changes info without password
   */
  void changeInfo(UserInfo info) throws BadUserInfoException, UserNotExistsException;

  /**
   * Change info 
   */
  void change(UserInfo info) throws BadUserInfoException, UserNotExistsException;

  /**
   * Mutes and unmutes channel
   */
  void mute(int userId, Ticket sid, int state) throws UserNotExistsException;
}
