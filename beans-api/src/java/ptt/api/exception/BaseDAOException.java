package ptt.api.exception;

/**
 * 
 * @author Danil Krivopustov 
 * 
 */
/**
 * Supper class for all exception that throws in data access objects
 */
public class BaseDAOException extends RuntimeException {

	private static final long serialVersionUID = 1228700150622746532L;

	public BaseDAOException() {
		super();
	}

	public BaseDAOException(String message) {
		super(message);
	}

	public BaseDAOException(Throwable cause) {
		super(cause);
	}

	public BaseDAOException(String message, Throwable cause) {
		super(message, cause);
	}
}
