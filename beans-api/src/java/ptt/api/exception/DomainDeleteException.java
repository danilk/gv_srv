package ptt.api.exception;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public class DomainDeleteException extends BaseException {

	private static final long serialVersionUID = 7422006128003836887L;

	public DomainDeleteException() {
		super();
	}

	public DomainDeleteException(String message) {
		super(message);
	}

	public DomainDeleteException(Throwable cause) {
		super(cause);
	}

	public DomainDeleteException(String message, Throwable cause) {
		super(message, cause);
	}
}
