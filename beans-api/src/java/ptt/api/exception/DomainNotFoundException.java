package ptt.api.exception;

/**
 * 
 * @author Danil Krivopustov 
 * 
 */
/**
 * This exception is threw if domain is not found in database
 */
public class DomainNotFoundException extends BaseException {

	private static final long serialVersionUID = -8606146204754376593L;

	public DomainNotFoundException() {
		super();
	}

	public DomainNotFoundException(String message) {
		super(message);
	}

	public DomainNotFoundException(Throwable cause) {
		super(cause);
	}

	public DomainNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
