package ptt.api.exception;

/**
 * 
 * @author Danil Krivopustov
 * 
 */

/**
 * This is exception type that throws when record can not be created in database
 */
public class RecordIsNotCreated extends BaseException {

	private static final long serialVersionUID = -6993714242971367470L;

	public RecordIsNotCreated() {
		super();
	}

	public RecordIsNotCreated(String message) {
		super(message);
	}

	public RecordIsNotCreated(Throwable cause) {
		super(cause);
	}

	public RecordIsNotCreated(String message, Throwable cause) {
		super(message, cause);
	}
}
