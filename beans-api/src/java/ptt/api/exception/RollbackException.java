package ptt.api.exception;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public class RollbackException extends BaseException {

	private static final long serialVersionUID = 7421151949115540586L;

	public RollbackException() {
		super();
	}

	public RollbackException(String message) {
		super(message);
	}

	public RollbackException(Throwable cause) {
		super(cause);
	}

	public RollbackException(String message, Throwable cause) {
		super(message, cause);
	}
}
