package ptt.api.exception;

/**
 * 
 * @author Danil Krivoustov
 * 
 */
public class TransactionRollbackException extends RuntimeException {

	private static final long serialVersionUID = 5016266549820659982L;

	public TransactionRollbackException() {
		super();
	}

	public TransactionRollbackException(String message) {
		super(message);
	}

	public TransactionRollbackException(Throwable cause) {
		super(cause);
	}

	public TransactionRollbackException(String message, Throwable cause) {
		super(message, cause);
	}
}
