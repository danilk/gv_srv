package ptt.api.manager;

import java.lang.String;
import java.util.List;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.Ticket;

import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.slice.exceptions.LoginExistsException;
import ptt.api.net.slice.exceptions.BadUserInfoException;
import ptt.api.net.slice.exceptions.ChannelNotExistsException;
import ptt.api.net.slice.exceptions.BadChannelDataException;
import ptt.api.net.slice.exceptions.AccessDeniedException;
import ptt.api.net.slice.exceptions.ChannelNameExistsException;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public interface IChannelKeeper {


  /**
   * Gets repeat interval from config
   */
  int getRepeatInterval();
  
  String getFilepath();

  /**
   * Checks if password required
   */
  boolean IsPasswordRequired(int channelId);

  /**
   * Checks if user is channel owner
   */
  boolean IsChannelOwner(int channelId, Ticket sid) throws UserNotExistsException, ChannelNotExistsException;

  /**
   * Authorizes on channel
   */
  boolean Authorize(int channelId, String password);

  /**
   * Creates new channel
   */
  int Create(ChannelData info, Ticket sid) throws UserNotExistsException, BadChannelDataException, ChannelNameExistsException;

  /**
   * Checks if channel name is unique
   */ 
  boolean IsUniq(String name);
                  
  /**
   * Checks if user can send messages
   */
  boolean CanISpeak(int channelId, Ticket sid) throws ChannelNotExistsException;

  /**
   * Deletes user
   */
  boolean Remove(int channelId, Ticket sid) throws ChannelNotExistsException, AccessDeniedException;

  /**
   * Gets users channels
   */
  List<ChannelData> Get(Ticket sid);

  /**
   * Gets users channels by keyword
   */
  List<ChannelData> GetAll(String keyword, Ticket sid);

  /**
   * Gets all channels by keyword
   */
  List<ChannelData> GetAll(String keyword);
                  
  /**
   * Gets info by id
   */
  ChannelData Get(int channelId);

  /**
   * Gets users by channel id
   */
  List<UserInfo> GetUsers(int channelId);

  /**
   * Gets info by id and ticket
   */
  ChannelData Get(int channelId, Ticket sid);
  /**
   * Subscribers user
   */
  boolean Subscribe(int channelId, Ticket sid) throws ChannelNotExistsException, AccessDeniedException;

  /**
   * Unsubcribes user
   */
  boolean Unsubscribe(int channelId, Ticket sid) throws ChannelNotExistsException;
  
  /**
   * Mutes and unmutes channel
   */
  void Mute(int channelId, Ticket sid, int state) throws ChannelNotExistsException;
  
  /**
   * Changes password
   */
  void Change(ChannelData info, Ticket sid) throws UserNotExistsException, AccessDeniedException, ChannelNotExistsException, BadChannelDataException;
}
