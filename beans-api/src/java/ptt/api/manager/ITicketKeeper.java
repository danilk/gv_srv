package ptt.api.manager;

import ptt.api.net.slice.Credential;
import ptt.api.net.slice.Ticket;
import java.lang.String;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public interface ITicketKeeper {

	/**
	 * Method Authorizes user in the system and generates ticket
	 * 
	 * @return result of the operation
	 */
	Ticket Get(Credential credentialInfo);
	
	
  /**
   * Checks ticket
   *
   */
	boolean IsValid(Ticket ticket);

  /**
   * Sets identity with ticket
   */
  void setIdentity(Ticket ticket, String ident, String userIdent);

  /**
   * Gets identity by ticket
   */
  String getChannelIdentity(Ticket ticket);
  
  /**
   * Gets identity by ticket
   */
  String getUserIdentity(Ticket ticket);

  int getUserId(Ticket ticket);

  /**
   * Destroys session for logged pesant
   *
   */
	boolean Drop(Ticket ticket);
	
	void refresh(Ticket ticket);

}
