package ptt.api.manager;

import java.util.List;
import java.lang.String;

import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;

import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.slice.exceptions.LoginExistsException;
import ptt.api.net.slice.exceptions.BadUserInfoException;
/**
 * 
 * @author Danil Krivopustov
 * 
 */
public interface IUserKeeper {

  /**
   * Checks if given user name is unique
   *
   */
	boolean IsLoginUniq(String userName);

  String getFilepath();
  /**
   * Creates new user in the storage
   *
   */
	void Create(UserInfo info) throws LoginExistsException, BadUserInfoException;

  /**
   * Deletes user
   */
  void Remove(int userId) throws UserNotExistsException;

  /**
   * Gets info
   */
  UserInfo Get(Ticket sid) throws UserNotExistsException;  

  /**
   * Gets info
   */
  UserInfo GetUser(int id) throws UserNotExistsException;  

  /**
   * Gets user contact list
   */
  List<UserInfo> GetContacts(Ticket sid) throws UserNotExistsException;

  /**
   * Searches users
   */
  List<UserInfo> SearchUser(String keyword);

 /**
  * Adds user to contacts
  */
  void AddToContacts(Ticket sid, int id);

  /**
   * Deletes user from contacts
   */
  void DeleteFromContacts(Ticket sid, int id);

  /**
   * Changes password
   */
  void ChangePassword(UserInfo info, Ticket sid) throws BadUserInfoException, UserNotExistsException;

  /**
   * Chnges info without password
   */
  void ChangeInfo(UserInfo info, Ticket sid) throws BadUserInfoException, UserNotExistsException;

  /**
   * Chnage info 
   */
  void Change(UserInfo info, Ticket sid) throws BadUserInfoException, UserNotExistsException;
  
  /**
   * Mutes and unmutes channel
   */
  void Mute(int userId, Ticket sid, int state) throws UserNotExistsException;
}
