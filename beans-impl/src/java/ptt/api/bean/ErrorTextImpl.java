package ptt.api.bean;

import org.springframework.stereotype.Service;

import ptt.api.spring.annotation.Property;

/**
 * 
 * @author Alexey Stolbovskikh
 * 
 */
@Service
public class ErrorTextImpl implements ErrorText {

	private String approveFileErrorText;

	private String remouteReaderIsNotFound;

	public String getApproveFileErrorText() {
		return approveFileErrorText;
	}

	@Property(key = "approve.file.error.test", defaultValue = " ")
	public void setApproveFileErrorText(String approveFileErrorText) {
		this.approveFileErrorText = approveFileErrorText;
	}

	public String getRemouteReaderIsNotFound() {
		return remouteReaderIsNotFound;
	}

	@Property(key = "reader.is.not.found", defaultValue = " ")
	public void setRemouteReaderIsNotFound(String remouteReaderIsNotFound) {
		this.remouteReaderIsNotFound = remouteReaderIsNotFound;
	}

	@Override
	public String AccessForbidden() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String AccessViolation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String CommonError() {
		// TODO Auto-generated method stub
		return null;
	}
}
