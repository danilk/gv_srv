package ptt.api.bean;

import java.beans.PropertyDescriptor;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import ptt.api.spring.annotation.Property;

/**
 * 
 * @author Alexey Stolbovskikh
 * 
 */

public class PropertyAnnotationConfigurer implements BeanFactoryPostProcessor {

	private Logger log = Logger.getLogger(this.getClass());

	private String location;

	private String[] locations;

	private Properties properties = new Properties();

	private String buildPath(String path) {
		StringBuilder builder = new StringBuilder();
		StringTokenizer sttk = new StringTokenizer(path, "}", false);
		while (sttk.hasMoreTokens()) {
			String str = sttk.nextToken();
			int i = str.indexOf("${");
			if (i > -1) {
				i += 2;
				String variable = str.substring(i, str.length());
				String value = System.getProperty(variable);
				if (value == null) {
					log.warn("Variable " + value + " is not defined");
					new IllegalArgumentException(variable + " is not defined");
				}
				builder.append(str.substring(0, i - 2));
				builder.append(value);
			} else {
				builder.append(str);
			}
		}
		return builder.toString();
	}

	private void init(String path) throws IOException {
		path = buildPath(path);
		if (log.isDebugEnabled())
			log.debug("Property file is define: " + path);
		File file = new File(path);
		if (!file.canRead()) {
			throw new FileNotFoundException("Path : " + path);
		}
		InputStream is = null;
		try {
			is = new BufferedInputStream(new FileInputStream(file));
			properties.load(is);
		} finally {
			is.close();
		}
	}

	private void loadProperties() throws BeanInitializationException {
		try {
			if (location != null) {
				init(location);
			}
			if (locations != null) {
				for (String item : locations) {
					init(item);
				}
			}
		} catch (IOException e) {
			log.error("Initialize error", e);
			throw new BeanInitializationException("Initialize error", e);
		}
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory arg0)
			throws BeansException {
		loadProperties();
		processProperties(arg0);
	}

	protected void processProperties(ConfigurableListableBeanFactory beanFactory)
			throws BeansException {
		for (String name : beanFactory.getBeanDefinitionNames()) {
			MutablePropertyValues mpv = beanFactory.getBeanDefinition(name)
					.getPropertyValues();
			Class clazz = beanFactory.getType(name);

			if (log.isDebugEnabled())
				log.debug("Configuring properties for bean=" + name + "["
						+ clazz + "]");

			if (clazz != null) {
				for (PropertyDescriptor property : BeanUtils
						.getPropertyDescriptors(clazz)) {
					Method setter = property.getWriteMethod();
					Method getter = property.getReadMethod();
					Property annotation = null;
					if (setter != null
							&& setter.isAnnotationPresent(Property.class)) {
						annotation = setter.getAnnotation(Property.class);
					} else if (setter != null && getter != null
							&& getter.isAnnotationPresent(Property.class)) {
						annotation = getter.getAnnotation(Property.class);
					}
					if (annotation != null) {
						String value = properties.getProperty(annotation.key());
						if (StringUtils.isEmpty(value)) {
							value = annotation.defaultValue();
						}
						if (StringUtils.isEmpty(value)) {
							throw new RuntimeException("No such property=["
									+ annotation.key()
									+ "] found in properties.");
						}
						if (log.isDebugEnabled())
							log.debug("setting property=[" + clazz.getName()
									+ "." + property.getName() + "] value=["
									+ annotation.key() + "=" + value + "]");
						mpv.addPropertyValue(property.getName(), value);
					}
				}

				for (Field field : clazz.getDeclaredFields()) {
					if (log.isDebugEnabled())
						log.debug("examining field=[" + clazz.getName() + "."
								+ field.getName() + "]");
					if (field.isAnnotationPresent(Property.class)) {
						Property annotation = field
								.getAnnotation(Property.class);
						PropertyDescriptor property = BeanUtils
								.getPropertyDescriptor(clazz, field.getName());

						if (property.getWriteMethod() == null) {
							throw new RuntimeException("setter for property=["
									+ clazz.getName() + "." + field.getName()
									+ "] not available.");
						}

						Object value = properties.getProperty(annotation.key());
						if (value == null) {
							value = annotation.defaultValue();
						}
						if (value == null) {
							throw new RuntimeException("No such property=["
									+ annotation.key()
									+ "] found in properties.");
						}
						if (log.isDebugEnabled())
							log.debug("setting property=[" + clazz.getName()
									+ "." + field.getName() + "] value=["
									+ annotation.key() + "=" + value + "]");
						mpv.addPropertyValue(property.getName(), value);
					}
				}
			}
		}
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setLocations(String[] locations) {
		this.locations = locations;
	}
}
