package ptt.api.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.Ticket;

import ptt.api.net.slice.exceptions.ChannelNotExistsException;
import ptt.api.net.slice.exceptions.BadChannelDataException;
import ptt.api.net.slice.exceptions.AccessDeniedException;
import ptt.api.net.slice.exceptions.ChannelNameExistsException;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
@Service
public class ChannelDAOImpl extends GenericDAOImpl implements
		ChannelDAO {

	@Override
	public boolean isPasswordRequired(int channelId) {
		log.info("looking in data base is password required for" + channelId);
		try {
			return (Boolean)this.getSqlMapClient().queryForObject("isPasswordRequired", channelId);
		} catch(SQLException ex)
		{
			log.error("Can't check channel", ex);
		}
		return false;	
	}

	@Override
	public boolean isChannelOwner(int channelId, int id)  throws ChannelNotExistsException {
		log.info("looking in data base is it channel owner" + channelId + " user" + id);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("userId", id);
			param.put("channelId", channelId);
			return (Boolean)this.getSqlMapClient().queryForObject("isChannelOwner", param);
		} catch(SQLException ex)
		{
			log.error("Can't check channel", ex);
      throw new ChannelNotExistsException(ex.getCause().getMessage());
		}
	}

	@Override
	public boolean authorize(int channelId, String password) {
		log.info("Authorizing on channel" + channelId);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("password", password);
			param.put("channelId", channelId);
			return (Boolean)this.getSqlMapClient().queryForObject("AuthorizeChannel", param);
		} catch(SQLException ex)
		{
			log.error("Can't authorize", ex);
		}
		return false;
	}

	@Override
	public int create(ChannelData info) throws BadChannelDataException, ChannelNameExistsException{
		log.info("creating channel" + info.name);
		try {
			return (Integer)this.getSqlMapClient().queryForObject("CreateChannel", info);
		} catch(SQLException ex)
		{
			log.error("Can't create", ex);
      String message = ex.getCause().getMessage();
      if(message.indexOf("gv_channels_name_key") !=-1)
      {
        throw new ChannelNameExistsException();
      }
      else
      {
        throw new BadChannelDataException(message);
      }
		}
	}

	@Override
	public boolean isUniq(String name) {
		log.info("checking uniq name of channel" + name);
		try {
			return (Boolean)this.getSqlMapClient().queryForObject("IsChannelNameUniq", name);
		} catch(SQLException ex)
		{
			log.error("Can't check", ex);
		}
		return false;
	}

	@Override
	public boolean remove(int channelId, Ticket sid)  throws ChannelNotExistsException, AccessDeniedException{
		log.info("remove channel" + channelId);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("sid", sid.sid);
			param.put("channelId", channelId);
			return (Boolean)this.getSqlMapClient().queryForObject("DropChannel", param);
		} catch(SQLException ex)
		{
			log.error("Can't remove", ex);

      String message = ex.getCause().getMessage();
      if(message.indexOf("access_error") !=-1)
      {
        throw new AccessDeniedException();
      }
      else
      {
        throw new ChannelNotExistsException(message);
      }
		}
	}

	@Override
	public List<ChannelData> get(Ticket sid) {
		log.info("get user channels" + sid.sid);
		try {
			return ( List<ChannelData>)this.getSqlMapClient().queryForList("GetUserChannels", sid);
		} catch(SQLException ex)
		{
			log.error("Can't get", ex);
		}
		return  new ArrayList<ChannelData>();
	}

	@Override
	public List<UserInfo> getUsers(int channelId) {
		log.info("get users in channel" + channelId);
		try {
			return ( List<UserInfo>)this.getSqlMapClient().queryForList("GetUsersChannel", channelId);
		} catch(SQLException ex)
		{
			log.error("Can't get", ex);
		}
		return new ArrayList<UserInfo>();
	}

	@Override
	public List<ChannelData> getAll(String keyword, Ticket sid) {
		log.info("get user channels by " + keyword);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("keyword", keyword);
			param.put("sid", sid.sid);
			return ( List<ChannelData>)this.getSqlMapClient().queryForList("GetUserChannelsByWord", param);
		} catch(SQLException ex)
		{
			log.error("Can't get", ex);
		}
		return new ArrayList<ChannelData>();
	}

	@Override
	public List<ChannelData> getAll(String keyword) {
		log.info("get user channels  by" + keyword);
		try {
			return ( List<ChannelData>)this.getSqlMapClient().queryForList("GetChannelsByWord", keyword);
		} catch(SQLException ex)
		{
			log.error("Can't get", ex);
		}
		return  new ArrayList<ChannelData>();
	}

	@Override
	public boolean subscribe(int channelId, Ticket sid)  throws ChannelNotExistsException, AccessDeniedException{
		log.info("subscribe on channel" + channelId);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("sid", sid.sid);
			param.put("channelId", channelId);
			return (Boolean)this.getSqlMapClient().queryForObject("SubscribeOnChannel", param);
		} catch(SQLException ex)
		{
			log.error("Can't subscribe", ex);

      String message = ex.getCause().getMessage();
      if(message.indexOf("access_error") !=-1)
      {
        throw new AccessDeniedException();
      }
      else
      {
        throw new ChannelNotExistsException(message);
      }
		}
	}

	@Override
	public boolean unsubscribe(int channelId, Ticket sid) throws ChannelNotExistsException{
		log.info("unsubscribe on channel" + channelId);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("sid", sid.sid);
			param.put("channelId", channelId);
			return (Boolean)this.getSqlMapClient().queryForObject("UnsubscribeFromChannel", param);
		} catch(SQLException ex)
		{
			log.error("Can't unsubscribe", ex);
      throw new ChannelNotExistsException(ex.getCause().getMessage());
		}
	}

	@Override
	public void change(ChannelData info) throws AccessDeniedException, ChannelNotExistsException, BadChannelDataException {
		log.info("Changing channel" + info.name);
		try {
			this.getSqlMapClient().queryForObject("ChangeChannel", info);
		} catch(SQLException ex)
		{
			log.error("Can't change", ex);
      String message = ex.getCause().getMessage();
      if(message.indexOf("access_error") !=-1)
      {
        throw new AccessDeniedException();
      }
      else if(message.indexOf("chk_info") !=-1)
      {
        throw new BadChannelDataException();
      }
      else
      {
        throw new ChannelNotExistsException(message);
      }
		}
	}

  @Override
  public void mute(int channelId, Ticket sid, int state) throws ChannelNotExistsException
  {
		log.info((state != 4 ? "un" : "") + "muting channel " + channelId);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("sid", sid.sid);
			param.put("channelId", channelId);
			param.put("state", state);
			this.getSqlMapClient().queryForObject("Mute", param);
		} catch(SQLException ex)
		{
			log.error("Can't change", ex);
      throw new ChannelNotExistsException();
		}
  }
 
  @Override 
  public ChannelData get(int channelId)
  {
		log.info("getting info about channel[" + channelId + "]");
		try {
			return (ChannelData)this.getSqlMapClient().queryForObject("getChannelInfo", channelId);
		} catch(SQLException ex)
		{
			log.error("Can't get info", ex);
		}
		return null;
  }

  @Override 
  public ChannelData get(int channelId, Ticket sid)
  {
		log.info("getting info about channel[" + channelId + "] and sid " + sid.sid);
		try {

			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("sid", sid.sid);
			param.put("channelId", channelId);
			return (ChannelData)this.getSqlMapClient().queryForObject("getChannelInfoBySid", param);
		} catch(SQLException ex)
		{
			log.error("Can't get info", ex);
		}
		return null;
  }

  public boolean canISpeak(int channelId, Ticket sid) throws ChannelNotExistsException
  {
    
		log.info("can I speak on channel" + channelId);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("sid", sid.sid);
			param.put("channelId", channelId);
			return (Boolean)this.getSqlMapClient().queryForObject("CanISpeakOnChannel", param);
		} catch(SQLException ex)
		{
			log.error("Can't check", ex);

      String message = ex.getCause().getMessage();
      throw new ChannelNotExistsException(message);
		}
  }
}
