package ptt.api.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.ibatis.sqlmap.client.SqlMapClient;


/**
 * 
 * @author Alexey Stolbovskikh
 * 
 */
public abstract class GenericDAOImpl
		implements GenericDAO {

	private SqlMapClient sqlMapClient;

	protected Logger log = Logger.getLogger(this.getClass());

	@Autowired
	@Override
	public void setSqlMapClient(SqlMapClient value) {
		sqlMapClient = value;
	}

	@Override
	public SqlMapClient getSqlMapClient() {
		return sqlMapClient;
	}
}
