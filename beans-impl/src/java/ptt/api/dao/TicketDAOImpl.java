package ptt.api.dao;

import java.util.Map;
import java.util.HashMap;
import java.sql.SQLException;
import org.springframework.stereotype.Service;

import ptt.api.net.slice.Credential;
import ptt.api.net.slice.Ticket;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
@Service
public class TicketDAOImpl extends GenericDAOImpl implements
		TicketDAO {


	@Override
	public Ticket getTicketByCredential(Credential credentialInfo) {
		log.info("Trying to authorize");
		try {
			log.info("Trying to authorize");
      return (Ticket)this.getSqlMapClient().queryForObject("getTicketByCredential", credentialInfo);
    } catch(SQLException ex)
		{
			log.error("Can't authorize user", ex);
		}
		return null;	
	}

	@Override
	public boolean dropTicket(Ticket ticket) {
		try {
			return (Boolean)this.getSqlMapClient().queryForObject("dropTicket", ticket);
		} catch(SQLException ex)
		{
			log.error("Wrong session id", ex);
		}
		return false;
	}
	@Override
	public boolean isTicketValid(Ticket ticket) {
		try {
			return (Boolean)this.getSqlMapClient().queryForObject("isTicketValid", ticket);
		} catch(SQLException ex)
		{
			log.error("Wrong session id", ex);
		}
		return false;
	}

	@Override
	public void refresh(Ticket ticket) {
		try {
			this.getSqlMapClient().queryForObject("refreshSession", ticket);
		} catch(SQLException ex)
		{
			log.error("Failed refreshing", ex);
		}		
	}

  @Override
  public void setIdentity(Ticket ticket, String ident, String userIdent)
  {
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("sid", ticket.sid);
			param.put("ident", ident);
			param.put("ident2", userIdent);
			this.getSqlMapClient().queryForObject("setIdentity", param);
		} catch(SQLException ex)
		{
			log.error("Failed setting identity", ex);
		}		
  }

  @Override
  public String getChannelIdentity(Ticket ticket)
  {
		try {
			return (String)this.getSqlMapClient().queryForObject("getChannelIdentity", ticket);
		} catch(SQLException ex)
		{
			log.error("Wrong session id", ex);
		}
		return "";
  }
  @Override
  public String getUserIdentity(Ticket ticket)
  {
		try {
			return (String)this.getSqlMapClient().queryForObject("getUserIdentity", ticket);
		} catch(SQLException ex)
		{
			log.error("Wrong session id", ex);
		}
		return "";
  }
  @Override
    public int getUserId(Ticket ticket)
    {
      try {
        return (Integer)this.getSqlMapClient().queryForObject("getUserId", ticket);
      } catch(SQLException ex)
      {
        log.error("Wrong session id", ex);
      }
      return 0;
    }
}
