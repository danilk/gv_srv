package ptt.api.dao;

import java.sql.SQLException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.lang.String;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.slice.exceptions.LoginExistsException;
import ptt.api.net.slice.exceptions.BadUserInfoException;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
@Service
public class UserDAOImpl extends GenericDAOImpl implements
		UserDAO {

	@Override
	public boolean isLoginUniq(String userName) {
		log.info("looking in data base is login unique?");
		try {
			return (Boolean)this.getSqlMapClient().queryForObject("isLoginUniq", userName);
		}
		catch(SQLException ex)
		{
			log.error("Can't check user name", ex);
		}
		return false;	
	}

	@Override
	public void create(UserInfo info) throws LoginExistsException, BadUserInfoException {
		log.info("creating user " + info.login + " with password " + info.password);
		try {
			this.getSqlMapClient().queryForObject("createUser", info);
		}
		catch(SQLException ex)
		{
			log.error("Can't create user", ex);

      String message = ex.getCause().getMessage();
      if (message.indexOf("gv_users_login_key") !=-1)
      {
        throw new LoginExistsException();
      }
      else
      {
        throw new BadUserInfoException(message);
      }
		}
	}

	@Override
	public void remove(int userId) throws UserNotExistsException {
		log.info("removing user by id " + userId );
		try {
			this.getSqlMapClient().queryForObject("deleteUser", userId);
		} catch(SQLException ex)
		{
			log.error("Can't remove user", ex);
      throw new UserNotExistsException(ex.getCause().getMessage());
		}
	}

	@Override
	public UserInfo get(Ticket sid) throws UserNotExistsException {
		log.info("getting user info by sid " + sid.sid );
		try {
			return (UserInfo)this.getSqlMapClient().queryForObject("getInfo", sid);
		} catch(SQLException ex)
		{
			log.error("Can't get user info", ex);
      throw new UserNotExistsException(ex.getCause().getMessage());
		}
	}

	@Override
	public void changePassword(UserInfo info)  throws UserNotExistsException, BadUserInfoException {
		log.info("changing password for user id " + info.id );
		try {
			this.getSqlMapClient().queryForObject("changePassword", info);
		} catch(SQLException ex)
		{
			log.error("Can't change password", ex);

      String message = ex.getCause().getMessage();
      if (message.indexOf("chk_info") !=-1)
      {
        throw new BadUserInfoException("Bad password");
      }
      else
      {
        throw new UserNotExistsException(message);
      }
		}
	}

	@Override
	public void changeInfo(UserInfo info)  throws UserNotExistsException, BadUserInfoException {
		log.info("changing info for user id " + info.id );
		try {
			this.getSqlMapClient().queryForObject("changeInfo", info);
		} catch(SQLException ex)
		{
			log.error("Can't change info", ex);
      String message = ex.getCause().getMessage();
      if (message.indexOf("chk_info") !=-1)
      {
        throw new BadUserInfoException("Bad info");
      }
      else
      {
        throw new UserNotExistsException(message);
      }
		}
	}

	@Override
	public void change(UserInfo info)  throws UserNotExistsException, BadUserInfoException {
		log.info("changing info for user id " + info.id );
		try {
			this.getSqlMapClient().queryForObject("change", info);
		} catch(SQLException ex)
		{
			log.error("Can't change info", ex);
      String message = ex.getCause().getMessage();
      if (message.indexOf("chk_info") !=-1)
      {
        throw new BadUserInfoException("Bad info");
      }
      else
      {
        throw new UserNotExistsException(message);
      }
		}
	}

  /**
   * Gets info
   */
	@Override
  public UserInfo getUser(int id) throws UserNotExistsException
  {
		log.info("getting user info by id " + id );
		try {
			return (UserInfo)this.getSqlMapClient().queryForObject("getInfoById", id);
		} catch(SQLException ex)
		{
			log.error("Can't get user info", ex);
      throw new UserNotExistsException(ex.getCause().getMessage());
		}
  }

  /**
   * Gets user contact list
   */
	@Override
  public List<UserInfo> getContacts(Ticket sid) throws UserNotExistsException
  {
		log.info("get user contacts" + sid.sid);
		try {
			return ( List<UserInfo>)this.getSqlMapClient().queryForList("GetUserContacts", sid);
		} catch(SQLException ex)
		{
			log.error("Can't get", ex);
		}
		return  new ArrayList<UserInfo>();
  }

  /**
   * Searches users
   */
	@Override
  public List<UserInfo> searchUser(String keyword)
  {
		log.info("search user contacts" + keyword);
		try {
			return ( List<UserInfo>)this.getSqlMapClient().queryForList("SearchUsers", keyword);
		} catch(SQLException ex)
		{
			log.error("Can't searching", ex);
		}
		return  new ArrayList<UserInfo>();
  }

 /**
  * Adds user to contacts
  */
	@Override
  public void addToContacts(Ticket sid, int id)
  {
		log.info("adding  user id " + id + " to cl " + sid.sid );
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("id", id);
			param.put("sid", sid.sid);
		try {
			this.getSqlMapClient().queryForObject("AddToContacts", param);
		} catch(SQLException ex)
    {
			log.error("Can't add to contacts", ex);
    }
  }

  /**
   * Deletes user from contacts
   */
	@Override
  public void deleteFromContacts(Ticket sid, int id)
  {
		log.info("deleting user id " + id + " to cl " + sid.sid );
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("id", id);
			param.put("sid", sid.sid);
		try {
			this.getSqlMapClient().queryForObject("DeleteFromContacts", param);
		} catch(SQLException ex)
    {
			log.error("Can't delete from contacts", ex);
    }
  }
  @Override
  public void mute(int userId, Ticket sid, int state) throws UserNotExistsException
  {
		log.info((state != 4 ? "un" : "") + "muting user " + userId);
		try {
			Map<Object,Object> param = new HashMap<Object,Object>();
			param.put("sid", sid.sid);
			param.put("userId", userId);
			param.put("state", state);
			this.getSqlMapClient().queryForObject("MuteUser", param);
		} catch(SQLException ex)
		{
			log.error("Can't change", ex);
      throw new UserNotExistsException();
		}
  }
 
}
