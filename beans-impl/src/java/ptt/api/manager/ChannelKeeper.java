package ptt.api.manager;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ptt.api.dao.ChannelDAO;
import ptt.api.dao.UserDAO;
import ptt.api.manager.IChannelKeeper;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;

import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.slice.exceptions.LoginExistsException;
import ptt.api.net.slice.exceptions.BadUserInfoException;

import ptt.api.net.slice.exceptions.ChannelNotExistsException;
import ptt.api.net.slice.exceptions.BadChannelDataException;
import ptt.api.net.slice.exceptions.AccessDeniedException;
import ptt.api.net.slice.exceptions.ChannelNameExistsException;

import ptt.api.spring.annotation.Property;
/**
 * 
 * @author Danil Krivopustov
 * 
 */
@Service
public class ChannelKeeper implements IChannelKeeper {

	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(this.getClass());

	private ChannelDAO channelDAO;	
	private UserDAO userDAO;
  private int m_repeatInterval = 60;
  private String m_path;

  @Property(key="repeat_interval")
  public 
    void setRepeatInterval(int repeatInterval)
    {
      m_repeatInterval = repeatInterval;
    }
  @Override
    public int getRepeatInterval()
    {
      return m_repeatInterval;
    }

  @Property(key="channel_filepath")
  public 
    void setFilepath(String path)
    {
      m_path = path;
    }

	@Override
    public 
    String getFilepath()
    {
      return m_path;
    }

	@Autowired
	public void setChannelDAO(ChannelDAO channel)
	{
		this.channelDAO = channel;
	}

	@Autowired
	public void setUserDAO(UserDAO user)
	{
		this.userDAO = user;
	}
	@Override
	public boolean IsPasswordRequired(int channelId) {
		return channelDAO.isPasswordRequired(channelId);
	}

	@Override
	public boolean IsChannelOwner(int channelId, Ticket sid)  throws UserNotExistsException, ChannelNotExistsException {		
		UserInfo user = userDAO.get(sid);
		return channelDAO.isChannelOwner(channelId, user.id);
	}

	@Override
	public boolean Authorize(int channelId, String password) {
		return channelDAO.authorize(channelId, password);
	}



	@Override
	public int Create(ChannelData info, Ticket sid) throws UserNotExistsException, BadChannelDataException, ChannelNameExistsException 
  {
		UserInfo user = userDAO.get(sid);
		info.ownerId = user.id;
		return channelDAO.create(info);
	}

	@Override
	public boolean IsUniq(String name) {
		return channelDAO.isUniq(name);
	}

	@Override
	public boolean Remove(int channelId, Ticket sid) throws ChannelNotExistsException, AccessDeniedException 
  {
		return channelDAO.remove(channelId, sid);
	}



	@Override
	public List<ChannelData> Get(Ticket sid) {
		return channelDAO.get(sid);
	}


	@Override
	public List<UserInfo> GetUsers(int channelId) {
		return channelDAO.getUsers(channelId);
	}

	@Override
	public List<ChannelData> GetAll(String keyword, Ticket sid) {
		return channelDAO.getAll(keyword, sid);
	}

	@Override
	public List<ChannelData> GetAll(String keyword) {
		return channelDAO.getAll(keyword);
	}

	@Override
	public boolean Subscribe(int channelId, Ticket sid) throws ChannelNotExistsException, AccessDeniedException
  {
		return channelDAO.subscribe(channelId, sid);
	}
	
	@Override
	public boolean Unsubscribe(int channelId, Ticket sid) throws ChannelNotExistsException
  {

		return channelDAO.unsubscribe(channelId, sid);
	}

	@Override
	public void Change(ChannelData info, Ticket sid) throws UserNotExistsException, AccessDeniedException, ChannelNotExistsException, BadChannelDataException {
		UserInfo user = userDAO.get(sid);
    info.ownerId = user.id;
		channelDAO.change(info);
	}

	@Override
	public ChannelData Get(int channelId)
  {
		return channelDAO.get(channelId);
	}

	@Override
	public ChannelData Get(int channelId, Ticket sid)
  {
		return channelDAO.get(channelId, sid);
	}

  @Override
  public void Mute(int channelId, Ticket sid, int state) throws ChannelNotExistsException
  {
    channelDAO.mute(channelId, sid, state);
  }

  @Override
  public boolean CanISpeak(int channelId, Ticket sid) throws ChannelNotExistsException
  {
    return channelDAO.canISpeak(channelId, sid);
  }
}
