package ptt.api.manager;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ptt.api.dao.TicketDAO;
import ptt.api.manager.ITicketKeeper;
import ptt.api.net.slice.Credential;
import ptt.api.net.slice.Ticket;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
@Service
public class TicketKeeper implements ITicketKeeper {

	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(this.getClass());

	private TicketDAO ticketDAO;

	
	@Override
	public Ticket Get(Credential credentialInfo) {

		return ticketDAO.getTicketByCredential(credentialInfo);
	}
	
	@Override
	public boolean IsValid(Ticket ticket)
	{
		return ticketDAO.isTicketValid(ticket);
	}

	@Override
	public boolean Drop(Ticket ticket) {
		ticketDAO.dropTicket(ticket);
    return true;
	}
	
	
	@Autowired
	public void setTicketDAO(TicketDAO ticket)
	{
		this.ticketDAO = ticket;
	}

	@Override
	public void refresh(Ticket ticket) {
		ticketDAO.refresh(ticket);
	}

  @Override
    public void setIdentity(Ticket ticket, String ident, String userIdent)
    {
      ticketDAO.setIdentity(ticket, ident, userIdent);
    }

  @Override
    public String getChannelIdentity(Ticket ticket)
    {
      return ticketDAO.getChannelIdentity(ticket);
    }

  @Override
    public String getUserIdentity(Ticket ticket)
    {
      return ticketDAO.getUserIdentity(ticket);
    }

  @Override
    public int getUserId(Ticket ticket)
    {
      return ticketDAO.getUserId(ticket);
    }
}
