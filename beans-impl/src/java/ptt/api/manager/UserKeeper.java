package ptt.api.manager;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ptt.api.dao.UserDAO;
import ptt.api.manager.IUserKeeper;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;

import java.util.List;
import java.lang.String;
import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.slice.exceptions.LoginExistsException;
import ptt.api.net.slice.exceptions.BadUserInfoException;
import ptt.api.spring.annotation.Property;
/**
 * 
 * @author Danil Krivopustov
 * 
 */
@Service
public class UserKeeper implements IUserKeeper {

	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(this.getClass());

	private UserDAO userDAO;
  private String m_path;

  @Property(key="user_filepath")
  public 
    void setFilepath(String path)
    {
      m_path = path;
    }
	
	@Override
    public 
    String getFilepath()
    {
      return m_path;
    }
	@Override
	public void Create(UserInfo info) throws LoginExistsException, BadUserInfoException {

		userDAO.create(info);
	}
	
	@Override
	public boolean IsLoginUniq(String userName)
	{
		return userDAO.isLoginUniq(userName);
	}

	@Autowired
	public void setUserDAO(UserDAO user)
	{
		this.userDAO = user;
	}

	@Override
	public void Remove(int userId)  throws UserNotExistsException {
		userDAO.remove(userId);
	}

	@Override
	public UserInfo Get(Ticket sid)  throws UserNotExistsException {
		return userDAO.get(sid);
	}

	@Override
	public void ChangePassword(UserInfo info, Ticket sid)  throws UserNotExistsException, BadUserInfoException {
		info.id = Get(sid).id;
		userDAO.changePassword(info);
		}

	@Override
	public void ChangeInfo(UserInfo info, Ticket sid)  throws UserNotExistsException, BadUserInfoException {
		info.id = Get(sid).id;
		userDAO.changeInfo(info);
	}

	@Override
	public void Change(UserInfo info, Ticket sid)  throws UserNotExistsException, BadUserInfoException {
		info.id = Get(sid).id;
		userDAO.change(info);
	}

  /**
   * Gets info
   */
	@Override
  public UserInfo GetUser(int id) throws UserNotExistsException
  {
    return userDAO.getUser(id);
  }

  /**
   * Gets user contact list
   */
	@Override
  public List<UserInfo> GetContacts(Ticket sid) throws UserNotExistsException
  {
    return userDAO.getContacts(sid);
  }

  /**
   * Searches users
   */
	@Override
  public List<UserInfo> SearchUser(String keyword)
  {
    return userDAO.searchUser(keyword);
  }

 /**
  * Adds user to contacts
  */
	@Override
  public void AddToContacts(Ticket sid, int id)
  {
    userDAO.addToContacts(sid, id);
  }

  /**
   * Deletes user from contacts
   */
	@Override
  public void DeleteFromContacts(Ticket sid, int id)
  {
    userDAO.deleteFromContacts(sid, id);
  }

  @Override
  public void Mute(int userId, Ticket sid, int state) throws UserNotExistsException
  {
    userDAO.mute(userId, sid, state);
  }
}
