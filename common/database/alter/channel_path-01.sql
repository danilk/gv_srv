
ALTER TABLE gv_channels DROP COLUMN private CASCADE;
ALTER TABLE gv_subscribers DROP COLUMN muted CASCADE;


ALTER TABLE gv_channels ADD column broadcast_type INTEGER DEFAULT 0;
ALTER TABLE gv_channels ADD column communication_type INTEGER DEFAULT 0;

ALTER TABLE gv_subscribers ADD column state INTEGER DEFAULT 0;
