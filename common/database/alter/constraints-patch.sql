ALTER TABLE gv_users ADD UNIQUE(login); 

ALTER TABLE gv_users ADD CONSTRAINT chk_info CHECK (login <> '' AND password <> '' AND email <> '');

ALTER TABLE gv_channels ADD UNIQUE(name); 

ALTER TABLE gv_channels ADD CONSTRAINT chk_info CHECK (name <> '');
