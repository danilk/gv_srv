
CREATE OR REPLACE FUNCTION 
authorize(i_signature VARCHAR(32))
  RETURNS VARCHAR(32)
  AS $$
  DECLARE
    v_user_id INTEGER;
  BEGIN
    SELECT INTO v_user_id user_id FROM gv_users WHERE md5(login || password) = i_signature;
    RETURN get_sid(v_user_id);
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
authorize(i_login VARCHAR(32), i_password VARCHAR(32))
  RETURNS VARCHAR(32)
  AS $$
  DECLARE
    v_user_id INTEGER;
  BEGIN
    SELECT INTO v_user_id user_id FROM gv_users WHERE login = i_login AND password = i_password;
    RETURN get_sid(v_user_id);
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
refresh(i_sid VARCHAR(32))
  RETURNS void
  AS $$
  DECLARE
    v_user_id INTEGER;
  BEGIN
    update gv_sessions set update_ts = current_timestamp WHERE session_id = i_sid;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
get_identity(i_sid VARCHAR(32))
  RETURNS VARCHAR(255)
  AS $$
  DECLARE
    v_ident VARCHAR(255);
  BEGIN
    select INTO v_ident ident FROM gv_sessions WHERE  session_id = i_sid;
    return v_ident;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
get_user_identity(i_sid VARCHAR(32))
  RETURNS VARCHAR(255)
  AS $$
  DECLARE
    v_ident VARCHAR(255);
  BEGIN
    select INTO v_ident user_ident FROM gv_sessions WHERE  session_id = i_sid;
    return v_ident;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
set_identity(i_sid VARCHAR(32), i_ident VARCHAR(255), i_ident2 VARCHAR(255))
  RETURNS void
  AS $$
  BEGIN
    update gv_sessions set ident = i_ident, user_ident = i_ident2 WHERE session_id = i_sid;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
