
CREATE OR REPLACE FUNCTION 
is_password_required(i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
   v_private BOOLEAN; 
  BEGIN
    SELECT INTO v_private broadcast_type in(4, 5, 1) FROM gv_channels WHERE channel_id = i_channel_id;
    RETURN v_private;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
can_i_speak_on_channel(i_sid VARCHAR(32), i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    RETURN is_channel_owner(get_uid(i_sid), i_channel_id) IS TRUE OR does_channel_allow(get_uid(i_sid), i_channel_id) IS TRUE;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
does_channel_allow(i_user_id INTEGER, i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
  v_result BOOLEAN;
  BEGIN
    SELECT INTO v_result (
        (broadcast_type IN(2, 3, 5) AND s.authorized IS TRUE) 
        OR 
        broadcast_type IN(0, 1, 4)
        ) 
    AND communication_type = 0
    FROM gv_channels as c INNER JOIN gv_subscribers as s on s.channel_id = c.channel_id 
    WHERE c.channel_id = i_channel_id AND s.user_id = i_user_id;
    RETURN v_result;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
is_channel_owner(i_sid VARCHAR(32), i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    RETURN is_channel_owner(get_uid(i_sid), i_channel_id);
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
CREATE OR REPLACE FUNCTION 
is_channel_owner(i_user_id INTEGER, i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
   v_channel_id INTEGER; 
  BEGIN
    SELECT INTO v_channel_id channel_id FROM gv_channels WHERE user_id = i_user_id AND channel_id = i_channel_id;
    RETURN v_channel_id is not null;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;


CREATE OR REPLACE FUNCTION 
channel_authorize(i_channel_id INTEGER, i_password VARCHAR(255))
  RETURNS BOOLEAN
  AS $$
  DECLARE
   v_channel_id INTEGER;
  BEGIN
    SELECT INTO v_channel_id channel_id FROM gv_channels WHERE channel_id = i_channel_id AND (i_password = password OR is_password_required(channel_id) is FALSE);
    RETURN v_channel_id is not NULL;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
