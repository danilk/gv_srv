
--DROP FUNCTION add_channel(i_name VARCHAR(255), i_password VARCHAR(255), i_description TEXT, i_sid VARCHAR(32), i_private BOOLEAN);
CREATE OR REPLACE FUNCTION
add_channel(i_name VARCHAR(255), i_display_name VARCHAR(255), i_password VARCHAR(255), i_description TEXT, i_sid VARCHAR(32), i_communication_type INTEGER, i_broadcast_type INTEGER)
  RETURNS INTEGER
  AS $$
  BEGIN
    RETURN add_channel(i_name, i_display_name, i_password, i_description, get_uid(i_sid), i_communication_type, i_broadcast_type);
  END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION add_channel(i_name VARCHAR(255), i_password VARCHAR(255), i_description TEXT, i_user INTEGER, i_private BOOLEAN);
CREATE OR REPLACE FUNCTION
add_channel(i_name VARCHAR(255), i_display_name VARCHAR(255), i_password VARCHAR(255), i_description TEXT, i_user INTEGER, i_communication_type INTEGER, i_broadcast_type INTEGER)
  RETURNS INTEGER
  AS $$
  DECLARE
    v_channel_id INTEGER;
  BEGIN
    IF is_channel_name_unique(i_name) is TRUE THEN
    BEGIN
      INSERT INTO gv_channels(name, display_name, password, description, communication_type, broadcast_type, user_id) VALUES(i_name, i_display_name, i_password, i_description, i_communication_type, i_broadcast_type, i_user);
      SELECT INTO v_channel_id currval('seq_channel_id');
      PERFORM subscribe(i_user, v_channel_id);
      RETURN v_channel_id;
    END;
    END IF;
    RETURN -1;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
is_channel_name_unique(i_name VARCHAR(255))
  RETURNS BOOLEAN
  AS $$
  DECLARE
    v_channel_id INTEGER;
  BEGIN
    SELECT INTO v_channel_id channel_id FROM gv_channels WHERE name = i_name;
    RETURN v_channel_id is NULL;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
