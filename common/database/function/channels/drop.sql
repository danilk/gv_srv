CREATE OR REPLACE FUNCTION
drop_channel(i_channel_id INTEGER, i_sid VARCHAR(32))
  RETURNS BOOLEAN
  AS $$
  DECLARE
    v_channel_id INTEGER;
  BEGIN
    RETURN drop_channel(i_channel_id, get_uid(i_sid));
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
drop_channel(i_channel_id INTEGER, i_user_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    IF is_channel_owner(i_user_id, i_channel_id) is TRUE THEN
    BEGIN
      DELETE FROM gv_channels WHERE channel_id = i_channel_id;
      RETURN TRUE;
    END;
    END IF;
    RAISE EXCEPTION 'access_error';
  END;
$$ LANGUAGE plpgsql;
