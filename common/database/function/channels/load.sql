
DROP TYPE IF EXISTS gvt_channel_info CASCADE;
CREATE TYPE gvt_channel_info as
(
  channel_id INTEGER,
  name VARCHAR(255),
  display_name VARCHAR(255),
  password VARCHAR(255),
  description TEXT,
  broadcast_type INTEGER,
  communication_type INTEGER,
  channel_state INTEGER,
  authorized BOOLEAN,
  user_id INTEGER,
  subscribers BIGINT,
  owner BOOLEAN,
  blocked BOOLEAN
);

DROP FUNCTION IF EXISTS get_user_channels(i_sid VARCHAR(32));
CREATE OR REPLACE FUNCTION
get_user_channels(i_sid VARCHAR(32))
  RETURNS SETOF gvt_channel_info
  AS $$
  DECLARE
  BEGIN
    RETURN QUERY SELECT vch.channel_id, name, display_name, password, description, broadcast_type,
     communication_type, s.state, s.authorized, vch.user_id, subscribers, 
     (vch.user_id = s.user_id) as owner,  s.blocked
    FROM vgv_channels as vch
    INNER JOIN gv_subscribers as s ON s.channel_id = vch.channel_id
    WHERE  s.user_id = get_uid(i_sid);
  END;
$$ LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS search_channels(i_keyword VARCHAR(255),i_sid VARCHAR(32));
CREATE OR REPLACE FUNCTION
search_channels(i_keyword VARCHAR(255),i_sid VARCHAR(32))
  RETURNS SETOF gvt_channel_info
  AS $$
  DECLARE
    keyword VARCHAR(255);
  BEGIN
    keyword := '%' || i_keyword || '%';
    RETURN QUERY SELECT vch.channel_id, name, display_name, password, description, broadcast_type, communication_type, s.state, s.authorized, 
    vch.user_id, subscribers,
    (vch.user_id = s.user_id) as owner,  s.blocked
   FROM vgv_channels as vch
    INNER JOIN gv_subscribers as s ON s.channel_id = vch.channel_id
    WHERE  s.user_id = get_uid(i_sid)
        AND (vch.name LIKE keyword OR vch.description LIKE keyword OR vch.display_name LIKE keyword);
   END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS search_channels(i_keyword VARCHAR(255));
CREATE OR REPLACE FUNCTION
search_channels(i_keyword VARCHAR(255))
  RETURNS SETOF gvt_channel_info
  AS $$
  DECLARE
    keyword VARCHAR(255);
  BEGIN
    keyword := '%' || i_keyword || '%';
    RETURN QUERY SELECT channel_id, name, display_name, password, description, broadcast_type, communication_type, 1 as state, FALSE as authorized, 
    user_id, subscribers, false as owner, false as blocked
 FROM vgv_channels as vch WHERE
        vch.name LIKE keyword OR vch.description LIKE keyword OR vch.display_name LIKE keyword;
  END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION
get_channel_info(i_channel_id INTEGER)
  RETURNS gvt_channel_info
  AS $$
  DECLARE
  	v_result gvt_channel_info%ROWTYPE;
  BEGIN
    SELECT INTO v_result channel_id, name, display_name, password, description, broadcast_type, communication_type, 1 as state, FALSE as authorized, 
    user_id, subscribers, false as owner, false as blocked
 FROM vgv_channels as vch WHERE
        vch.channel_id = i_channel_id;
	RETURN v_result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
get_channel_info(i_channel_id INTEGER, i_sid VARCHAR(32))
  RETURNS gvt_channel_info
  AS $$
  DECLARE
  	v_result gvt_channel_info%ROWTYPE;
  BEGIN
    SELECT INTO v_result vch.channel_id, name, display_name, password, description, broadcast_type, communication_type, s.state, s.authorized, 
    vch.user_id, subscribers,
    (vch.user_id = s.user_id) as owner,  s.blocked
   FROM vgv_channels as vch
    INNER JOIN gv_subscribers as s ON s.channel_id = vch.channel_id
    WHERE  s.user_id = get_uid(i_sid)
        AND vch.channel_id = i_channel_id;
	RETURN v_result;
  END;
$$ LANGUAGE plpgsql;
