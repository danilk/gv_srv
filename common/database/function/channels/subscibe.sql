
CREATE OR REPLACE FUNCTION 
unsubscribe_channel(i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    DELETE FROM gv_subscribers as s WHERE s.channel_id = i_channel_id;
    RETURN TRUE;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
unsubscribe_user(i_user_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    DELETE FROM gv_subscribers WHERE user_id = i_user_id;
    RETURN TRUE;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
unsubscribe(i_sid VARCHAR(32), i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    RETURN unsubscribe(get_uid(i_sid), i_channel_id);
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;



CREATE OR REPLACE FUNCTION 
unsubscribe(i_user_id INTEGER, i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    DELETE FROM gv_subscribers WHERE user_id = i_user_id  AND channel_id = i_channel_id;
    RETURN TRUE;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
subscribe(i_sid VARCHAR(32), i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    RETURN subscribe(get_uid(i_sid), i_channel_id);
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
subscribe(i_user_id INTEGER, i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
   v_user_id INTEGER;
  BEGIN
    SELECT INTO v_user_id user_id FROM gv_subscribers WHERE user_id = i_user_id AND channel_id = i_channel_id;
    IF v_user_id is NULL THEN    
      INSERT INTO gv_subscribers(user_id, channel_id) VALUES(i_user_id, i_channel_id);    
      RETURN TRUE;
    END IF;
    RETURN FALSE;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
