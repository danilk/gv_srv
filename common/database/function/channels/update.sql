
CREATE OR REPLACE FUNCTION
update_channel(i_channel_id INTEGER, i_name VARCHAR(255), i_display_name VARCHAR(255),i_password VARCHAR(255), i_description TEXT, i_sid VARCHAR(32), i_communication_type INTEGER, i_broadcast_type INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
    v_channel_id INTEGER;
  BEGIN
    RETURN update_channel(i_channel_id, i_name, i_display_name, i_password, i_description, get_uid(i_sid), i_communication_type, i_broadcast_type);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
update_channel(i_channel_id INTEGER, i_name VARCHAR(255), i_display_name VARCHAR(255), i_password VARCHAR(255), i_description TEXT, i_user INTEGER, i_communication_type INTEGER, i_broadcast_type INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
    v_channel_id INTEGER;
  BEGIN
    IF is_channel_owner(i_user, i_channel_id) is TRUE AND is_channel_name_unique(i_name, i_channel_id) is TRUE THEN
    BEGIN
      UPDATE gv_channels SET name = i_name, display_name = i_display_name, password = i_password, description = i_description, communication_type = i_communication_type, broadcast_type = i_broadcast_type WHERE user_id = i_user AND channel_id = i_channel_id;
      RETURN TRUE;
    END;
    END IF;
    RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
is_channel_name_unique(i_name VARCHAR(255), i_channel_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
    v_channel_id INTEGER;
  BEGIN
    SELECT INTO v_channel_id channel_id FROM gv_channels WHERE name = i_name AND channel_id != i_channel_id;
    RETURN v_channel_id is NULL;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION
mute_channel(i_sid VARCHAR(255), i_channel_id INTEGER, i_state INTEGER)
  RETURNS void
  AS $$
  DECLARE
    v_channel_id INTEGER;
  BEGIN
  --  IF is_channel_owner(get_uid(i_sid), i_channel_id) is TRUE THEN
  --  BEGIN
  --  	update gv_subscribers set state = i_state where channel_id = i_channel_id and user_id != get_uid(i_sid);
 --   END;
 --   ELSE
 --   BEGIN
      	update gv_subscribers set state = i_state where channel_id = i_channel_id;
  --  END;
  --  END IF;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
