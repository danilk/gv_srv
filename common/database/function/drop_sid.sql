CREATE OR REPLACE FUNCTION 
drop_sid(i_user_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
    v_sid VARCHAR(32);
    v_user_id INTEGER;
  BEGIN
    SELECT INTO v_user_id user_id FROM gv_users WHERE user_id = i_user_id;
    IF v_user_id is NULL THEN
      RETURN FALSE;
    END IF;
    SELECT INTO v_sid session_id FROM gv_sessions WHERE user_id = i_user_id;
    IF v_sid is NULL THEN
      BEGIN
        DELETE FROM gv_sessions WHERE session_id = v_sid;
        RETURN TRUE;
      END;
    END IF;
    RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION 
drop_sid(i_sid VARCHAR(32))
  RETURNS BOOLEAN
  AS $$
  DECLARE
    v_user_id INTEGER;
  BEGIN
    RETURN drop_sid(get_uid(i_sid));
  END;
$$ LANGUAGE plpgsql;

