CREATE OR REPLACE FUNCTION 
get_sid(i_user_id INTEGER)
  RETURNS VARCHAR(32)
  AS $$
  DECLARE
    v_sid VARCHAR(32);
    v_user_id INTEGER;
  BEGIN
	DELETE FROM gv_sessions WHERE update_ts < CURRENT_TIMESTAMP - INTERVAL '24 hour';
    SELECT INTO v_user_id user_id FROM gv_users WHERE user_id = i_user_id;
    IF v_user_id is NULL THEN
      RETURN NULL;
    END IF;
    SELECT INTO v_sid session_id FROM gv_sessions WHERE user_id = i_user_id;
    IF v_sid is NULL THEN
      BEGIN
        INSERT INTO gv_sessions(user_id) VALUES(i_user_id);
        SELECT INTO v_sid session_id FROM gv_sessions WHERE user_id = i_user_id ORDER BY update_ts DESC;
      END;
    END IF;
    PERFORM refresh(v_sid);
    RETURN v_sid;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

