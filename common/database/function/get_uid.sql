CREATE OR REPLACE FUNCTION 
get_uid(i_sid VARCHAR(32))
  RETURNS INTEGER
  AS $$
  DECLARE
    v_user_id INTEGER;
  BEGIN
    SELECT INTO v_user_id user_id FROM gv_sessions WHERE session_id = i_sid;
    RETURN v_user_id;
  END;
$$ LANGUAGE plpgsql
IMMUTABLE
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
is_valid_sid(i_sid VARCHAR(32))
  RETURNS BOOLEAN
  AS $$
  BEGIN
    RETURN get_uid(i_sid) is not NULL;
  END;
$$ LANGUAGE plpgsql
IMMUTABLE
RETURNS NULL ON NULL INPUT;

