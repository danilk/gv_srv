DROP FUNCTION IF EXISTS register_user(i_login VARCHAR(255), i_password VARCHAR(255), i_email VARCHAR(255));
  
CREATE OR REPLACE FUNCTION 
register_user(i_login VARCHAR(255), i_password VARCHAR(255), i_email VARCHAR(255))
  RETURNS VOID
  AS $$
  BEGIN
    INSERT INTO gv_users(login, password, email) VALUES(i_login, i_password, i_email);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION 
is_login_unique(i_login VARCHAR(255))
  RETURNS BOOLEAN
  AS $$
  DECLARE
    v_user_id INTEGER;
  BEGIN
    SELECT INTO v_user_id user_id FROM gv_users WHERE login = i_login;
    RETURN v_user_id is NULL;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
