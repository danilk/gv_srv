DROP FUNCTION delete_user(i_user_id INTEGER);

CREATE OR REPLACE FUNCTION 
delete_user(i_user_id INTEGER)
  RETURNS VOID
  AS $$
  BEGIN
    DELETE FROM gv_users WHERE user_id = i_user_id;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
