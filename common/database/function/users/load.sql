DROP TYPE IF EXISTS gvt_user_info CASCADE;
CREATE TYPE gvt_user_info as
(
  user_id INTEGER,
  login VARCHAR(255),
  password VARCHAR(255),
  email VARCHAR(255),
  state integer
);

CREATE OR REPLACE FUNCTION
get_user_info(in VARCHAR(32))
  RETURNS gvt_user_info
  AS $$
    SELECT user_id, login, password, email, 1 FROM gv_users WHERE user_id = get_uid($1);
$$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION
get_user_info(in INTEGER)
  RETURNS gvt_user_info
  AS $$
    SELECT user_id, login, password, email, 1 FROM gv_users WHERE user_id = $1;
$$ LANGUAGE sql;


DROP FUNCTION IF EXISTS get_users_channel(i_id integer);
CREATE OR REPLACE FUNCTION
get_users_channel(i_id integer)
  RETURNS SETOF gvt_user_info
  AS $$
  DECLARE
  BEGIN
    RETURN QUERY SELECT u.user_id, u.login, password, u.email, 1
    FROM gv_users as u
    INNER JOIN gv_subscribers as s ON s.user_id = u.user_id
    WHERE  s.channel_id = i_id;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS get_user_contacts(i_sid VARCHAR(32));
CREATE OR REPLACE FUNCTION
get_user_contacts(i_sid VARCHAR(32))
  RETURNS SETOF gvt_user_info
  AS $$
  DECLARE
  BEGIN
    RETURN QUERY SELECT u.user_id, u.login,password, u.email, s.state
    FROM gv_users as u
    INNER JOIN gv_contacts as s ON s.user_id = u.user_id
    WHERE  s.owner_id = get_uid(i_sid);
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS search_users(i_keyword VARCHAR(255));
CREATE OR REPLACE FUNCTION
search_users(i_keyword VARCHAR(255))
  RETURNS SETOF gvt_user_info
  AS $$
  DECLARE
    keyword VARCHAR(255);
  BEGIN
    keyword := '%' || i_keyword || '%';
    RETURN QUERY SELECT user_id, login,password, email, 1
    FROM gv_users as u WHERE
        u.login LIKE keyword OR u.email LIKE keyword;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS search_contacts(i_keyword VARCHAR(255),i_sid VARCHAR(32));
CREATE OR REPLACE FUNCTION
search_contacts(i_keyword VARCHAR(255),i_sid VARCHAR(32))
  RETURNS SETOF gvt_user_info
  AS $$
  DECLARE
    keyword VARCHAR(255);
  BEGIN
    keyword := '%' || i_keyword || '%';
    RETURN QUERY SELECT u.user_id, u.login, password, u.email, c.state
    FROM gv_users as u 
    INNER JOIN gv_contacts as c ON c.user_id = u.user_id
    AND c.owner_id = get_uid(i_sid)
    WHERE
        u.login LIKE keyword OR u.email LIKE keyword;
   END;
$$ LANGUAGE plpgsql;
