CREATE OR REPLACE FUNCTION 
remove_contact(i_owner_id INTEGER, i_user_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    DELETE FROM gv_contacts WHERE owner_id = i_owner_id  AND user_id = i_user_id;
    RETURN TRUE;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
remove_contact(i_sid VARCHAR(32), i_user_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    RETURN remove_contact(get_uid(i_sid), i_user_id);
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
add_contact(i_owner_id INTEGER, i_user_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  DECLARE
   v_user_id INTEGER;
  BEGIN
    SELECT INTO v_user_id user_id FROM gv_contacts WHERE owner_id = i_owner_id AND user_id = i_user_id;
    IF v_user_id is NULL THEN    
      BEGIN
      INSERT INTO gv_contacts(owner_id, user_id) VALUES(i_owner_id, i_user_id);    
      PERFORM add_contact(i_user_id, i_owner_id);
      RETURN TRUE;
      END;
    END IF;
    RETURN FALSE;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION 
add_contact(i_sid VARCHAR(32), i_user_id INTEGER)
  RETURNS BOOLEAN
  AS $$
  BEGIN
    RETURN add_contact(get_uid(i_sid), i_user_id);
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;

CREATE OR REPLACE FUNCTION
mute_user(i_sid VARCHAR(255), i_user_id INTEGER, i_state INTEGER)
  RETURNS void
  AS $$
  DECLARE
    v_user_id INTEGER;
  BEGIN
  --  IF is_channel_owner(get_uid(i_sid), i_channel_id) is TRUE THEN
  --  BEGIN
  --  	update gv_subscribers set state = i_state where channel_id = i_channel_id and user_id != get_uid(i_sid);
 --   END;
 --   ELSE
 --   BEGIN
      	update gv_contacts set state = i_state where user_id = i_user_id and owner_id = get_uid(i_sid);
  --  END;
  --  END IF;
  END;
$$ LANGUAGE plpgsql
RETURNS NULL ON NULL INPUT;
