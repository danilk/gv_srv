CREATE OR REPLACE FUNCTION 
change_password(i_user_id INTEGER, i_password VARCHAR(255))
  RETURNS void
  AS $$
  BEGIN
    UPDATE gv_users SET password = i_password WHERE user_id = i_user_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION 
change_info(i_user_id INTEGER, i_email VARCHAR(255))
  RETURNS void
  AS $$
  BEGIN
    UPDATE gv_users SET email = i_email WHERE user_id = i_user_id;
  END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION 
change_info(i_user_id INTEGER, i_password VARCHAR(255), i_email VARCHAR(255))
  RETURNS void
  AS $$
  BEGIN
    PERFORM change_info(i_user_id, i_email);
    PERFORM change_password(i_user_id, i_password);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION 
change_password(i_sid VARCHAR(32), i_password VARCHAR(255))
  RETURNS void
  AS $$
  BEGIN
    PERFORM change_password(get_uid(i_sid), i_password);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION 
change_info(i_sid VARCHAR(32), i_email VARCHAR(255))
  RETURNS void
  AS $$
  BEGIN
    PERFORM change_info(get_uid(i_sid), i_email);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION 
change_info(i_sid VARCHAR(32), i_password VARCHAR(255), i_email VARCHAR(255))
  RETURNS void
  AS $$
  BEGIN
    PERFORM change_info(get_uid(i_sid), i_password, i_email);
  END;
$$ LANGUAGE plpgsql;
