---


--DROP TYPE IF EXISTS gv_channel_state CASCADE;
--CREATE TYPE gv_channel_state AS ENUM ();

DROP SEQUENCE IF EXISTS seq_channel_id CASCADE;
CREATE SEQUENCE seq_channel_id;


DROP TABLE IF EXISTS gv_channels CASCADE;
CREATE TABLE gv_channels (
 channel_id INTEGER DEFAULT nextval('seq_channel_id') NOT NULL PRIMARY KEY,
 name VARCHAR(255) NOT NULL DEFAULT '',
 password VARCHAR(255) NOT NULL DEFAULT '',
 description TEXT,
 broadcast_type INTEGER DEFAULT 0,
 communication_type INTEGER DEFAULT 0,
 user_id INTEGER NOT NULL CONSTRAINT fk_channel_user_id REFERENCES gv_users(user_id) ON DELETE CASCADE,
 update_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 create_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS gv_subscribers CASCADE;
CREATE TABLE gv_subscribers (
 channel_id INTEGER NOT NULL CONSTRAINT fx_subsriber_channel_id REFERENCES gv_channels(channel_id) ON DELETE CASCADE,
 user_id INTEGER NOT NULL CONSTRAINT fk_channel_user_id REFERENCES gv_users(user_id) ON DELETE CASCADE,
 state  INTEGER DEFAULT 0,
 autorized BOOLEAN DEFAULT FALSE
);

