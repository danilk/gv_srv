DROP VIEW IF EXISTS vgv_channels CASCADE;

CREATE OR REPLACE VIEW vgv_channels
AS SELECT ch.*,
   (SELECT count(1) FROM gv_subscribers as s WHERE s.channel_id = ch.channel_id) as subscribers
FROM gv_channels as ch
