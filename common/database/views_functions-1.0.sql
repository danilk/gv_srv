--Views
\i view/vgv_channels.sql


-- Functions

\i function/authorize.sql
\i function/get_sid.sql
\i function/get_uid.sql
\i function/drop_sid.sql

\i function/users/create.sql
\i function/users/load.sql
\i function/users/drop.sql
\i function/users/update.sql
\i function/users/subscribe.sql

\i function/channels/create.sql
\i function/channels/load.sql
\i function/channels/drop.sql
\i function/channels/update.sql
\i function/channels/authorize.sql
\i function/channels/subscibe.sql


