package ptt.api.net;


import java.util.List;

import org.apache.log4j.Logger;

import Ice.Current;

import ptt.api.manager.IChannelKeeper;
import ptt.api.net.slice.*;
import ptt.api.spring.DefaultBeanFactory;
import ptt.api.net.exceptions.SubscriberNotFoundException;

import ptt.api.net.slice.exceptions.GenericException;
import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.slice.exceptions.ChannelNotExistsException;
import ptt.api.net.slice.exceptions.BadChannelDataException;
import ptt.api.net.slice.exceptions.AccessDeniedException;
import ptt.api.net.slice.exceptions.ChannelNameExistsException;
import ptt.api.net.task.ChannelOperations;
import ptt.api.net.task.UserOperations;
/**
 * 
 * @author Danil Krivopustov
 * 
 */
public class ChanelKeeperImpl extends _ChannelKeeperDisp {
  
	private static final long serialVersionUID = -4681504342560827550L;
	private final Logger log = Logger.getLogger(this.getClass());
	
	private volatile MessageManager m_manager;
	
	public ChanelKeeperImpl(MessageManager manager)
    {
	  m_manager = manager;
	}

  synchronized
    public void destroy()
  {
    log.info("Destroying User keeper");
    this.notify();
  }

	private IChannelKeeper getKeeper() {
		return (IChannelKeeper) DefaultBeanFactory.getInstance().getBean(
				IChannelKeeper.class);
	}
	

@Override
public void GetAll_async(AMD_ChannelKeeper_GetAll cb, Ticket sid, Current current) 
{
	m_manager.addTask(ChannelOperations.getChannels(cb, sid, m_manager, current));	
}

@Override
public void GetByWordAndUser_async(AMD_ChannelKeeper_GetByWordAndUser cb,
		String info, Ticket sid, Current current) {
	m_manager.addTask(ChannelOperations.searchUserChannels(cb, m_manager, sid, info, current));
}

@Override
public void GetByWord_async(AMD_ChannelKeeper_GetByWord cb, String info, Ticket sid,
		Current current) {
	m_manager.addTask(ChannelOperations.searchChannels(cb, m_manager, info, sid));
}



@Override
public void Get_async(AMD_ChannelKeeper_Get cb, int channelId, Ticket sid,
		Current current) {
	m_manager.addTask(ChannelOperations.getChannel(cb, m_manager, channelId, sid));
}

@Override
public void Create_async(AMD_ChannelKeeper_Create cb, ChannelData info,
		Ticket sid, Current current) {
	m_manager.addTask(ChannelOperations.createChannel(cb, m_manager, info, sid, current));
}


@Override
public void Drop_async(AMD_ChannelKeeper_Drop cb, int id, Ticket sid,
		Current current) {
	m_manager.addTask(ChannelOperations.deleteChannel(cb, m_manager, id, sid, current));
}


@Override
public void Update_async(AMD_ChannelKeeper_Update cb, ChannelData info,
		Ticket sid, Current current)
{
	m_manager.addTask(ChannelOperations.updateChannel(cb, m_manager, info, sid, current));
}


@Override
public void Join_async(AMD_ChannelKeeper_Join cb, int id, Ticket sid,
		Current current) {
	m_manager.addTask(ChannelOperations.joinChannel(cb, m_manager, id, sid, current));
}


@Override
public void Authorize_async(AMD_ChannelKeeper_Authorize cb, int id,
		String password, Current current)
{
	m_manager.addTask(ChannelOperations.authorizeChannel(cb, m_manager, id, password));
}

@Override
public void Leave_async(AMD_ChannelKeeper_Leave cb, int id, Ticket sid,
		Current current) {
	m_manager.addTask(ChannelOperations.leaveChannel(cb, m_manager, id, sid, current));
}

@Override
public void IsNameUniq_async(AMD_ChannelKeeper_IsNameUniq cb, String name,
		Current current) {
	m_manager.addTask(ChannelOperations.uniqChannelName(cb, m_manager, name));
}

@Override
public void Mute_async(AMD_ChannelKeeper_Mute cb, int channelId, Ticket sid, Current current) {
	m_manager.addTask(ChannelOperations.muteChannel(cb, m_manager, channelId, sid, current));
}

@Override
public void UnMute_async(AMD_ChannelKeeper_UnMute cb, int channelId, Ticket sid, Current current) {
	m_manager.addTask(ChannelOperations.unMuteChannel(cb, m_manager, channelId, sid, current));
}

@Override
public void GetUsers_async(AMD_ChannelKeeper_GetUsers cb, int channelId,
		Current current) {
	m_manager.addTask(ChannelOperations.getUsers(cb, m_manager, channelId, current));	
}

}
