package ptt.api.net;

import ptt.api.net.slice.Buffer;
import ptt.api.net.slice.AMD_ChannelMessageSender_sendBuffer;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice._ChannelMessageSenderDisp;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.AMD_ChannelMessageSender_startTransmission;
import ptt.api.net.slice.AMD_ChannelMessageSender_endTransmission;
import ptt.api.net.slice.AMD_ChannelMessageSender_channelUpdated;
import ptt.api.net.exceptions.SubscriberNotFoundException;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class ChannelMessageSenderI extends _ChannelMessageSenderDisp
{
   private volatile MessageManager m_manager;
   private final Logger log = Logger.getLogger(this.getClass());

   public ChannelMessageSenderI(MessageManager manager)
   {
      m_manager = manager;
   }
   
   @Override
   public void startTransmission_async(AMD_ChannelMessageSender_startTransmission cb, Ticket sid, int channelId, Ice.Current current)
   {
		log.info("start transmission");
    Subscriber subs = null;
    try
    {
      subs = m_manager.getSubscribers().getSubscriber(sid);
    }
    catch(SubscriberNotFoundException ex)
    {
      if(m_manager.restoreSubscriber(current, sid))
      {
        subs = m_manager.getSubscribers().getSubscriber(sid);
      }
    }
		m_manager.addTask(ITask.createSTTask(subs, channelId, cb, m_manager.getSubscribers()));   	   
   }
   
   @Override
   public void endTransmission_async(AMD_ChannelMessageSender_endTransmission cb,Ticket sid, int channelId, int userId, String timestamp, Ice.Current current)
   {
		log.info("end transmission");

    Subscriber subs = null;
    try
    {
      subs = m_manager.getSubscribers().getSubscriber(sid);
    }
    catch(SubscriberNotFoundException ex)
    {
      if(m_manager.restoreSubscriber(current, sid))
      {
        subs = m_manager.getSubscribers().getSubscriber(sid);
      }
    }
		m_manager.addTask(ITask.createETTask(subs, channelId, userId, timestamp, cb, m_manager.getSubscribers()));   
   }

	@Override
    public void sendBuffer_async(AMD_ChannelMessageSender_sendBuffer cb, Buffer msg, int channelId, Ice.Current current)
    {
		log.info("received message");

    Subscriber subs = null;
    try
    {
      subs = m_manager.getSubscribers().getSubscriber(msg.sid);
    }
    catch(SubscriberNotFoundException ex)
    {
      if(m_manager.restoreSubscriber(current, msg.sid))
      {
        subs = m_manager.getSubscribers().getSubscriber(msg.sid);
      }
    }
    msg.timestamp = subs.getTimestamp();
		m_manager.addTask(ITask.createSendTask(subs, msg, channelId, cb, m_manager.getSubscribers()));
    m_manager.addTask(ITask.createStoreMessage(m_manager.getChannelKeeper().getFilepath() + '/' + channelId, subs.getFilename(), msg));
    }

  @Override
  public void channelUpdated_async(AMD_ChannelMessageSender_channelUpdated cb, ChannelData channel, Ice.Current current)
  {
    log.info("somebody has updated channel" + channel.name);

    m_manager.addTask(ITask.createChannelNotification(channel, cb, m_manager.getSubscribers()));
  }
}
