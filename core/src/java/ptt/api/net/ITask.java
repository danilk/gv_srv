package ptt.api.net;

import ptt.api.net.slice.exceptions.GenericException;
import ptt.api.net.slice.AMD_ChannelMessageSender_sendBuffer;
import ptt.api.net.slice.AMD_ChannelMessageSender_startTransmission;
import ptt.api.net.slice.AMD_ChannelMessageSender_endTransmission;
import ptt.api.net.slice.AMD_ChannelMessageSender_channelUpdated;
import ptt.api.net.slice.Buffer;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;

import ptt.api.net.task.GlobalNotification;
import ptt.api.net.task.ChannelNotification;
import ptt.api.net.task.SendLastMessages;
import ptt.api.net.task.StoreMessage;

import org.apache.log4j.Logger;
/**
 * @author lavei
 *
 */
public abstract class ITask {
	
	protected final Logger log = Logger.getLogger(this.getClass());

	public abstract void Complete();
	
	public abstract void Exception(GenericException ex);
	
	public abstract void Notify() throws GenericException;

	public abstract int getThreadId();

/**
 * Factory methods
 */  

	static public ITask createSendTask(Subscriber author, Buffer buffer, int channel, AMD_ChannelMessageSender_sendBuffer cb, Subscribers subs)
	{
		return ChannelNotification.createSendTask(author, buffer, channel, cb, subs);
	}
	
	static public ITask createSTTask(Subscriber author, int channel, AMD_ChannelMessageSender_startTransmission cb, Subscribers subs)
	{
		return ChannelNotification.createSTTask(author, channel, cb, subs);
	}
	
	static public ITask createETTask(Subscriber author, int channel, int userId, String timestamp, AMD_ChannelMessageSender_endTransmission cb, Subscribers subs)
	{
		return ChannelNotification.createETTask(author, channel, userId, timestamp, cb, subs);
	}
	
	static public ITask createChannelNotification(ChannelData channel, AMD_ChannelMessageSender_channelUpdated cb, Subscribers subs)
	{
		return ChannelNotification.createChannelNotification(channel, cb, subs);
	}

	static public ITask createPingNotification(Subscribers subs)
	{
		return GlobalNotification.createPingNotification(subs);
	}

	static public ITask createStoreMessage(String i_path, String i_name, Buffer i_msg)
	{
		return new StoreMessage(i_path, i_name, i_msg);
	}
	static public ITask createSendLastMessages(MessageManager manager, Ticket sid)
	{
		return new SendLastMessages(manager, sid);
	}

}

