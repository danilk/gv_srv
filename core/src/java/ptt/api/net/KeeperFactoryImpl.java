package ptt.api.net;


import java.util.Map;
import java.util.HashMap;
import java.lang.NoSuchMethodException;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;

import Ice.Current;
import ptt.api.net.slice.*;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
@SuppressWarnings("serial")
public class KeeperFactoryImpl extends _KeeperFactoryDisp {
  
	@SuppressWarnings("unused")
  private final Logger log = Logger.getLogger(this.getClass());
  private Map<String, Ice.ObjectPrx> m_keepers = new HashMap<String,Ice.ObjectPrx>();
  private MessageManager m_manager;

  private <I extends Ice.Object, T extends I> Ice.ObjectPrx obtainServant(String name, Class<T> clazz, Ice.Current current)
  {
    if (m_keepers.get(name) == null)
    {

      Ice.Communicator comm = current.adapter.getCommunicator();

      try
      {
        I servant;
        try
        {
          servant = clazz.getConstructor(MessageManager.class).newInstance(m_manager);
        }
        catch(InvocationTargetException e)
        {
          servant = clazz.newInstance();
        }
        catch(NoSuchMethodException e)
        {
          servant = clazz.newInstance();
        }

		    Ice.ObjectPrx obj = current.adapter.add(servant, comm.stringToIdentity(name));
        m_keepers.put(name, obj);
      }
      catch(InstantiationException e)
      {

      }
      catch(IllegalAccessException e)
      {
      }

    }

    return m_keepers.get(name);
  }

  @SuppressWarnings("unused")
  private Ice.Object 
    findServant(String name, Ice.ObjectAdapter adapter)
    {
      Ice.Communicator comm = adapter.getCommunicator();
		  return adapter.find(comm.stringToIdentity(name));
    }

  public KeeperFactoryImpl()
  {
    m_manager = new MessageManager();
  }

	@Override
	public TicketKeeperPrx GetTicket(Current current)
  {
    Ice.ObjectPrx obj = this.<TicketKeeper,TicketKeeperImpl>obtainServant("TicketKeeper", TicketKeeperImpl.class, current);
    return TicketKeeperPrxHelper.uncheckedCast(obj);
	}

	@Override
	public UserKeeperPrx GetUser(Current current)
  {
    Ice.ObjectPrx obj = this.<UserKeeper,UserKeeperImpl>obtainServant("UserKeeper", UserKeeperImpl.class, current);
    return UserKeeperPrxHelper.uncheckedCast(obj);
	}

	@Override
	public ChannelKeeperPrx GetChanel(Current current)
  {
    Ice.ObjectPrx obj = this.<ChannelKeeper,ChanelKeeperImpl>obtainServant("ChannelKeeper", ChanelKeeperImpl.class, current);
    return ChannelKeeperPrxHelper.uncheckedCast(obj);
	}

	@Override
	public SubscribeKeeperPrx GetSubscribe(Current current)
  {
    Ice.ObjectPrx obj = this.<SubscribeKeeper,SubscribeKeeperImpl>obtainServant("SubscribeKeeper", SubscribeKeeperImpl.class, current);
    return SubscribeKeeperPrxHelper.uncheckedCast(obj);
	}

  @Override
  public ChannelMessageSenderPrx GetChannelSender(Ice.Current current)
  {
    Ice.ObjectPrx obj = this.<ChannelMessageSender,ChannelMessageSenderI>obtainServant("ChannelMessageSender", ChannelMessageSenderI.class, current);
    return ChannelMessageSenderPrxHelper.uncheckedCast(obj);
  }

  @Override
  public UserMessageSenderPrx GetUserSender(Ice.Current current)
  {
    Ice.ObjectPrx obj = this.<UserMessageSender,UserMessageSenderI>obtainServant("UserMessageSender", UserMessageSenderI.class, current);
    return UserMessageSenderPrxHelper.uncheckedCast(obj);
  }

  public 
  void destroy()
  {
    m_manager.destroy();
  }
}

