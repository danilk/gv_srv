package ptt.api.net;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.*;

import ptt.api.manager.IChannelKeeper;
import ptt.api.manager.ITicketKeeper;
import ptt.api.manager.IUserKeeper;
import ptt.api.net.TaskDispatcher;
import ptt.api.net.task.*;
import ptt.api.net.Subscribers;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.*;
import ptt.api.spring.DefaultBeanFactory;
import org.apache.log4j.Logger;

public class MessageManager {

  private final Logger log = Logger.getLogger(this.getClass());
  volatile private TaskDispatcher m_dispatcher = new TaskDispatcher();
  volatile private Subscribers m_subscribers = new Subscribers();

  private final ScheduledExecutorService m_scheduler =
	       Executors.newScheduledThreadPool(1);
  
  private final ScheduledFuture<?> m_beeperHandle;
  private final Runnable m_beeper = new Runnable() {
      public void run() { 
    	  m_dispatcher.add(ITask.createPingNotification(m_subscribers)); 
    	  }
  };
  
	public IChannelKeeper getChannelKeeper() {
		return (IChannelKeeper) DefaultBeanFactory.getInstance().getBean(
				IChannelKeeper.class);
	}

	private ITicketKeeper getTicketKeeper() {
		return (ITicketKeeper) DefaultBeanFactory.getInstance().getBean(
				ITicketKeeper.class);
	}

	public IUserKeeper getUserKeeper() {
		return (IUserKeeper) DefaultBeanFactory.getInstance().getBean(
				IUserKeeper.class);
	}
  public
    MessageManager()
    {
      m_subscribers.setManager(this);      
     
      m_dispatcher.init(); 
      m_beeperHandle = m_scheduler.scheduleAtFixedRate(m_beeper, 10, getChannelKeeper().getRepeatInterval(), SECONDS);
    }

  public
    Subscribers getSubscribers()
    {
      return m_subscribers;
    }
  public void addTask(ITask task)
  {
    m_dispatcher.add(task);
  }

  public
     boolean restoreSubscriber(Ice.Current current, Ticket sid)
    {
      String ident = getTicketKeeper().getChannelIdentity(sid); 
      if (ident.equals(""))
      {
        return false;
      }
     
      String ident2 = getTicketKeeper().getUserIdentity(sid); 
      if (ident.equals(""))
      {
        return false;
      }

      log.info("Restore subscriber with sid [" + sid.sid + "] with found identity ["+ ident +"]");

      Ice.Identity identity = current.adapter.getCommunicator().stringToIdentity(ident);
      Ice.Identity identity2 = current.adapter.getCommunicator().stringToIdentity(ident2);
      createSubscriber(current, sid, identity, identity2);
      return true;
    }
  public 
    void createSubscriber(Ice.Current current, Ticket sid, Ice.Identity channel, Ice.Identity user)
    {
		  Ice.ObjectPrx base = current.con.createProxy(channel);
		  ChannelMessageSenderPrx client = ChannelMessageSenderPrxHelper.uncheckedCast(base);

		  Ice.ObjectPrx base2 = current.con.createProxy(user);
		  UserMessageSenderPrx userClient = UserMessageSenderPrxHelper.uncheckedCast(base2);
      getTicketKeeper().setIdentity(sid, current.adapter.getCommunicator().identityToString(channel), current.adapter.getCommunicator().identityToString(user));
      subscribe(client, userClient,  sid);
    }

  private
    void subscribe(ChannelMessageSenderPrx channel, UserMessageSenderPrx user, Ticket sid)
    {

      int userId = getTicketKeeper().getUserId(sid);
      m_subscribers.registerSubscriber(channel, user, sid, userId);

	    List<ChannelData> list = getChannelKeeper().Get(sid);

      for(ChannelData itm : list)
      {
        m_subscribers.addSubscriber(sid, itm);
        actualizeOnlineSubscribers(itm);
        m_dispatcher.add(ITask.createChannelNotification(itm, null, m_subscribers));
      }

      try
      {    
        UserInfo info = getUserKeeper().GetUser(userId);
        m_dispatcher.add(UserOperations.notifyUserUpdate(info, null, m_subscribers));
      }
      catch(ptt.api.net.slice.exceptions.UserNotExistsException ex)
      {
        log.error(ex);
      }
    }

  public 
    void subscribe(int channelId, Ticket sid) throws SubscriberNotFoundException
    {
	      ChannelData info = getChannelKeeper().Get(channelId, sid);
        if (info == null)
          return;

        m_subscribers.addSubscriber(sid, info);
        actualizeOnlineSubscribers(info);
        m_dispatcher.add(ITask.createChannelNotification(info, null, m_subscribers));
    }
  
  public
    void unsubscribe(Ticket sid)
    {
      m_subscribers.removeByTicket(sid);

      try
      {    
        UserInfo info = getUserKeeper().Get(sid);
        m_dispatcher.add(UserOperations.notifyUserUpdate(info, null, m_subscribers));
      }
      catch(ptt.api.net.slice.exceptions.UserNotExistsException ex)
      {
        log.error(ex);
      }
    }
  
  public
    void unsubscribe(int channelId, Ticket sid)
    {
      m_subscribers.unsubscribe(channelId, sid);
      notifyUpdateChannel(channelId);

    }
  
  public void setSubscriberData(List<ChannelData> list, Ticket sid) throws SubscriberNotFoundException
  {
      for (ChannelData item : list)
      {
      	actualizeOnlineSubscribers(item);
        getSubscribers().setSubscriberData(item, sid);
      }
  }

  public void actualizeOnlineSubscribers(List<ChannelData> list)
  {
      for (ChannelData item : list)
      {
      	actualizeOnlineSubscribers(item);
      }
  }

  public void actualizeOnlineSubscribers(ChannelData info)
  {
  	info.onlineCnt = getSubscribers().getByChannel(info.channelId).size();
  }
  
  synchronized
  public void updateChannelData(ChannelData info)
  {
  	for(Subscriber itm : getSubscribers().getByChannel(info.channelId))
    {
      itm.editChannel(info);
    }
  }
  
  public void notifyUpdateChannel(int channelId)
  {
	  ChannelData info = getChannelKeeper().Get(channelId);
	  if (info != null)
	  {
	  	actualizeOnlineSubscribers(info);
	  	m_dispatcher.add(ITask.createChannelNotification(info, null, m_subscribers));
	  }
  }

  public 
  void destroy()
  {
    m_dispatcher._destroy();
	  m_beeperHandle.cancel(true);
	  m_scheduler.shutdown();
  }
}
