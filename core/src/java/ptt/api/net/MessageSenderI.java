package ptt.api.net;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import ptt.api.net.slice.Buffer;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.ChannelMessageSenderPrx;
import ptt.api.net.slice.UserMessageSenderPrx;
import ptt.api.net.slice._MessageSenderDisp;
import ptt.api.net.slice.Ticket;
import ptt.api.net.exceptions.SubscriberNotFoundException;

import org.apache.log4j.Logger;

import Ice.Current;

@SuppressWarnings("serial")
public class MessageSenderI extends _MessageSenderDisp {
	private volatile MessageManager m_manager;
	private final Logger log = Logger.getLogger(this.getClass());

	private Map<String, Ice.ObjectPrx> m_keepers = new HashMap<String, Ice.ObjectPrx>();

	public MessageSenderI(MessageManager manager) {
		m_manager = manager;
	}

	@Override
	public UserMessageSenderPrx getUserSender(Current __current) {
		log.error("Called getUserSender");
		return null;
	}

	@Override
	public ChannelMessageSenderPrx getChannelSender(Current __current) {
		log.error("Called getChannelSender");
		return null;
	}
}
