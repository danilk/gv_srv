package ptt.api.net;


import org.apache.log4j.Logger;

import Ice.Current;
import Ice.Identity;
import ptt.api.net.slice.*;
import ptt.api.net.task.UserOperations;
import ptt.api.net.MessageManager;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public class SubscribeKeeperImpl extends _SubscribeKeeperDisp {
  
	private static final long serialVersionUID = -4681504342560827550L;
	private final Logger log = Logger.getLogger(this.getClass());
	private MessageManager m_manager;
  	private java.text.SimpleDateFormat m_date = new java.text.SimpleDateFormat("yyyyMMdd_HHmmss");

  public
    SubscribeKeeperImpl(MessageManager manager)
    {
      m_manager = manager;
    }

  synchronized
    public void destroy()
  {
    log.info("Destroying User keeper");
    this.notify();
  }

	@Override
	public void Subscribe_async(AMD_SubscribeKeeper_Subscribe __cb,
			Identity channel, Identity user, Ticket sid, Current current) {
    /// TODO move identity to the Ticket 
		log.info("subscribing client " + current.adapter.getCommunicator().identityToString(channel)
				+ "'[" + sid.sid + "]" + "["+ current.adapter.getCommunicator().identityToString(current.id) + "]");
		m_manager.addTask(UserOperations.subscribe(__cb, m_manager, current, sid, channel, user));
		m_manager.addTask(ITask.createSendLastMessages(m_manager, sid));
	}

	@Override
	public void Unsubscribe_async(AMD_SubscribeKeeper_Unsubscribe __cb,
			Identity ident, Ticket sid, Current current) {
		log.info("unsubscribing client " + current.adapter.getCommunicator().identityToString(ident)
				+ "'");
		m_manager.addTask(UserOperations.unsubscribe(__cb, m_manager, sid));
//		m_manager.unsubscribe(sid);
//		__cb.ice_response();
	}

	@Override
	public String getTimestamp(Current __current) {
		return m_date.format(new java.util.Date());
	}

}

