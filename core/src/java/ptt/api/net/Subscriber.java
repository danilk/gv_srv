package ptt.api.net;

import ptt.api.net.slice.ChannelMessageSenderPrx;
import ptt.api.net.slice.UserMessageSenderPrx;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.UserInfo;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.log4j.Logger;


public class Subscriber {

	private ChannelMessageSenderPrx m_channelSender;
	private UserMessageSenderPrx m_userSender;
	private Ticket m_ticket;
  private int m_userId;
  /// channelId | channel broad
	private Map<Integer, ChannelData> m_channels = new HashMap<Integer, ChannelData>();
	private Map<Integer, UserInfo> m_contacts = new HashMap<Integer, UserInfo>();
	private final Logger log = Logger.getLogger(this.getClass());
  private java.text.SimpleDateFormat m_date = new java.text.SimpleDateFormat("yyyyMMdd_HHmmss");
  private String m_lastTS;

	public Subscriber(ChannelMessageSenderPrx channel, UserMessageSenderPrx user, Ticket ticket, int userId) {
		m_channelSender = channel;
		m_userSender = user;
		m_ticket = ticket;
    m_userId = userId;
	}

	public ChannelMessageSenderPrx getChannelSender() {
		return m_channelSender;
	}

	public UserMessageSenderPrx getUserSender() {
		return m_userSender;
	}

	public Ticket getTicket() {
		return m_ticket;
	}

	public int getUserId() {
		return m_userId;
	}

  public void setTimestamp()
  {
    m_lastTS = m_date.format(new java.util.Date());
  }

  public String getTimestamp()
  {
    return m_lastTS;
  }

  public String getFilename()
  {
    return m_lastTS + '_' + m_userId;
  }

  public void actualize(ChannelData other)
  {
    ChannelData channel = m_channels.get(other.channelId);
    if (channel == null)
      return;
    other.channelState = channel.channelState;
    other.blocked = channel.blocked;
    other.isOwner = channel.isOwner;
    other.authorized = channel.authorized || channel.isOwner;
    /// set his rights
    other.canRead = canRead(other.channelId);
    other.canWrite = canWrite(other.channelId);
  }

  public boolean isOwner(int id)
  {
    ChannelData channel = m_channels.get(id);
    return channel != null && channel.isOwner;
  }

  public boolean canRead(int id, Subscriber author)
  {
    ChannelData channel = m_channels.get(id);
    return canRead(id) && (channel.methodOfCommunication != 1 || author.isOwner(id));
  }

  public boolean canRead(int id)
  {
    ChannelData channel = m_channels.get(id);
    return channel != null && channel.channelState != 7
      && (
          channel.isOwner
          || (!channel.blocked
            &&( (channel.typeOfBroadcast <= 2) // Open or Mixed or Open with authorization
              || (channel.typeOfBroadcast == 3 && channel.authorized ) // Authorization 
              || (channel.typeOfBroadcast == 4) // Closed
              || (channel.typeOfBroadcast == 5) // Closed with Authorization 
              )
            )
          );
  }

  public boolean canWrite(int id)
  {
    ChannelData channel = m_channels.get(id);

    return channel != null && channel.channelState != 7
      && (
          channel.isOwner
          || (!channel.blocked
            &&( (channel.typeOfBroadcast <= 1) // Open or Mixed
              || (channel.typeOfBroadcast == 2 && channel.authorized ) // Open with authorization 
              || (channel.typeOfBroadcast == 3 && channel.authorized ) // Authorization 
              || (channel.typeOfBroadcast == 4) // Closed
              || (channel.typeOfBroadcast == 5 && channel.authorized ) // Closed with Authorization 
              )
            )
          );
  }

  public void addChannel(ChannelData data)
  {
    m_channels.put(data.channelId, (ChannelData)data.clone());
  }

  public void editChannel(ChannelData data)
  {
    m_channels.get(data.channelId).typeOfBroadcast = data.typeOfBroadcast;
    m_channels.get(data.channelId).methodOfCommunication = data.methodOfCommunication;
  }

  public void editChannel(int id,  int channelState)
  {
    m_channels.get(id).channelState = channelState;
  }

  public void removeChannel(int id)
  {
    m_channels.remove(id);
  }

  public boolean containsChannel(int id)
  {
    return m_channels.containsKey(id);
  }

	public Iterator<Integer> getChannels() {
		return m_channels.keySet().iterator();
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (obj.getClass() != getClass())
			return false;

		Subscriber rhs = (Subscriber) obj;
		return new EqualsBuilder().append(m_ticket.sid, rhs.m_ticket.sid)
				.isEquals();
	}
	
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            // if deriving: appendSuper(super.hashCode()).
            append(m_ticket.sid).
            toHashCode();
    }

}
