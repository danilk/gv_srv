package ptt.api.net;

import ptt.api.net.slice.Buffer;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.ChannelMessageSenderPrx;
import ptt.api.net.slice.UserMessageSenderPrx;
import ptt.api.net.task.INotification;
import ptt.api.net.task.ChannelNotification;
import ptt.api.net.exceptions.SubscriberNotFoundException;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.lang.Integer;

import org.apache.log4j.Logger;

public class Subscribers {

  private volatile Map<Integer, LinkedList<Subscriber>> m_subscribers = new HashMap<Integer, LinkedList<Subscriber>>();
  private volatile Map<String, Subscriber> m_registeredSubscribers = new HashMap<String, Subscriber>();
  private volatile Map<Integer, String> m_subscribersIdx = new HashMap<Integer, String>();
  private final Logger log = Logger.getLogger(this.getClass());
  volatile private MessageManager m_manager;

 public
    void registerSubscriber(ChannelMessageSenderPrx channel, UserMessageSenderPrx user, Ticket sid, int userId)
    {
	    log.info("register subs with" + sid.sid + " user id = " + userId);
      m_registeredSubscribers.put(sid.sid, new Subscriber(channel, user, sid, userId));
      m_subscribersIdx.put(userId, sid.sid);
    }

 public
  void addSubscriber(Ticket sid, ChannelData channel) throws SubscriberNotFoundException
  {
    Subscriber subs = m_registeredSubscribers.get(sid.sid);

    if (subs == null )
    {
      throw new SubscriberNotFoundException();
    }
    if (subs.containsChannel(channel.channelId))
    {
      log.debug("This ["+sid.sid+"] already has ["+channel.channelId+"]");
      return;
    }

    log.debug("Adding ["+ sid.sid + "] ch ["+channel.channelId + "]");
    subs.addChannel(channel);

    LinkedList<Subscriber> list = getByChannel(channel.channelId);
    if (!list.contains(subs))
    {
    	list.add(subs);
        log.debug("Adding ["+ sid.sid + "] to channels ch ["+channel.channelId + "]");
    }
  }

 public
  void editSubscriber(Integer channel, Ticket sid, int channelState) throws SubscriberNotFoundException
  {
    Subscriber subs = m_registeredSubscribers.get(sid.sid);

    if (subs == null)
    {
      throw new SubscriberNotFoundException();
    }

    log.debug("Edit ["+ sid.sid + "] ch ["+channel + "]");
    subs.editChannel(channel, channelState);
  }

public
  void editSubscriber(ChannelData data, Ticket sid) throws SubscriberNotFoundException
  {
    Subscriber subs = m_registeredSubscribers.get(sid.sid);

    if (subs == null)
    {
      throw new SubscriberNotFoundException();
    }

    log.debug("changing["+ sid.sid + "] ch["+data.channelId + "]");
    subs.editChannel(data);
  }

public 
   void setSubscriberData(ChannelData channel, Ticket sid) throws SubscriberNotFoundException
   {
     if (m_registeredSubscribers.get(sid.sid) == null)
     {
       throw new SubscriberNotFoundException();
     }
     m_registeredSubscribers.get(sid.sid).actualize(channel);

     log.debug("after setting data can write is :" + channel.canWrite);
   }

	public LinkedList<Subscriber> getByChannel(Integer channel) {
		if (m_subscribers.get(channel) == null) {
			m_subscribers.put(channel, new LinkedList<Subscriber>());
		}

		return m_subscribers.get(channel);
	}

  public Subscriber getSubscriber(int userId) throws SubscriberNotFoundException
  {  
     String sid = m_subscribersIdx.get(userId);
     if (sid == null)
     {
       log.debug("Can't find subscriber by id :" + userId);
       throw new SubscriberNotFoundException();
     }
     Subscriber sub = m_registeredSubscribers.get(sid);
     if (sub == null)
     {
       log.debug("Can't find subscriber by sid :" + sid);
       throw new SubscriberNotFoundException();
     }
     return sub;
  }
  public Subscriber getSubscriber(Ticket sid) throws SubscriberNotFoundException
  {
     Subscriber sub = m_registeredSubscribers.get(sid.sid);
     if (sub == null)
     {
       log.debug("Can't find subscriber just by sid :" + sid);
       throw new SubscriberNotFoundException();
     }
     return sub;
  }

public void removeByTicket(Ticket sid)
  {
		removeSubscriber(m_registeredSubscribers.get(sid.sid));
	}

public
    void unsubscribe(int channelId, Ticket sid)
    {
      Subscriber subs = m_registeredSubscribers.get(sid.sid);
      if (subs == null)
      {
      	log.error("UNSUBS: Can't find a subscriber by " + sid.sid);    	
        return;
      }

      m_subscribers.get(channelId).remove(subs);
      subs.removeChannel(channelId);

    }
  
private
    void removeSubscriber(Subscriber client)
  {
    if (client == null)
    {
      log.error("Can't remove null subscrier.");    	
      return;
    }

	  log.info("remove subscriber " + client.getTicket().sid);
    Iterator<Integer> it = client.getChannels();
    while(it.hasNext())
    {
      Integer channel = it.next();
      m_subscribers.get(channel).remove(client);
      m_manager.notifyUpdateChannel(channel);
    }
    m_registeredSubscribers.remove(client.getTicket().sid);
    m_subscribersIdx.remove(client.getUserId());
  }
  
  private void sendTo(INotification task, Subscriber item)
  {
    try
    {
      task.Execute(item);
    }
    catch(Ice.ConnectionLostException ex)
    {
      log.info("Grave zombi " + item.getTicket().sid + ":" + ex.getMessage());
      ex.printStackTrace();
      removeSubscriber(item);
    }
    catch(Exception ex)
    {
      log.info("Grave zombi " + item.getTicket().sid + ":" + ex.getMessage());
      ex.printStackTrace();
      removeSubscriber(item);
    }
  }

  public
    void sendNotification(ChannelNotification task, int channelId)
    {
      if (m_subscribers.get(channelId) ==null)
      {
        return;
      }
      if (!task.isValid())
      {
        log.debug("skipped because isn't valid.");
        return;
      }
      
   //TODO this was done to avoid ConcurentModificationException 
      Subscriber[] subs = m_subscribers.get(channelId).toArray(new Subscriber[m_subscribers.get(channelId).size()]);
      for (Subscriber item : subs)
      {
        sendTo(task, item);
      }  
    }
  
  public
  void sendNotification(INotification task)
  {
    if (m_registeredSubscribers ==null)
    {
      return;
    }
  
   //TODO this was done to avoid ConcurentModificationException 
    Subscriber[] subs = m_registeredSubscribers.values().toArray(new Subscriber[m_registeredSubscribers.values().size()]);

    for(Subscriber item : subs)
    {
      sendTo(task, item);
    }
  }
  
  public synchronized
  void setManager(MessageManager manager)
  {
	  m_manager = manager;
  }

}
