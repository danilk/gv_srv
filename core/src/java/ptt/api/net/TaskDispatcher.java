package ptt.api.net;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TaskDispatcher
{

  private List<WorkQueue> m_workers;
  private final Logger log = Logger.getLogger(this.getClass());

  private WorkQueue getFreeWorker()
  {

    log.debug("Looking for free worker..");
    WorkQueue freeWorker = null;

    for(WorkQueue worker : m_workers)
    {
      if (freeWorker == null)
      {
        freeWorker = worker;
      }
      else
      {
        if (freeWorker.size() > worker.size())
        {
          freeWorker = worker;
        }
      }
    }

    return freeWorker;
  }

  public
    TaskDispatcher()
    {
      int count = 4; //TODO should be fixed
      m_workers = new ArrayList<WorkQueue>();
      for(int i = 0; i< count; ++i)
      {
        m_workers.add(new WorkQueue());
      }
      log.info("Created " + count + " working threads.");
    }

  public void init()
  {
    for(WorkQueue worker : m_workers)
    {
      worker.start();
    }
    log.info("All workers have been started.");
  }

  public void add(ITask task)
  {
    int id = task.getThreadId();

    if (id == 0)
    {
      getFreeWorker().add(task);
    }
    else
    {
      log.debug("Using worker number:[" + (id%m_workers.size()) + "]");
      m_workers.get(id%m_workers.size()).add(task);
    }
  }

  public void _destroy()
    {
      log.info("Shutting down workers...");
      for(WorkQueue worker : m_workers)
      {
        try
        {
          worker.join(5000);
        }
        catch(InterruptedException ex)
        {
        }
        worker._destroy();
      }
    }
}
