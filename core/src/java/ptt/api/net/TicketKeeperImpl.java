package ptt.api.net;


import org.apache.log4j.Logger;

import Ice.Current;
import ptt.api.manager.ITicketKeeper;
import ptt.api.net.slice.*;
import ptt.api.spring.DefaultBeanFactory;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public class TicketKeeperImpl extends _TicketKeeperDisp {
  
	private static final long serialVersionUID = -4681504342560827550L;
	private final Logger log = Logger.getLogger(this.getClass());

	private ITicketKeeper getKeeper() {
		return (ITicketKeeper) DefaultBeanFactory.getInstance().getBean(
				ITicketKeeper.class);
	}

  synchronized
    public void destroy()
  {
    log.info("Destroying Ticket keeper");
    this.notify();
  }

	@Override
	public void Get_async(AMD_TicketKeeper_Get __cb, Credential credentialInfo,
			Current __current) {
		__cb.ice_response(getKeeper().Get(credentialInfo));
	}

	@Override
	public void Drop_async(AMD_TicketKeeper_Drop __cb, Ticket sid,
			Current __current) {
		getKeeper().Drop(sid);
		__cb.ice_response();
	}
}

