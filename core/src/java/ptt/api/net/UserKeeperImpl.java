package ptt.api.net;


import org.apache.log4j.Logger;

import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.task.UserOperations;
import Ice.Current;
/**
 * 
 * @author Danil Krivopustov
 * 
 */
public class UserKeeperImpl extends _UserKeeperDisp {
  
	private static final long serialVersionUID = -4681504342560827550L;
	private final Logger log = Logger.getLogger(this.getClass());

	synchronized public void destroy() {
		log.info("Destroying User keeper");
		this.notify();
	}

	private volatile MessageManager m_manager;
	
	public UserKeeperImpl(MessageManager manager)
    {
	  m_manager = manager;
	}

	@Override
	public void IsLoginUniq_async(AMD_UserKeeper_IsLoginUniq __cb,
			String login, Current __current) 
	{
		m_manager.addTask(UserOperations.isLoginUniq(__cb, login));
	}
	
	@Override
	public void Register_async(AMD_UserKeeper_Register __cb, UserInfo info,
			Current __current)
	{
		m_manager.addTask(UserOperations.registerUser(__cb, info));
	}

	@Override
	public void Get_async(AMD_UserKeeper_Get __cb, Ticket sid, Current __current)
	{
		m_manager.addTask(UserOperations.getUserInfo(__cb, sid));
	}

	@Override
	public void Update_async(AMD_UserKeeper_Update __cb, Ticket sid,
			UserInfo info, Current __current) 
	{
		m_manager.addTask(UserOperations.updateUser(__cb, info, sid));
	}

	@Override
	public void Drop_async(AMD_UserKeeper_Drop __cb, int id, Current __current)
	{
		m_manager.addTask(UserOperations.deleteUser(__cb, id));
	}

	@Override
	public void GetUser_async(AMD_UserKeeper_GetUser __cb, int id,
			Current __current) throws UserNotExistsException {
		m_manager.addTask(UserOperations.getUserInfo(__cb, m_manager, id));
	}

	@Override
	public void GetByWord_async(AMD_UserKeeper_GetByWord __cb, String info,
			Current __current) {
		m_manager.addTask(UserOperations.searchUser(__cb, info));		
	}

	@Override
	public void GetContactList_async(AMD_UserKeeper_GetContactList __cb,
			Ticket sid, Current __current) {
		m_manager.addTask(UserOperations.getContacts(__cb,m_manager, sid));		
	}

	@Override
	public void AddToList_async(AMD_UserKeeper_AddToList __cb, Ticket sid,
			int id, Current __current) {
		m_manager.addTask(UserOperations.addToContacts( __cb, m_manager, sid, id));				
	}

	@Override
	public void RemoveFromList_async(AMD_UserKeeper_RemoveFromList __cb,
			Ticket sid, int id, Current __current) {
		m_manager.addTask(UserOperations.removeFromContacts(__cb, sid, id));		
	}
  
  @Override
    public void Mute_async(AMD_UserKeeper_Mute cb, int userId, Ticket sid, Current current) {
      m_manager.addTask(UserOperations.muteUser(cb, m_manager, userId, sid, current));
    }

  @Override
    public void UnMute_async(AMD_UserKeeper_UnMute cb, int userId, Ticket sid, Current current) {
      m_manager.addTask(UserOperations.unMuteUser(cb, m_manager, userId, sid, current));
    }

}

