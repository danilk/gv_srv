package ptt.api.net;

import ptt.api.net.slice.AMD_UserMessageSender_endTransmission;
import ptt.api.net.slice.AMD_UserMessageSender_sendBuffer;
import ptt.api.net.slice.AMD_UserMessageSender_startTransmission;
import ptt.api.net.slice.AMD_UserMessageSender_userUpdated;
import ptt.api.net.slice.Buffer;
import ptt.api.net.slice.AMD_UserMessageSender_sendBuffer;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice._MessageSenderDisp;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.AMD_UserMessageSender_startTransmission;
import ptt.api.net.slice.AMD_UserMessageSender_endTransmission;
import ptt.api.net.slice.AMD_UserMessageSender_userUpdated;
import ptt.api.net.slice._UserMessageSenderDisp;
import ptt.api.net.task.UserOperations;
import ptt.api.net.exceptions.SubscriberNotFoundException;

import org.apache.log4j.Logger;

import Ice.Current;

@SuppressWarnings("serial")
public class UserMessageSenderI extends _UserMessageSenderDisp {
	private volatile MessageManager m_manager;
	private final Logger log = Logger.getLogger(this.getClass());

	public UserMessageSenderI(MessageManager manager) {
		m_manager = manager;
	}

	@Override
	public void userUpdated_async(AMD_UserMessageSender_userUpdated cb,
			UserInfo info, Ice.Current current) {
	    log.info("user changed his state" + info.login);

	    m_manager.addTask(UserOperations.notifyUserUpdate(info, cb, m_manager.getSubscribers()));

	}

	@Override
	public void startTransmission_async(
			AMD_UserMessageSender_startTransmission __cb, Ticket sid,
			int userId, Current current) {
		log.info("start user2user transmission");
		Subscriber subs = null;
		try {
			subs = m_manager.getSubscribers().getSubscriber(sid);
		} catch (SubscriberNotFoundException ex) {
			if (m_manager.restoreSubscriber(current, sid)) {
				subs = m_manager.getSubscribers().getSubscriber(sid);
			}
		}
		Subscriber receiver = null;
		try {
			receiver = m_manager.getSubscribers().getSubscriber(userId);
		} catch (SubscriberNotFoundException ex) {
			log.error("Can't find subscriber by userid = " + userId);
			__cb.ice_exception(ex);
			return;
		}
		m_manager.addTask(UserOperations.startTransmission(__cb, subs, receiver));
	}

	@Override
	public void endTransmission_async(
			AMD_UserMessageSender_endTransmission __cb, Ticket sid, int userId, String timestamp,
			Current current) {

		log.info("end user2user transmission");
		Subscriber subs = null;
		try {
			subs = m_manager.getSubscribers().getSubscriber(sid);
		} catch (SubscriberNotFoundException ex) {
			if (m_manager.restoreSubscriber(current, sid)) {
				subs = m_manager.getSubscribers().getSubscriber(sid);
			}
		}
		Subscriber receiver = null;
		try {
			receiver = m_manager.getSubscribers().getSubscriber(userId);
		} catch (SubscriberNotFoundException ex) {
			log.error("Can't find subscriber by userid = " + userId);
			__cb.ice_exception(ex);
			return;
		}
		 m_manager.addTask(UserOperations.endTransmission(__cb, subs,
		 receiver));
	}

	@Override
	public void sendBuffer_async(AMD_UserMessageSender_sendBuffer __cb,
			Buffer msg, int userId, Current current) {

		log.info("send message user2user");
		Subscriber subs = null;
		try {
			subs = m_manager.getSubscribers().getSubscriber(msg.sid);
		} catch (SubscriberNotFoundException ex) {
			if (m_manager.restoreSubscriber(current, msg.sid)) {
				subs = m_manager.getSubscribers().getSubscriber(msg.sid);
			}
		}
		Subscriber receiver = null;
		try {
			receiver = m_manager.getSubscribers().getSubscriber(userId);
		} catch (SubscriberNotFoundException ex) {
			log.error("Can't find subscriber by userid = " + userId);
			__cb.ice_exception(ex);
			return;
		}
    msg.timestamp = subs.getTimestamp();
		 m_manager.addTask(UserOperations.sendMessage(__cb, msg, subs,
		 receiver));
     m_manager.addTask(ITask.createStoreMessage(m_manager.getUserKeeper().getFilepath() + '/' + userId, subs.getFilename(), msg));
	}
}
