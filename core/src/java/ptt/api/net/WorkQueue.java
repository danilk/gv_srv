package ptt.api.net;

import org.apache.log4j.Logger;

import ptt.api.net.slice.exceptions.GenericException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;



public class WorkQueue extends Thread
{
  private BlockingQueue<ITask> _messages = new LinkedBlockingQueue<ITask>();
  private boolean _done = false;
  private final Logger log = Logger.getLogger(this.getClass());

  public int size()
  {
    return _messages.size();
  }
  public  
    void run()
    {
      while(!_done)
      {
          try
          {
            ITask task = _messages.take();
            log.info("Process task" + task);

          /// DO stuff
          // send part of message to the subscribers
           try
           {
             task.Notify();
             task.Complete();
           }
           catch(GenericException ex)
           {
             task.Exception(ex);
             log.error("Task has error");
             log.error(ex.getMessage());
           }
          log.info("Complete task" + task);
          }
          catch(java.lang.Throwable  ex)
          {
            log.error("Queue has error");
            log.error(ex);
            log.error(ex.getMessage());
          }
      }

    }

  public  
    void add(ITask task)
    {
      if (_done)
      {
        log.error("What's the fuck?!!");
        task.Exception(new GenericException("Queue is sutting down"));
        return;
      }
      try
      {
        log.debug("Adding task to [" + _messages.size() + "] ones.");
        _messages.put(task);
      }
      catch(InterruptedException ex)
      {
        task.Exception(new GenericException(ex.toString()));
        log.error(ex.getMessage());
      }
    }

  public synchronized
    void _destroy()
    {
      if (!_done)
      {
        notify();
      }
      _done = true;
    }
}
