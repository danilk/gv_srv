// **********************************************************************
//
// Copyright (c) 2003-2011 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.4.2
//
// <auto-generated>
//
// Generated from file `Callback_ChannelKeeper_GetByWord.java'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package ptt.api.net.slice;

public abstract class Callback_ChannelKeeper_GetByWord extends Ice.TwowayCallback
{
    public abstract void response(ChannelData[] __ret);

    public final void __completed(Ice.AsyncResult __result)
    {
        ChannelKeeperPrx __proxy = (ChannelKeeperPrx)__result.getProxy();
        ChannelData[] __ret = null;
        try
        {
            __ret = __proxy.end_GetByWord(__result);
        }
        catch(Ice.LocalException __ex)
        {
            exception(__ex);
            return;
        }
        response(__ret);
    }
}
