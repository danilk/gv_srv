// **********************************************************************
//
// Copyright (c) 2003-2011 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.4.2
//
// <auto-generated>
//
// Generated from file `TicketKeeperPrxHelper.java'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package ptt.api.net.slice;

public final class TicketKeeperPrxHelper extends Ice.ObjectPrxHelperBase implements TicketKeeperPrx
{
    public void
    Drop(Ticket sid)
    {
        Drop(sid, null, false);
    }

    public void
    Drop(Ticket sid, java.util.Map<String, String> __ctx)
    {
        Drop(sid, __ctx, true);
    }

    private void
    Drop(Ticket sid, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __delBase = __getDelegate(false);
                _TicketKeeperDel __del = (_TicketKeeperDel)__delBase;
                __del.Drop(sid, __ctx);
                return;
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __cnt = __handleExceptionWrapperRelaxed(__delBase, __ex, null, __cnt);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    private static final String __Drop_name = "Drop";

    public Ice.AsyncResult begin_Drop(Ticket sid)
    {
        return begin_Drop(sid, null, false, null);
    }

    public Ice.AsyncResult begin_Drop(Ticket sid, java.util.Map<String, String> __ctx)
    {
        return begin_Drop(sid, __ctx, true, null);
    }

    public Ice.AsyncResult begin_Drop(Ticket sid, Ice.Callback __cb)
    {
        return begin_Drop(sid, null, false, __cb);
    }

    public Ice.AsyncResult begin_Drop(Ticket sid, java.util.Map<String, String> __ctx, Ice.Callback __cb)
    {
        return begin_Drop(sid, __ctx, true, __cb);
    }

    public Ice.AsyncResult begin_Drop(Ticket sid, Callback_TicketKeeper_Drop __cb)
    {
        return begin_Drop(sid, null, false, __cb);
    }

    public Ice.AsyncResult begin_Drop(Ticket sid, java.util.Map<String, String> __ctx, Callback_TicketKeeper_Drop __cb)
    {
        return begin_Drop(sid, __ctx, true, __cb);
    }

    private Ice.AsyncResult begin_Drop(Ticket sid, java.util.Map<String, String> __ctx, boolean __explicitCtx, IceInternal.CallbackBase __cb)
    {
        IceInternal.OutgoingAsync __result = new IceInternal.OutgoingAsync(this, __Drop_name, __cb);
        try
        {
            __result.__prepare(__Drop_name, Ice.OperationMode.Idempotent, __ctx, __explicitCtx);
            IceInternal.BasicStream __os = __result.__os();
            __os.writeObject(sid);
            __os.writePendingObjects();
            __os.endWriteEncaps();
            __result.__send(true);
        }
        catch(Ice.LocalException __ex)
        {
            __result.__exceptionAsync(__ex);
        }
        return __result;
    }

    public void end_Drop(Ice.AsyncResult __result)
    {
        __end(__result, __Drop_name);
    }

    public Ticket
    Get(Credential credentialInfo)
    {
        return Get(credentialInfo, null, false);
    }

    public Ticket
    Get(Credential credentialInfo, java.util.Map<String, String> __ctx)
    {
        return Get(credentialInfo, __ctx, true);
    }

    private Ticket
    Get(Credential credentialInfo, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("Get");
                __delBase = __getDelegate(false);
                _TicketKeeperDel __del = (_TicketKeeperDel)__delBase;
                return __del.Get(credentialInfo, __ctx);
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    private static final String __Get_name = "Get";

    public Ice.AsyncResult begin_Get(Credential credentialInfo)
    {
        return begin_Get(credentialInfo, null, false, null);
    }

    public Ice.AsyncResult begin_Get(Credential credentialInfo, java.util.Map<String, String> __ctx)
    {
        return begin_Get(credentialInfo, __ctx, true, null);
    }

    public Ice.AsyncResult begin_Get(Credential credentialInfo, Ice.Callback __cb)
    {
        return begin_Get(credentialInfo, null, false, __cb);
    }

    public Ice.AsyncResult begin_Get(Credential credentialInfo, java.util.Map<String, String> __ctx, Ice.Callback __cb)
    {
        return begin_Get(credentialInfo, __ctx, true, __cb);
    }

    public Ice.AsyncResult begin_Get(Credential credentialInfo, Callback_TicketKeeper_Get __cb)
    {
        return begin_Get(credentialInfo, null, false, __cb);
    }

    public Ice.AsyncResult begin_Get(Credential credentialInfo, java.util.Map<String, String> __ctx, Callback_TicketKeeper_Get __cb)
    {
        return begin_Get(credentialInfo, __ctx, true, __cb);
    }

    private Ice.AsyncResult begin_Get(Credential credentialInfo, java.util.Map<String, String> __ctx, boolean __explicitCtx, IceInternal.CallbackBase __cb)
    {
        __checkAsyncTwowayOnly(__Get_name);
        IceInternal.OutgoingAsync __result = new IceInternal.OutgoingAsync(this, __Get_name, __cb);
        try
        {
            __result.__prepare(__Get_name, Ice.OperationMode.Normal, __ctx, __explicitCtx);
            IceInternal.BasicStream __os = __result.__os();
            __os.writeObject(credentialInfo);
            __os.writePendingObjects();
            __os.endWriteEncaps();
            __result.__send(true);
        }
        catch(Ice.LocalException __ex)
        {
            __result.__exceptionAsync(__ex);
        }
        return __result;
    }

    public Ticket end_Get(Ice.AsyncResult __result)
    {
        Ice.AsyncResult.__check(__result, this, __Get_name);
        if(!__result.__wait())
        {
            try
            {
                __result.__throwUserException();
            }
            catch(Ice.UserException __ex)
            {
                throw new Ice.UnknownUserException(__ex.ice_name(), __ex);
            }
        }
        TicketHolder __ret = new TicketHolder();
        IceInternal.BasicStream __is = __result.__is();
        __is.startReadEncaps();
        __is.readObject(__ret);
        __is.readPendingObjects();
        __is.endReadEncaps();
        return __ret.value;
    }

    public static TicketKeeperPrx
    checkedCast(Ice.ObjectPrx __obj)
    {
        TicketKeeperPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (TicketKeeperPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    TicketKeeperPrxHelper __h = new TicketKeeperPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static TicketKeeperPrx
    checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        TicketKeeperPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (TicketKeeperPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    TicketKeeperPrxHelper __h = new TicketKeeperPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static TicketKeeperPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        TicketKeeperPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    TicketKeeperPrxHelper __h = new TicketKeeperPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static TicketKeeperPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        TicketKeeperPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    TicketKeeperPrxHelper __h = new TicketKeeperPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static TicketKeeperPrx
    uncheckedCast(Ice.ObjectPrx __obj)
    {
        TicketKeeperPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (TicketKeeperPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                TicketKeeperPrxHelper __h = new TicketKeeperPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static TicketKeeperPrx
    uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        TicketKeeperPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            TicketKeeperPrxHelper __h = new TicketKeeperPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::ptt::api::net::slice::TicketKeeper"
    };

    public static String
    ice_staticId()
    {
        return __ids[1];
    }

    protected Ice._ObjectDelM
    __createDelegateM()
    {
        return new _TicketKeeperDelM();
    }

    protected Ice._ObjectDelD
    __createDelegateD()
    {
        return new _TicketKeeperDelD();
    }

    public static void
    __write(IceInternal.BasicStream __os, TicketKeeperPrx v)
    {
        __os.writeProxy(v);
    }

    public static TicketKeeperPrx
    __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            TicketKeeperPrxHelper result = new TicketKeeperPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }
}
