// **********************************************************************
//
// Copyright (c) 2003-2011 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.4.2
//
// <auto-generated>
//
// Generated from file `_AMD_ChannelKeeper_Create.java'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package ptt.api.net.slice;

final class _AMD_ChannelKeeper_Create extends IceInternal.IncomingAsync implements AMD_ChannelKeeper_Create
{
    public
    _AMD_ChannelKeeper_Create(IceInternal.Incoming in)
    {
        super(in);
    }

    public void
    ice_response(int __ret)
    {
        if(__validateResponse(true))
        {
            try
            {
                IceInternal.BasicStream __os = this.__os();
                __os.writeInt(__ret);
            }
            catch(Ice.LocalException __ex)
            {
                ice_exception(__ex);
            }
            __response(true);
        }
    }

    public void
    ice_exception(java.lang.Exception ex)
    {
        try
        {
            throw ex;
        }
        catch(ptt.api.net.slice.exceptions.BadChannelDataException __ex)
        {
            if(__validateResponse(false))
            {
                __os().writeUserException(__ex);
                __response(false);
            }
        }
        catch(ptt.api.net.slice.exceptions.ChannelNameExistsException __ex)
        {
            if(__validateResponse(false))
            {
                __os().writeUserException(__ex);
                __response(false);
            }
        }
        catch(ptt.api.net.slice.exceptions.UserNotExistsException __ex)
        {
            if(__validateResponse(false))
            {
                __os().writeUserException(__ex);
                __response(false);
            }
        }
        catch(java.lang.Exception __ex)
        {
            super.ice_exception(__ex);
        }
    }
}
