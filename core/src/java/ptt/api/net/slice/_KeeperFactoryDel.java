// **********************************************************************
//
// Copyright (c) 2003-2011 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.4.2
//
// <auto-generated>
//
// Generated from file `_KeeperFactoryDel.java'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package ptt.api.net.slice;

public interface _KeeperFactoryDel extends Ice._ObjectDel
{
    TicketKeeperPrx GetTicket(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;

    UserKeeperPrx GetUser(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;

    ChannelKeeperPrx GetChanel(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;

    SubscribeKeeperPrx GetSubscribe(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;

    ChannelMessageSenderPrx GetChannelSender(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;

    UserMessageSenderPrx GetUserSender(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;
}
