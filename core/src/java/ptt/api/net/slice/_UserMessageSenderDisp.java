// **********************************************************************
//
// Copyright (c) 2003-2011 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.4.2
//
// <auto-generated>
//
// Generated from file `_UserMessageSenderDisp.java'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package ptt.api.net.slice;

public abstract class _UserMessageSenderDisp extends Ice.ObjectImpl implements UserMessageSender
{
    protected void
    ice_copyStateFrom(Ice.Object __obj)
        throws java.lang.CloneNotSupportedException
    {
        throw new java.lang.CloneNotSupportedException();
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::ptt::api::net::slice::UserMessageSender"
    };

    public boolean
    ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean
    ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[]
    ice_ids()
    {
        return __ids;
    }

    public String[]
    ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String
    ice_id()
    {
        return __ids[1];
    }

    public String
    ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String
    ice_staticId()
    {
        return __ids[1];
    }

    public final void
    endTransmission_async(AMD_UserMessageSender_endTransmission __cb, Ticket sid, int userId)
    {
        endTransmission_async(__cb, sid, userId, null);
    }

    public final void
    sendBuffer_async(AMD_UserMessageSender_sendBuffer __cb, Buffer msg, int userId)
    {
        sendBuffer_async(__cb, msg, userId, null);
    }

    public final void
    startTransmission_async(AMD_UserMessageSender_startTransmission __cb, Ticket sid, int userId)
    {
        startTransmission_async(__cb, sid, userId, null);
    }

    public final void
    userUpdated_async(AMD_UserMessageSender_userUpdated __cb, UserInfo info)
    {
        userUpdated_async(__cb, info, null);
    }

    public static Ice.DispatchStatus
    ___startTransmission(UserMessageSender __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        __is.startReadEncaps();
        TicketHolder sid = new TicketHolder();
        __is.readObject(sid);
        int userId;
        userId = __is.readInt();
        __is.readPendingObjects();
        __is.endReadEncaps();
        AMD_UserMessageSender_startTransmission __cb = new _AMD_UserMessageSender_startTransmission(__inS);
        try
        {
            __obj.startTransmission_async(__cb, sid.value, userId, __current);
        }
        catch(java.lang.Exception ex)
        {
            __cb.ice_exception(ex);
        }
        return Ice.DispatchStatus.DispatchAsync;
    }

    public static Ice.DispatchStatus
    ___endTransmission(UserMessageSender __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        __is.startReadEncaps();
        TicketHolder sid = new TicketHolder();
        __is.readObject(sid);
        int userId;
        userId = __is.readInt();
        __is.readPendingObjects();
        __is.endReadEncaps();
        AMD_UserMessageSender_endTransmission __cb = new _AMD_UserMessageSender_endTransmission(__inS);
        try
        {
            __obj.endTransmission_async(__cb, sid.value, userId, __current);
        }
        catch(java.lang.Exception ex)
        {
            __cb.ice_exception(ex);
        }
        return Ice.DispatchStatus.DispatchAsync;
    }

    public static Ice.DispatchStatus
    ___sendBuffer(UserMessageSender __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        __is.startReadEncaps();
        BufferHolder msg = new BufferHolder();
        __is.readObject(msg);
        int userId;
        userId = __is.readInt();
        __is.readPendingObjects();
        __is.endReadEncaps();
        AMD_UserMessageSender_sendBuffer __cb = new _AMD_UserMessageSender_sendBuffer(__inS);
        try
        {
            __obj.sendBuffer_async(__cb, msg.value, userId, __current);
        }
        catch(java.lang.Exception ex)
        {
            __cb.ice_exception(ex);
        }
        return Ice.DispatchStatus.DispatchAsync;
    }

    public static Ice.DispatchStatus
    ___userUpdated(UserMessageSender __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Idempotent, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        __is.startReadEncaps();
        UserInfoHolder info = new UserInfoHolder();
        __is.readObject(info);
        __is.readPendingObjects();
        __is.endReadEncaps();
        AMD_UserMessageSender_userUpdated __cb = new _AMD_UserMessageSender_userUpdated(__inS);
        try
        {
            __obj.userUpdated_async(__cb, info.value, __current);
        }
        catch(java.lang.Exception ex)
        {
            __cb.ice_exception(ex);
        }
        return Ice.DispatchStatus.DispatchAsync;
    }

    private final static String[] __all =
    {
        "endTransmission",
        "ice_id",
        "ice_ids",
        "ice_isA",
        "ice_ping",
        "sendBuffer",
        "startTransmission",
        "userUpdated"
    };

    public Ice.DispatchStatus
    __dispatch(IceInternal.Incoming in, Ice.Current __current)
    {
        int pos = java.util.Arrays.binarySearch(__all, __current.operation);
        if(pos < 0)
        {
            throw new Ice.OperationNotExistException(__current.id, __current.facet, __current.operation);
        }

        switch(pos)
        {
            case 0:
            {
                return ___endTransmission(this, in, __current);
            }
            case 1:
            {
                return ___ice_id(this, in, __current);
            }
            case 2:
            {
                return ___ice_ids(this, in, __current);
            }
            case 3:
            {
                return ___ice_isA(this, in, __current);
            }
            case 4:
            {
                return ___ice_ping(this, in, __current);
            }
            case 5:
            {
                return ___sendBuffer(this, in, __current);
            }
            case 6:
            {
                return ___startTransmission(this, in, __current);
            }
            case 7:
            {
                return ___userUpdated(this, in, __current);
            }
        }

        assert(false);
        throw new Ice.OperationNotExistException(__current.id, __current.facet, __current.operation);
    }

    public void
    __write(IceInternal.BasicStream __os)
    {
        __os.writeTypeId(ice_staticId());
        __os.startWriteSlice();
        __os.endWriteSlice();
        super.__write(__os);
    }

    public void
    __read(IceInternal.BasicStream __is, boolean __rid)
    {
        if(__rid)
        {
            __is.readTypeId();
        }
        __is.startReadSlice();
        __is.endReadSlice();
        super.__read(__is, true);
    }

    public void
    __write(Ice.OutputStream __outS)
    {
        Ice.MarshalException ex = new Ice.MarshalException();
        ex.reason = "type ptt::api::net::slice::UserMessageSender was not generated with stream support";
        throw ex;
    }

    public void
    __read(Ice.InputStream __inS, boolean __rid)
    {
        Ice.MarshalException ex = new Ice.MarshalException();
        ex.reason = "type ptt::api::net::slice::UserMessageSender was not generated with stream support";
        throw ex;
    }
}
