package ptt.api.net.task;

import ptt.api.net.slice.AMD_UserKeeper_AddToList;
import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.*;
import ptt.api.net.exceptions.*;
import ptt.api.net.*;

public class AddContact extends UserOperations<AMD_UserKeeper_AddToList> {

	private Ticket m_sid;
	private int m_id;
  private MessageManager m_manager;
	
	public AddContact(AMD_UserKeeper_AddToList cb, MessageManager manager, Ticket sid, int id) {
		super(cb);
		m_sid = sid;
		m_id = id;
    m_manager = manager;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		getKeeper().AddToContacts(m_sid, m_id);
		UserInfo info = getKeeper().Get(m_sid);
    try
    {
      Subscriber subs = m_manager.getSubscribers().getSubscriber(m_id);
      m_manager.addTask(UserOperations.notifyUserAdded(info, subs));
    }
    catch(SubscriberNotFoundException ex)
    {
      /// Looks like user is offline
    }
	}
}
