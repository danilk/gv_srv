package ptt.api.net.task;

import ptt.api.net.MessageManager;
import ptt.api.net.slice.AMD_ChannelKeeper_Authorize;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.exceptions.GenericException;

public class ChannelAuthorizer extends ChannelOperations<AMD_ChannelKeeper_Authorize> {
	private int m_channelId;
  private boolean m_result = false;
  private String m_password;
	
	public ChannelAuthorizer(AMD_ChannelKeeper_Authorize cb, MessageManager manager, int channelId, String password) {
		super(cb, manager);
		m_channelId = channelId;
    m_password = password;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_result);
	}

	@Override
	public void Notify() throws GenericException {
	  m_result = getKeeper().Authorize(m_channelId, m_password);
	}
}
