package ptt.api.net.task;

import org.apache.log4j.Logger;

import ptt.api.manager.IUserKeeper;
import ptt.api.spring.DefaultBeanFactory;

import ptt.api.net.Subscriber;
import ptt.api.net.Subscribers;

import ptt.api.net.slice.AMD_ChannelMessageSender_sendBuffer;
import ptt.api.net.slice.AMD_ChannelMessageSender_startTransmission;
import ptt.api.net.slice.AMD_ChannelMessageSender_endTransmission;
import ptt.api.net.slice.AMD_ChannelMessageSender_channelUpdated;
import ptt.api.net.slice.Buffer;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.exceptions.GenericException;

/**
 * @author lavei
 *
 */
public abstract class ChannelNotification extends INotification {

  protected int m_channel = -1;
  private Subscribers m_subs;

  protected ChannelNotification(int channel, Subscribers subs)
  {
    m_channel = channel;
    m_subs = subs;
  }
	
  @Override
    public void Notify() {
      log.debug("Started sending " + this + " on " + m_channel);
      m_subs.sendNotification(this, m_channel);			
      log.debug("Finish sending " + this + " on " + m_channel);
    }
  @Override
    public int getThreadId()
    {
      return m_channel == -1 ? 0 : m_channel;
    }

 // public abstract Ticket getTicket();
  public abstract boolean isValid(); 

	static public ChannelNotification createSendTask(Subscriber author, Buffer buffer, int channel, AMD_ChannelMessageSender_sendBuffer cb, Subscribers subs)
	{
		return new SendData(author, buffer, channel, cb, subs);
	}
	
	static public ChannelNotification createSTTask(Subscriber author, int channel, AMD_ChannelMessageSender_startTransmission cb, Subscribers subs)
	{
		return new StartTransmission(author, channel, cb, subs);
	}
	
	static public ChannelNotification createETTask(Subscriber author, int channel, int userId, String timestamp, AMD_ChannelMessageSender_endTransmission cb, Subscribers subs)
	{
		return new EndTransmission(author, channel, userId, timestamp, cb, subs);
	}
	
	static public ChannelNotification createChannelNotification(ChannelData channel, AMD_ChannelMessageSender_channelUpdated cb, Subscribers subs)
	{
		return new ChannelUpdateNotification(channel, cb, subs);
	}
}

class SendData extends ChannelNotification
{
  private Buffer m_buffer;
  private Subscriber m_author;
  private AMD_ChannelMessageSender_sendBuffer m_cb;

  private IUserKeeper getKeeper() {
    return (IUserKeeper) DefaultBeanFactory.getInstance().getBean(
        IUserKeeper.class);
  }

  public SendData(Subscriber author, Buffer buffer, int channel, AMD_ChannelMessageSender_sendBuffer cb, Subscribers subs)
  {
    super(channel, subs);
    m_buffer = buffer;
    m_author = author;
    m_cb = cb;
  }

  @Override
    public void Execute(Subscriber itm) 
    {
      log.info("Process" + m_buffer.sid.sid);

      if (itm.equals(m_author) || !itm.canRead(m_channel, m_author))
      {
        log.info("Skip this guy:" + itm.getTicket().sid);
        return;
      }
      log.info("Send to this guy:" + itm.getTicket().sid);
      itm.getChannelSender().sendBuffer(m_buffer, m_channel);
    }

  @Override
    public void Complete() {
      m_cb.ice_response();
    }

  @Override
    public void Exception(GenericException ex) {
      m_cb.ice_exception(ex);
    }
/*  @Override
    public Ticket getTicket()
    {
      return m_author.getTicket();
    }*/
  @Override
    public boolean isValid()
    {
      return m_channel != -1 && m_author != null && m_author.canWrite(m_channel);
    }
}

class StartTransmission extends ChannelNotification
{
  private Subscriber m_author;
  private AMD_ChannelMessageSender_startTransmission m_cb;
  public StartTransmission(Subscriber author, int channel, AMD_ChannelMessageSender_startTransmission cb, Subscribers subs)
  {
    super(channel, subs);
    m_author = author;
    m_author.setTimestamp();
    m_cb = cb;
  }

  @Override
    public void Execute(Subscriber itm) 
    {
      if (itm.equals(m_author)
          || !itm.canRead(m_channel, m_author))
      {
        return;
      }

      itm.getChannelSender().begin_startTransmission(m_author.getTicket(), m_channel);
    }

  @Override
    public void Complete() {
      m_cb.ice_response();
    }

  @Override
    public void Exception(GenericException ex) {
      m_cb.ice_exception(ex);
    }

/*  @Override
    public Ticket getTicket()
    {
      return m_author.getTicket();
    }*/

  @Override
    public boolean isValid()
    {
      return m_channel != -1 && m_author != null && m_author.canWrite(m_channel);
    }
}

class EndTransmission extends ChannelNotification
{
  private Subscriber m_author;
  private AMD_ChannelMessageSender_endTransmission m_cb;
  public EndTransmission(Subscriber author, int channel, int userId, String timestamp, AMD_ChannelMessageSender_endTransmission cb, Subscribers subs)
  {
    super(channel, subs);
    m_author = author;
    m_cb = cb;
  }

  @Override
    public void Execute(Subscriber itm) 
    {
      if (itm.equals(m_author)|| !itm.canRead(m_channel, m_author))
      {
        return;
      }

      itm.getChannelSender().begin_endTransmission(m_author.getTicket(), m_channel, m_author.getUserId(), m_author.getTimestamp());
    }

  @Override
    public void Complete() {
      m_cb.ice_response();
    }

  @Override
    public void Exception(GenericException ex) {
      m_cb.ice_exception(ex);
    }

/*  @Override
    public Ticket getTicket()
    {
      return m_author.getTicket();
    }*/
  
  @Override
    public boolean isValid()
    {
      return m_channel != -1 && m_author != null && m_author.canWrite(m_channel);
    }
}

class ChannelUpdateNotification extends ChannelNotification
{
  private AMD_ChannelMessageSender_channelUpdated m_cb;
  private ChannelData m_channelData;
  public ChannelUpdateNotification(ChannelData channel, AMD_ChannelMessageSender_channelUpdated cb, Subscribers subs)
  {
    super(channel.channelId, subs);
    m_cb =cb;
    m_channelData = channel;
  }

  @Override
    public void Execute(Subscriber itm) 
    {
      //m_channelData.channelState = itm.getChannelState(m_channel);
      ///TODO work with copy
      itm.actualize(m_channelData);
      itm.getChannelSender().begin_channelUpdated(m_channelData);
    }

  @Override
    public void Complete() {
      if (m_cb != null)
      {
        m_cb.ice_response();
      }
    }

  @Override
    public void Exception(GenericException ex) {

      if (m_cb != null)
      {
        m_cb.ice_exception(ex);
      }
    }
/*  @Override
    public Ticket getTicket()
    {
      return null;
    }*/
  @Override
    public boolean isValid()
    {
      return true;
    }
}

