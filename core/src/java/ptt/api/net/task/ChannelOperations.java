package ptt.api.net.task;

import org.apache.log4j.Logger;

import Ice.Current;

import ptt.api.manager.IChannelKeeper;
import ptt.api.manager.IUserKeeper;
import ptt.api.net.ITask;
import ptt.api.net.MessageManager;
import ptt.api.net.slice.AMD_ChannelKeeper_Authorize;
import ptt.api.net.slice.AMD_ChannelKeeper_Create;
import ptt.api.net.slice.AMD_ChannelKeeper_Drop;
import ptt.api.net.slice.AMD_ChannelKeeper_Get;
import ptt.api.net.slice.AMD_ChannelKeeper_GetAll;
import ptt.api.net.slice.AMD_ChannelKeeper_GetByWord;
import ptt.api.net.slice.AMD_ChannelKeeper_GetByWordAndUser;
import ptt.api.net.slice.AMD_ChannelKeeper_GetUsers;
import ptt.api.net.slice.AMD_ChannelKeeper_IsNameUniq;
import ptt.api.net.slice.AMD_ChannelKeeper_Join;
import ptt.api.net.slice.AMD_ChannelKeeper_Leave;
import ptt.api.net.slice.AMD_ChannelKeeper_Mute;
import ptt.api.net.slice.AMD_ChannelKeeper_UnMute;
import ptt.api.net.slice.AMD_ChannelKeeper_Update;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;
import ptt.api.spring.DefaultBeanFactory;

public abstract class ChannelOperations<C extends Ice.AMDCallback> extends ITask {

	protected IChannelKeeper getKeeper() {
		return (IChannelKeeper) DefaultBeanFactory.getInstance().getBean(
				IChannelKeeper.class);
	}
	
	protected C m_cb;
	protected volatile MessageManager m_manager;
	protected final Logger log = Logger.getLogger(this.getClass());
	
	public ChannelOperations(C cb, MessageManager manager)
	{
		m_cb = cb;
		m_manager = manager;
	}

	@Override
	public void Exception(GenericException ex) {
		m_cb.ice_exception(ex);
	}

	@Override
	public int getThreadId() {
		return 0;
	}

	public static ITask getChannels(AMD_ChannelKeeper_GetAll cb, Ticket sid,
			MessageManager manager, Current current) {
		return new UserChannels(cb, manager, sid, current);
	}

	public static ITask searchUserChannels(
			AMD_ChannelKeeper_GetByWordAndUser cb, MessageManager manager,
			Ticket sid, String info, Current current) {
		return new UserChannelsSearch(cb, manager, sid, info, current);
	}

	public static ITask searchChannels(AMD_ChannelKeeper_GetByWord cb,
			MessageManager manager, String info, Ticket sid) 
	{
		return new ChannelsSearch(cb, manager, info, sid);
	}

	public static ITask getChannel(AMD_ChannelKeeper_Get cb,
			MessageManager manager, int channelId, Ticket sid) {
		return new GetChannelInfo(cb, manager, channelId, sid);
	}

	public static ITask createChannel(AMD_ChannelKeeper_Create cb,
			MessageManager manager, ChannelData info, Ticket sid,
			Current current) {
		return new CreateChannel(cb, manager, info, sid, current);
	}

	public static ITask deleteChannel(AMD_ChannelKeeper_Drop cb,
			MessageManager manager, int id, Ticket sid, Current current) {
		return new DeleteChannel(cb, manager, id, sid, current);
	}

	public static ITask updateChannel(AMD_ChannelKeeper_Update cb,
			MessageManager manager, ChannelData info, Ticket sid, Current current) {
		return new UpdateChannel(cb, manager, info, sid, current);
	}
	
  public static ITask joinChannel(AMD_ChannelKeeper_Join cb,
			MessageManager manager, int id, Ticket sid, Current current) {
		return new JoinChannel(cb, manager, id, sid, current);
	}
  
  public static ITask leaveChannel(AMD_ChannelKeeper_Leave cb,
			MessageManager manager, int id, Ticket sid, Current current) {
		return new LeaveChannel(cb, manager, id, sid, current);
	}
  
  public static ITask muteChannel(AMD_ChannelKeeper_Mute cb,
			MessageManager manager, int id, Ticket sid, Current current) {
		return new MuteChannel(cb, manager, id, sid, current);
	}
	
  public static ITask unMuteChannel(AMD_ChannelKeeper_UnMute cb,
			MessageManager manager, int id, Ticket sid, Current current) {
		return new UnMuteChannel(cb, manager, id, sid, current);
	}
  public static ITask authorizeChannel(AMD_ChannelKeeper_Authorize cb,
			MessageManager manager, int channelId, String password) {
		return new ChannelAuthorizer(cb, manager, channelId, password);
	}
  
  public static ITask uniqChannelName(AMD_ChannelKeeper_IsNameUniq cb,
			MessageManager manager, String name) {
		return new UniqChannelName(cb, manager, name);
	}

public static ITask getUsers(AMD_ChannelKeeper_GetUsers cb,
		MessageManager manager, int channelId, Current current) {
	return new UsersChannel(cb, manager, channelId, current);
}
}
