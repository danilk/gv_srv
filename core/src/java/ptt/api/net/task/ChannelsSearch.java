package ptt.api.net.task;

import java.util.List;

import ptt.api.net.MessageManager;
import ptt.api.net.slice.AMD_ChannelKeeper_GetByWord;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class ChannelsSearch extends ChannelOperations<AMD_ChannelKeeper_GetByWord> {
	private String m_info;
	private Ticket m_sid;
	private List<ChannelData> m_list;

	public ChannelsSearch(AMD_ChannelKeeper_GetByWord cb, MessageManager manager, String info, Ticket sid) {
		super(cb, manager);
		m_info = info;
		m_sid = sid;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_list.toArray(new ChannelData[m_list.size()]));
	}

	@Override
	public void Notify() throws GenericException {
		m_list = getKeeper().GetAll(m_info);
		m_manager.actualizeOnlineSubscribers(m_list);
		for(ChannelData itm : m_list)
		{
			m_manager.getSubscribers().getSubscriber(m_sid).actualize(itm);
		}
	}
}
