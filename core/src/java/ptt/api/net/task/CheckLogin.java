package ptt.api.net.task;

import ptt.api.net.slice.AMD_UserKeeper_IsLoginUniq;
import ptt.api.net.slice.exceptions.GenericException;

public class CheckLogin extends UserOperations<AMD_UserKeeper_IsLoginUniq> {

	private String  m_login;
	private boolean m_result = false;
	
	public CheckLogin(AMD_UserKeeper_IsLoginUniq cb, String login) {
		super(cb);
		m_login = login;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_result);
	}

	@Override
	public void Notify() throws GenericException {
		log.debug("Is login " + m_login + " uniq?");
		m_result = getKeeper().IsLoginUniq(m_login);
	}

}
