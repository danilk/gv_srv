package ptt.api.net.task;

import java.util.List;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.AMD_ChannelKeeper_Create;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class CreateChannel extends ChannelOperations<AMD_ChannelKeeper_Create> {

	private ChannelData m_info;
	private Ticket m_sid;
	private Ice.Current m_current;
	private int m_id;
	
	public CreateChannel(AMD_ChannelKeeper_Create cb, MessageManager manager, ChannelData info, Ticket sid, Current current) {
		super(cb, manager);
		m_info = info;
		m_sid = sid;
		m_current = current;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_id);
	}

	@Override
	public void Notify() throws GenericException {
		m_id = getKeeper().Create(m_info, m_sid);
		if (m_id != -1) {
			try {
				m_manager.subscribe(m_id, m_sid);
			} catch (SubscriberNotFoundException ex) {
				if (m_manager.restoreSubscriber(m_current, m_sid)) {
					m_manager.subscribe(m_id, m_sid);
				}
			}
		}
	}
}
