package ptt.api.net.task;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.slice.AMD_ChannelKeeper_Drop;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class DeleteChannel extends ChannelOperations<AMD_ChannelKeeper_Drop> {
	private Ticket m_sid;
	private Ice.Current m_current;
	private int m_id;

	public DeleteChannel(AMD_ChannelKeeper_Drop cb, MessageManager manager, int id, Ticket sid, Current current) {
		super(cb, manager);
		m_sid = sid;
		m_current = current;
		m_id = id;
	}

	@Override
	public void Complete() {
	    m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		getKeeper().Remove(m_id, m_sid);
		m_manager.unsubscribe(m_id, m_sid);
		// TODO change to remove notification
		// m_manager.getWorkQueue().add(ITask.CreateChannelNotification(id,
		// null));
	}
}
