package ptt.api.net.task;

import ptt.api.net.MessageManager;
import ptt.api.net.slice.AMD_ChannelKeeper_Get;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class GetChannelInfo extends ChannelOperations<AMD_ChannelKeeper_Get> {
	private int m_channelId;
	private Ticket m_sid;
	private ChannelData m_data;
	
	public GetChannelInfo(AMD_ChannelKeeper_Get cb, MessageManager manager, int channelId, Ticket sid) {
		super(cb, manager);
		m_channelId = channelId;
		m_sid = sid;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_data);
	}

	@Override
	public void Notify() throws GenericException {
		m_data = getKeeper().Get(m_channelId);
		m_manager.actualizeOnlineSubscribers(m_data);
		m_manager.getSubscribers().getSubscriber(m_sid).actualize(m_data);
	}
}
