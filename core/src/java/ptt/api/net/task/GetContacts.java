package ptt.api.net.task;

import java.util.List;
import ptt.api.net.*;
import ptt.api.net.exceptions.*;
import ptt.api.net.slice.AMD_UserKeeper_GetContactList;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.exceptions.GenericException;

public class GetContacts extends UserOperations<AMD_UserKeeper_GetContactList> {
	private List<UserInfo> m_list;
	private Ticket m_sid;
  private MessageManager m_manager;
	
	public GetContacts(AMD_UserKeeper_GetContactList cb, MessageManager manager,Ticket sid) {
		super(cb);
		m_sid = sid;
    m_manager = manager;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_list.toArray(new UserInfo[m_list.size()]));
	}

	@Override
	public void Notify() throws GenericException {
		m_list = getKeeper().GetContacts(m_sid);
    for(UserInfo itm : m_list)
    {
      try
      {
        m_manager.getSubscribers().getSubscriber(itm.id);
        itm.isOnline = true;
      }
      catch(SubscriberNotFoundException ex)
      {
      }
    }
	}
}
