package ptt.api.net.task;

import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.AMD_UserKeeper_GetUser;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.exceptions.GenericException;

public class GetUserInfo extends UserOperations<AMD_UserKeeper_GetUser> {

	private UserInfo m_info;
	private int m_id;
	private MessageManager m_manager;
	
	public GetUserInfo(AMD_UserKeeper_GetUser cb, MessageManager manager, int id) {
		super(cb);
		m_id = id;
		m_manager = manager;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_info);
	}

	@Override
	public void Notify() throws GenericException {
		m_info = getKeeper().GetUser(m_id);
		try
		{
			m_manager.getSubscribers().getSubscriber(m_id);
			m_info.isOnline = true;
		}
		catch(SubscriberNotFoundException ex)
		{
			m_info.isOnline = false;
		}
	}

}
