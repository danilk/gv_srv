package ptt.api.net.task;

import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.*;
import ptt.api.net.exceptions.*;
import ptt.api.net.Subscriber;
import ptt.api.net.Subscribers;
import ptt.api.net.ITask;

/**
 * @author lavei
 *
 */
public abstract class GlobalNotification extends INotification {

  private Subscribers m_subs;
	static public GlobalNotification createPingNotification(Subscribers subs)
	{
		return new PingNotification(subs);
	}
  public GlobalNotification(Subscribers subs)
  {
    m_subs = subs;
  }

  @Override
    public void Notify() {
      log.debug("Started sending " + this);
      m_subs.sendNotification(this);			
      log.debug("Finish sending " + this);
    }
  @Override
    public int getThreadId()
    {
      return 0;
    }
}

class PingNotification extends GlobalNotification
{
  public PingNotification(Subscribers subs)
  {
    super(subs);
  }
  @Override
    public void Execute(Subscriber itm) 
    {
	  log.debug("Ping  " + itm.getTicket().sid);
      itm.getChannelSender().ice_ping();
///      itm.getUserSender().ice_ping();
    }

  @Override
    public void Complete() {
      //TODO nothing to do
    }

  @Override
    public void Exception(GenericException ex) {
      //TODO nothing to do
    }
}
class UserUpdateNotification extends GlobalNotification
{
	UserInfo m_info;
	AMD_UserMessageSender_userUpdated m_cb;	
  public UserUpdateNotification(Subscribers subs, UserInfo info, AMD_UserMessageSender_userUpdated cb)
  {
    super(subs);
    m_info = info;
    m_cb = cb;
    try
    {
       subs.getSubscriber(info.id);
       m_info.isOnline = true;
    }
    catch(SubscriberNotFoundException ex)
    {
       m_info.isOnline = false;
    }
  }
  @Override
    public void Execute(Subscriber itm) 
    {
      itm.getUserSender().begin_userUpdated(m_info);
    }

  @Override
    public void Complete() {
	   if (m_cb != null)
     {
       m_cb.ice_response();
     }
    }

  @Override
    public void Exception(GenericException ex) {
	   if (m_cb != null)
     {
       m_cb.ice_exception(ex);
     }
    }
}


