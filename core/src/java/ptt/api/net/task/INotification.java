package ptt.api.net.task;

import ptt.api.net.ITask;
import ptt.api.net.Subscriber;


/**
 * @author lavei
 *
 */
public abstract class INotification extends ITask 
{
  public abstract void Execute(Subscriber itm);
}
