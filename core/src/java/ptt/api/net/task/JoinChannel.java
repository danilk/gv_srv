package ptt.api.net.task;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.AMD_ChannelKeeper_Drop;
import ptt.api.net.slice.AMD_ChannelKeeper_Join;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.*;

public class JoinChannel extends ChannelOperations<AMD_ChannelKeeper_Join> {
	private Ticket m_sid;
	private Ice.Current m_current;
	private int m_id;

	public JoinChannel(AMD_ChannelKeeper_Join cb, MessageManager manager,
			int id, Ticket sid, Current current) {
		super(cb, manager);
		m_sid = sid;
		m_current = current;
		m_id = id;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		try {
			getKeeper().Subscribe(m_id, m_sid);
			m_manager.subscribe(m_id, m_sid);
		} catch (SubscriberNotFoundException ex) {
			if (m_manager.restoreSubscriber(m_current, m_sid)) {
				m_manager.subscribe(m_id, m_sid);
			}
		}
	}
}
