package ptt.api.net.task;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.slice.AMD_ChannelKeeper_Drop;
import ptt.api.net.slice.AMD_ChannelKeeper_Leave;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class LeaveChannel extends ChannelOperations<AMD_ChannelKeeper_Leave> {
	private Ticket m_sid;
	private int m_id;

	public LeaveChannel(AMD_ChannelKeeper_Leave cb, MessageManager manager,
			int id, Ticket sid, Current current) {
		super(cb, manager);
		m_sid = sid;
		m_id = id;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		getKeeper().Unsubscribe(m_id, m_sid);
		m_manager.unsubscribe(m_id, m_sid);
	}
}
