package ptt.api.net.task;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.*;

public class MuteUser extends UserOperations<AMD_UserKeeper_Mute> {
	private Ticket m_sid;
	private Ice.Current m_current;
	private int m_id;
  private int m_state;
  private MessageManager m_manager;

	public MuteUser(AMD_UserKeeper_Mute cb, MessageManager manager,
			int id, Ticket sid, Current current) {
		super(cb);
    m_manager = manager;
		m_sid = sid;
		m_current = current;
		m_id = id;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		getKeeper().Mute(m_id, m_sid, m_state);
		// TODO should be replaced by enum
/*		try {
			m_manager.getSubscribers().muteSubscriber(m_id, m_sid, 4);
		} catch (SubscriberNotFoundException ex) {
			if (m_manager.restoreSubscriber(m_current, m_sid)) {
				m_manager.getSubscribers().muteSubscriber(m_id, m_sid, 4);
			}
		}*/
//		m_manager.notifyUpdateUser(m_id);
	}
}
