package ptt.api.net.task;

import ptt.api.net.task.UserOperations;
import ptt.api.net.slice.AMD_UserKeeper_RemoveFromList;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class RemoveContact extends UserOperations<AMD_UserKeeper_RemoveFromList> {

	private Ticket m_sid;
	private int m_id;
	
	public RemoveContact(AMD_UserKeeper_RemoveFromList cb, Ticket sid, int id) {
		super(cb);
		m_sid = sid;
		m_id = id;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		getKeeper().DeleteFromContacts(m_sid, m_id);
	}
}
