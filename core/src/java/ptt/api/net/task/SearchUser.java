package ptt.api.net.task;

import java.util.List;

import ptt.api.net.slice.AMD_UserKeeper_GetByWord;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.exceptions.GenericException;

public class SearchUser extends UserOperations<AMD_UserKeeper_GetByWord> {

	private List<UserInfo> m_list;
	private String m_info;
	
	public SearchUser(AMD_UserKeeper_GetByWord cb, String info) {
		super(cb);
		m_info = info;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_list.toArray(new UserInfo[m_list.size()]));
	}

	@Override
	public void Notify() throws GenericException {
		m_list = getKeeper().SearchUser(m_info);
	}

}
