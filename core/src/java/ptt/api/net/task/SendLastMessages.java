package ptt.api.net.task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.*;
import ptt.api.net.exceptions.*;
import ptt.api.net.*;

public class SendLastMessages extends ITask {

	private Ticket m_sid;
    private MessageManager m_manager;
    private java.text.SimpleDateFormat m_date = new java.text.SimpleDateFormat("yyyyMMdd_HHmmss");
    
	public SendLastMessages(MessageManager manager, Ticket sid) {
		m_sid = sid;
		m_manager = manager;
	}

	@Override
	public void Complete() {
	}

	@Override
	public void Notify() throws GenericException {
		UserInfo info = m_manager.getUserKeeper().Get(m_sid);
		String userPath = m_manager.getUserKeeper().getFilepath();
				
		Subscriber subs = null;
		try {
			subs = m_manager.getSubscribers().getSubscriber(m_sid);
		} catch (SubscriberNotFoundException ex) {
			log.error("Can't find subscriber for " + m_sid);
			return;
			// / Looks like user is offline
		}
		
		log.info("Process user messages");
		
		processDirectory(FileSystems.getDefault().getPath(userPath).resolve(String.valueOf(info.id)), subs, 0, true);
		
		List<ChannelData> channels = m_manager.getChannelKeeper().Get(m_sid);
		String channelPath = m_manager.getChannelKeeper().getFilepath();
		for(ChannelData channel : channels)
		{
			log.info("Process channel messages for " + channel.channelId);
			processDirectory(FileSystems.getDefault().getPath(channelPath).resolve(String.valueOf(channel.channelId)), subs, channel.channelId, false);
		}

	}

	@Override
	public void Exception(GenericException ex) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getThreadId() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private void processDirectory(Path dir, Subscriber subs, int chId, boolean isUser)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MINUTE, -2);
		Date dt = c.getTime();
		log.debug("Ethalon date " + dt.toString());

		List<Path> files = new ArrayList<>();
		DirectoryStream<Path> stream = null;
		try {
			stream = Files.newDirectoryStream(dir);
		} catch (IOException e) {
			log.info(dir.toString() + "Doesn't exists");
			return;
		}
		for (Path entry: stream) {
			String date = entry.getFileName().toFile().getName().substring(0, 15);
			log.debug("Process file " + entry.getFileName().toFile().getName() + " with date " + date);

			try {
				Date dd = m_date.parse(date);
				if(dt.before(dd))
				{
					log.debug("Add file " + entry.getFileName().toFile().getName());
					files.add(entry);				
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		log.debug("Total files to send: " + files.size());
		
		for (Path file : files)
		{
			int dotIdx = file.getFileName().toFile().getName().indexOf(".", 17);
			int id = Integer.parseInt(file.getFileName().toFile().getName().substring(17, dotIdx));
			if (id == subs.getUserId())
			{
				log.debug("Skipped message.");
				continue;
			}
/*			log.debug("Start transmission.");

			if (isUser)
			{
				subs.getUserSender().begin_startTransmission(subs.getTicket(), id);
			}
			else
			{
				subs.getChannelSender().begin_startTransmission(subs.getTicket(), id);
			}*/
			log.debug("Send data from file." + file.toString());

			try {
				InputStream reader = 
				        Files.newInputStream(file);
				
				byte[] b = new byte[(int) file.toFile().length()];
				int off = 0;
				reader.read(b);
//				while (reader.read(b) != 0 )
				{
					off += 1024*1024;
					Buffer msg = new Buffer();
					msg.isHistorical = true;
					msg.data = b;
					msg.timestamp = file.getFileName().toFile().getName().substring(0, 15); 
//					msg.sid = subs.getTicket();
					msg.userId = id;
					log.debug("Read data from file " + file.toString() + "size " + msg.data.length);					
					if (isUser)
					{
						subs.getUserSender().begin_sendBuffer(msg, id);
					}
					else
					{
						subs.getChannelSender().begin_sendBuffer(msg, chId);
					}		
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.debug("Send end transmission.");
		
			if (isUser)
			{
				subs.getUserSender().begin_endTransmission(subs.getTicket(), id, file.getFileName().toFile().getName().substring(0, 15));
			}
			else
			{
				subs.getChannelSender().begin_endTransmission(subs.getTicket(), chId, id, file.getFileName().toFile().getName().substring(0, 15));
			}
			
			log.debug("Finish processing.");			
		}
	}
}
