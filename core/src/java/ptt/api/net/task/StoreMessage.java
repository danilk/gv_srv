package ptt.api.net.task;

import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.*;
import ptt.api.net.exceptions.*;
import ptt.api.net.*;
import java.io.*;
import java.nio.file.*;

public class StoreMessage extends ITask {

	private String m_path;
	private String m_name;
    private Buffer m_msg;
	
	public StoreMessage(String i_path, String i_name, Buffer i_msg) {
		m_path = i_path;
		m_name = i_name;
		m_msg = i_msg;
	}

	@Override
	public void Complete() {
	}

  @Override
	public void Exception(GenericException ex)
  {
    log.error("Store message error:" + ex);
  }

	@Override
	public void Notify() throws GenericException {
    try
    {
      Path path = FileSystems.getDefault().getPath(m_path);
      log.info("write to " + path);
      path = Files.createDirectories(path);
      path = path.resolve(m_name + ".mp3");
      log.info("write to " + path);
      OutputStream out = Files.newOutputStream( path, StandardOpenOption.CREATE, StandardOpenOption.APPEND );
      out.write(m_msg.data);
      out.close();
    }
    catch(Exception ex)
    {
    	log.error("Store message error:" + ex.getMessage());
      /// Looks like user is offline
    }
	}
	
	public int getThreadId()
  {
    return 0;
  }

}
