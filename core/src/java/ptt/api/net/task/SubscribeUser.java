package ptt.api.net.task;

import Ice.Current;
import Ice.Identity;
import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.*;

public class SubscribeUser extends UserOperations<AMD_SubscribeKeeper_Subscribe> {
	private Ticket m_sid;
	private Ice.Current m_current;
	private Ice.Identity m_channel;
	private Ice.Identity m_user;
  private MessageManager m_manager;

	public SubscribeUser(AMD_SubscribeKeeper_Subscribe cb, MessageManager manager,
			Current current, Ticket sid, Ice.Identity channel, Identity user) {
		super(cb);
    m_manager = manager;
		m_sid = sid;
		m_current = current;
		m_channel = channel;
    m_user = user;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
    m_manager.createSubscriber(m_current, m_sid, m_channel, m_user);
	}
}
