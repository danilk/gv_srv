package ptt.api.net.task;

import java.util.LinkedList;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.Subscriber;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.AMD_ChannelKeeper_Drop;
import ptt.api.net.slice.AMD_ChannelKeeper_UnMute;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class UnMuteChannel extends ChannelOperations<AMD_ChannelKeeper_UnMute> {
	private Ticket m_sid;
	private Ice.Current m_current;
	private int m_id;

	public UnMuteChannel(AMD_ChannelKeeper_UnMute cb, MessageManager manager,
			int id, Ticket sid, Current current) {
		super(cb, manager);
		m_sid = sid;
		m_current = current;
		m_id = id;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		getKeeper().Mute(m_id, m_sid, 0);
		// TODO should be replaced by enum
		LinkedList<Subscriber> list = m_manager.getSubscribers().getByChannel(m_id);
		for (Subscriber itm : list) {
			itm.editChannel(m_id, 0);
		}
		m_manager.notifyUpdateChannel(m_id);
	}
}
