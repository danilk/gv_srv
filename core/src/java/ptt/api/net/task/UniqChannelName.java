package ptt.api.net.task;

import ptt.api.net.MessageManager;
import ptt.api.net.slice.AMD_ChannelKeeper_IsNameUniq;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.exceptions.GenericException;

public class  UniqChannelName extends ChannelOperations<AMD_ChannelKeeper_IsNameUniq> {
  private boolean m_result = false;
  private String m_name;
	
	public UniqChannelName(AMD_ChannelKeeper_IsNameUniq cb, MessageManager manager, String name) {
		super(cb, manager);
    m_name = name;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_result);
	}

	@Override
	public void Notify() throws GenericException {
    m_result = getKeeper().IsUniq(m_name);
	}
}
