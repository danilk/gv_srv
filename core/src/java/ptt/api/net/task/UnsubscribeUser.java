package ptt.api.net.task;

import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.*;

public class UnsubscribeUser extends UserOperations<AMD_SubscribeKeeper_Unsubscribe> {
	private Ticket m_sid;
  private MessageManager m_manager;

	public UnsubscribeUser(AMD_SubscribeKeeper_Unsubscribe cb, MessageManager manager,
			Ticket sid) {
		super(cb);
    m_manager = manager;
		m_sid = sid;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		m_manager.unsubscribe(m_sid);
	}
}
