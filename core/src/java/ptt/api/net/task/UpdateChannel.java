package ptt.api.net.task;

import java.util.List;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.AMD_ChannelKeeper_Update;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class UpdateChannel extends ChannelOperations<AMD_ChannelKeeper_Update> {

	private ChannelData m_info;
	private Ticket m_sid;
	private Ice.Current m_current;
	
	public UpdateChannel(AMD_ChannelKeeper_Update cb, MessageManager manager, ChannelData info, Ticket sid, Current current) {
		super(cb, manager);
		m_info = info;
		m_sid = sid;
		m_current = current;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
	  getKeeper().Change(m_info, m_sid);
    m_manager.updateChannelData(m_info);
    m_manager.notifyUpdateChannel(m_info.channelId);  
	}
}
