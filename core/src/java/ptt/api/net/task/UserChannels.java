package ptt.api.net.task;

import java.util.List;

import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.AMD_ChannelKeeper_GetAll;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class UserChannels extends ChannelOperations<AMD_ChannelKeeper_GetAll> {

	private Ticket m_sid;
	private List<ChannelData> m_list;
	private Ice.Current m_current;
	public UserChannels(AMD_ChannelKeeper_GetAll cb, MessageManager manager, Ticket sid, Ice.Current current) {
		super(cb, manager);
		m_sid = sid;
		m_current = current;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_list.toArray(new ChannelData[m_list.size()]));
	}

	@Override
	public void Notify() throws GenericException {
		m_list = getKeeper().Get(m_sid);
		try {
			m_manager.setSubscriberData(m_list, m_sid);
		} catch (SubscriberNotFoundException ex) {
			if (m_manager.restoreSubscriber(m_current, m_sid)) {
				m_manager.setSubscriberData(m_list, m_sid);
			}
		}
	}

}
