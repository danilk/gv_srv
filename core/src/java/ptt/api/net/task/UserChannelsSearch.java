package ptt.api.net.task;

import java.util.List;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.exceptions.SubscriberNotFoundException;
import ptt.api.net.slice.AMD_ChannelKeeper_GetByWordAndUser;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.exceptions.GenericException;

public class UserChannelsSearch extends ChannelOperations<AMD_ChannelKeeper_GetByWordAndUser> {
	
	private String m_info;
	private Ticket m_sid;
	private List<ChannelData> m_list;
	private Ice.Current m_current;
	
	public UserChannelsSearch(AMD_ChannelKeeper_GetByWordAndUser cb,
			MessageManager manager, Ticket sid, String info, Current current) {
		super(cb, manager);
		m_info = info;
		m_sid = sid;
		m_current = current;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_list.toArray(new ChannelData[m_list.size()]));
	}

	@Override
	public void Notify() throws GenericException {
		List<ChannelData> m_list = getKeeper().GetAll(m_info, m_sid);
		try {
			m_manager.setSubscriberData(m_list, m_sid);
		} catch (SubscriberNotFoundException ex) {
			if (m_manager.restoreSubscriber(m_current, m_sid)) {
				m_manager.setSubscriberData(m_list, m_sid);
			}
		}
	}
}
