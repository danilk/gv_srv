package ptt.api.net.task;

import ptt.api.net.Subscriber;
import ptt.api.net.Subscribers;
import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.GenericException;

public abstract class UserNotification extends INotification {
	protected int m_userId = -1;
	protected Subscriber m_receiver;
	protected Subscriber m_author;


	protected UserNotification(Subscriber author, Subscriber receiver) {
		m_receiver = receiver;
		m_author = author;
	}
	@Override
	public void Notify() throws GenericException {
	      log.debug("Started sending to user");
	      this.Execute(m_receiver);		
	      log.debug("Finish sending to user");
	}

	@Override
	public int getThreadId() {
		// TODO Auto-generated method stub
		return 0;
	}
}

class UserAddedNotification extends UserNotification
{
  private UserInfo m_info;
	public UserAddedNotification(Subscriber author, Subscriber receiver, UserInfo info) {
		super(author, receiver);
    m_info = info;
    m_info.isOnline = true;
    m_info.userAdded = true;
	}

  @Override
    public void Execute(Subscriber itm) 
    {
      itm.getUserSender().begin_userUpdated(m_info);
    }

  @Override
    public void Complete() {
    }

  @Override
    public void Exception(GenericException ex) {
    }
}
class StartUserTransmission extends UserNotification
{
	private AMD_UserMessageSender_startTransmission m_cb;

	public StartUserTransmission(Subscriber author, Subscriber receiver, AMD_UserMessageSender_startTransmission __cb) {
		super(author, receiver);
    m_author.setTimestamp();
		m_cb = __cb;
	}

  @Override
    public void Execute(Subscriber itm) 
    {
      if (itm.equals(m_author))
      {
        return;
      }
      itm.getUserSender().begin_startTransmission(m_author.getTicket(), m_author.getUserId());
    }

  @Override
    public void Complete() {
      m_cb.ice_response();
    }

  @Override
    public void Exception(GenericException ex) {
      m_cb.ice_exception(ex);
    }
}

class EndUserTransmission extends UserNotification
{
	private AMD_UserMessageSender_endTransmission m_cb;

	public EndUserTransmission(Subscriber author, Subscriber receiver, AMD_UserMessageSender_endTransmission __cb) {
		super(author, receiver);
		m_cb = __cb;
	}

  @Override
    public void Execute(Subscriber itm) 
    {
      if (itm.equals(m_author))
      {
        return;
      }
      itm.getUserSender().begin_endTransmission(m_author.getTicket(), m_author.getUserId(), m_author.getTimestamp());
    }

  @Override
    public void Complete() {
      m_cb.ice_response();
    }

  @Override
    public void Exception(GenericException ex) {
      m_cb.ice_exception(ex);
    }
}

class SendUserMessage extends UserNotification
{
	private AMD_UserMessageSender_sendBuffer m_cb;
	private Buffer m_buffer;

	public SendUserMessage(Subscriber author, Subscriber receiver, AMD_UserMessageSender_sendBuffer __cb, Buffer msg) {
		super(author, receiver);
		m_cb = __cb;
		m_buffer = msg;
	}

  @Override
    public void Execute(Subscriber itm) 
    {
      if (itm.equals(m_author))
      {
        return;
      }
      itm.getUserSender().begin_sendBuffer(m_buffer, m_author.getUserId());
    }

  @Override
    public void Complete() {
      m_cb.ice_response();
    }

  @Override
    public void Exception(GenericException ex) {
      m_cb.ice_exception(ex);
    }
}
