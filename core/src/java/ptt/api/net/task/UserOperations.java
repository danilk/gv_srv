/**
 * 
 */
package ptt.api.net.task;
import org.apache.log4j.Logger;

import Ice.Current;
import Ice.Identity;
import ptt.api.net.MessageManager;
import ptt.api.manager.IUserKeeper;
import ptt.api.net.slice.*;
import ptt.api.net.exceptions.*;
import ptt.api.spring.DefaultBeanFactory;
import ptt.api.net.slice.exceptions.GenericException;
import ptt.api.net.ITask;
import ptt.api.net.Subscriber;
import ptt.api.net.Subscribers;

/**
 * @author danilk
 *
 */
public abstract class UserOperations<C extends Ice.AMDCallback> extends ITask {

	protected IUserKeeper getKeeper() {
		return (IUserKeeper) DefaultBeanFactory.getInstance().getBean(
				IUserKeeper.class);
	}
	protected C m_cb;
	protected final Logger log = Logger.getLogger(this.getClass());
	
	public UserOperations(C cb)
	{
		m_cb = cb;
	}

	@Override
	public void Exception(GenericException ex) {
		m_cb.ice_exception(ex);
	}

	/* (non-Javadoc)
	 * @see ptt.api.net.ITask#getThreadId()
	 */
	@Override
	public int getThreadId() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	static public ITask registerUser(AMD_UserKeeper_Register cb, UserInfo info)
	{
		return new RegisterUser(cb, info);
	}
	
	static public ITask deleteUser(AMD_UserKeeper_Drop cb, int id)
	{
		return new DropUser(cb, id);
	}
	
	static public ITask updateUser(AMD_UserKeeper_Update cb, UserInfo info, Ticket sid)
	{
		return new UpdateUser(cb, info, sid);
	}

	public static ITask getUserInfo(AMD_UserKeeper_Get __cb, Ticket sid) 
	{
		return new GetUser(__cb, sid);
	}

	public static ITask isLoginUniq(AMD_UserKeeper_IsLoginUniq __cb,
			String login) {
		return new CheckLogin(__cb, login);
	}

	public static ITask getUserInfo(AMD_UserKeeper_GetUser cb, MessageManager manager, int id) {
		return new GetUserInfo(cb, manager, id);
	}

	public static ITask removeFromContacts(AMD_UserKeeper_RemoveFromList __cb,
			Ticket sid, int id) {
		return new RemoveContact(__cb, sid, id);
	}

	public static ITask addToContacts(AMD_UserKeeper_AddToList __cb,
      MessageManager manager, Ticket sid, int id) {
		return new AddContact(__cb, manager, sid, id);
	}

	public static ITask getContacts(AMD_UserKeeper_GetContactList __cb,
			MessageManager manager, Ticket sid) {
		return new GetContacts(__cb, manager, sid);
	}

	public static ITask searchUser(AMD_UserKeeper_GetByWord __cb, String info) {
		return new SearchUser(__cb, info);
	}

	public static ITask startTransmission(
			AMD_UserMessageSender_startTransmission __cb, Subscriber subs,
			Subscriber receiver) {
		return new StartUserTransmission(subs, receiver, __cb);
	}

	public static ITask endTransmission(
			AMD_UserMessageSender_endTransmission __cb, Subscriber subs,
			Subscriber receiver) {
		return new EndUserTransmission(subs, receiver, __cb);
	}

	public static ITask sendMessage(AMD_UserMessageSender_sendBuffer __cb,
			Buffer msg, Subscriber subs, Subscriber receiver) {
		return new SendUserMessage(subs, receiver, __cb, msg);
	}

	public static ITask notifyUserUpdate(UserInfo info,
			AMD_UserMessageSender_userUpdated cb, Subscribers subscribers) {
		return new UserUpdateNotification(subscribers, info, cb);
	}
  
	public static ITask notifyUserAdded(UserInfo info, Subscriber receiver) {
		return new UserAddedNotification(receiver, receiver, info);
	}
  public static ITask muteUser(AMD_UserKeeper_Mute cb,
			MessageManager manager, int id, Ticket sid, Current current) {
		return new MuteUser(cb, manager, id, sid, current);
	}

  public static ITask unMuteUser(AMD_UserKeeper_UnMute cb,
			MessageManager manager, int id, Ticket sid, Current current) {
		return new UnMuteUser(cb, manager, id, sid, current);
	}
  
  public static ITask subscribe(AMD_SubscribeKeeper_Subscribe cb,
			MessageManager manager,Current current, Ticket sid, Identity channel, Identity user) {
		return new SubscribeUser(cb, manager, current, sid, channel, user);
	}
  
  public static ITask unsubscribe(AMD_SubscribeKeeper_Unsubscribe cb,
			MessageManager manager, Ticket sid) {
		return new UnsubscribeUser(cb, manager, sid);
	}
}

class RegisterUser extends UserOperations<AMD_UserKeeper_Register>
{
	private UserInfo m_info;
	public RegisterUser(AMD_UserKeeper_Register cb, UserInfo info) {
		super(cb);
		m_info = info;
	}
	@Override
	public void Complete() {
		m_cb.ice_response();
	}
	@Override
	public void Notify() throws GenericException {
		log.debug("Registering " + m_info.login + " user");
		getKeeper().Create(m_info);
	}
}

class DropUser extends UserOperations<AMD_UserKeeper_Drop>
{
	int m_id; 
	public DropUser(AMD_UserKeeper_Drop cb, int id) {
		super(cb);
		m_id = id;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();
	}

	@Override
	public void Notify() throws GenericException {
		log.debug("Drop info for  " + m_id + " user");
		getKeeper().Remove(m_id);		
	}
}

class UpdateUser extends UserOperations<AMD_UserKeeper_Update>
{
	private UserInfo m_info;
	private Ticket m_sid;

	public UpdateUser(AMD_UserKeeper_Update cb, UserInfo info, Ticket sid) {
		super(cb);
		m_info = info;
		m_sid = sid;
	}

	@Override
	public void Complete() {
		m_cb.ice_response();		
	}

	@Override
	public void Notify() throws GenericException 
	{
		log.debug("Update info for  " + m_sid.sid + " user");
		getKeeper().Change(m_info, m_sid);
	}	
}

class GetUser extends UserOperations<AMD_UserKeeper_Get>
{
	private UserInfo m_info;
	private Ticket m_sid;
	
	public GetUser(AMD_UserKeeper_Get cb, Ticket sid) {
		super(cb);
		m_sid = sid;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_info);
	}

	@Override
	public void Notify() throws GenericException {
		log.debug("Get info for  " + m_sid.sid + " user");
		m_info = getKeeper().Get(m_sid);
	}
	
}
