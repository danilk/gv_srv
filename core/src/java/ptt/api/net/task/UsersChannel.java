package ptt.api.net.task;

import java.util.List;

import Ice.Current;
import ptt.api.net.MessageManager;
import ptt.api.net.task.ChannelOperations;
import ptt.api.net.slice.AMD_ChannelKeeper_GetUsers;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.exceptions.GenericException;

public class UsersChannel extends ChannelOperations<AMD_ChannelKeeper_GetUsers> {

	private int m_id;
	private List<UserInfo> m_list;
	public UsersChannel(AMD_ChannelKeeper_GetUsers cb, MessageManager manager, int channelId, Current current) {
		super(cb, manager);
		m_id = channelId;
	}

	@Override
	public void Complete() {
		m_cb.ice_response(m_list.toArray(new UserInfo[m_list.size()]));
	}

	@Override
	public void Notify() throws GenericException {
		m_list = getKeeper().GetUsers(m_id);
	}
}
