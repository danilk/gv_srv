package ptt.api.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author Alexey Stolbovskikh
 * 
 */

/**
 * This is class that initializes spring context and returns bean by class. This
 */
public class DefaultBeanFactory {
	private final static DefaultBeanFactory instance = new DefaultBeanFactory();

	private final static ApplicationContext ctx = new ClassPathXmlApplicationContext(
			"config/spring/spring-config.xml");

	private DefaultBeanFactory() {
		super();
	}

	/**
	 * Returns class instance
	 * 
	 * @return DefaultBeanFactory
	 */
	public static DefaultBeanFactory getInstance() {
		return instance;
	}

	/**
	 * Returns spring bean by class
	 * 
	 * @param Class
	 *            clazz - bean class
	 * @return Object
	 * @throws IllegalArgumentException
	 *             - If bean for class is not found or spring context contains
	 *             more than one bean for class then throws exception
	 */
	public Object getBean(Class<?> clazz) throws IllegalArgumentException {
		String[] names = ctx.getBeanNamesForType(clazz);

		if (names.length != 1) {
			throw new IllegalArgumentException(
					"Bean not is not found or class has more that one referenses");
		}
		return ctx.getBean(names[0]);
	}
}
