#ifndef CHANNEL_API_ICE
#define CHANNEL_API_ICE
#include "TicketKeeper.ice"
//"
#include "UserKeeper.ice"
//"

module ptt
{
    module api
    {
        module net
        {
            module slice
            {
                class ChannelData
                {
                	int channelId; 
                    string name;
                    string displayName;
                    int subscribersCnt;
                    int onlineCnt;
                    string description;
                    string password;
                    int ownerId;
                    // TODO make it bit masks
                    int channelState;
                    int typeOfBroadcast;
                    int methodOfCommunication;
                    bool authorized;                    
                    bool blocked;
                    bool isOwner;
                    bool canRead;
                    bool canWrite;
                };
                
                sequence<ChannelData> ChannelSeq;
                ["amd"] interface ChannelKeeper
                {
                	idempotent ChannelSeq GetAll(Ticket sid);
                	idempotent ChannelSeq GetByWordAndUser(string info, Ticket sid);
                	idempotent ChannelSeq GetByWord(string info, Ticket sid);                	
                  	idempotent ChannelData Get(int channelId, Ticket sid);
                  	UserSeq GetUsers(int channelId);
                	idempotent bool IsNameUniq(string name);                	
                	idempotent bool Authorize(int id, string password) throws exceptions::ChannelNotExistsException;
                	int Create(ChannelData info, Ticket sid) throws exceptions::UserNotExistsException, exceptions::BadChannelDataException, exceptions::ChannelNameExistsException;
                	idempotent void Drop(int id, Ticket sid) throws exceptions::AccessDeniedException, exceptions::ChannelNotExistsException;
                	idempotent void Update(ChannelData info, Ticket sid) throws exceptions::UserNotExistsException, exceptions::AccessDeniedException, exceptions::ChannelNotExistsException, exceptions::BadChannelDataException;
                	void Join(int id, Ticket sid) throws exceptions::AccessDeniedException, exceptions::ChannelNotExistsException;
                	idempotent void Leave(int id, Ticket sid) throws exceptions::ChannelNotExistsException;
                  idempotent void Mute(int channelId, Ticket sid) throws exceptions::ChannelNotExistsException;
                  idempotent void UnMute(int channelId, Ticket sid) throws exceptions::ChannelNotExistsException;
                };
            };
        };
    };
};
#endif
