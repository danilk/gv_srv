#ifndef SERVER_API_ICE
#define SERVER_API_ICE

#include "UserKeeper.ice"
#include "TicketKeeper.ice"
#include "ChanelKeeper.ice"
#include "SubscribeKeeper.ice"
//"


module ptt
{
    module api
    {
        module net
        {
            module slice
            {
                interface KeeperFactory {
                    TicketKeeper*    		GetTicket();
                    UserKeeper*      		GetUser();
                    ChannelKeeper*    		GetChanel();
                    SubscribeKeeper* 		GetSubscribe();
                    ChannelMessageSender*	GetChannelSender();
                    UserMessageSender*		GetUserSender();                    
                };

            };
        };
    };
};
#endif
