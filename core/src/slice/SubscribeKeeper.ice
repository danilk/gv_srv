#ifndef SUBSCRIBE_API_ICE
#define SUBSCRIBE_API_ICE

#include <Ice/Identity.ice>
#include "TicketKeeper.ice"
#include "ChanelKeeper.ice"
#include "UserKeeper.ice"
//"

module ptt
{
    module api
    {
        module net
        {
            module slice
            {
            	enum PocketType
            	{
            		First,
            		Continues,
            		Last
            	};
                sequence<byte> ByteSeq;
                class Buffer
                {
                    Ticket sid;
                    int seqNum;
                    ByteSeq data;
                    int size;
                    string userName;
                    int userId;
                    string timestamp;
                    bool isHistorical;
                };
                
                class Message
                {
                    string body;
                };

                interface SubscribeKeeper
                {
                    ["amd"] void Subscribe(Ice::Identity channelSender, Ice::Identity userSender,Ticket sid);
                    ["amd"] idempotent void Unsubscribe(Ice::Identity ident, Ticket sid);
                    string getTimestamp();
                };

                ["amd"] interface UserMessageSender
                {
                  //TODO add exception here
                  	void startTransmission(Ticket sid, int userId);
                   	void endTransmission(Ticket sid, int userId, string timestamp);                	
                    void sendBuffer(Buffer msg, int userId);
                    idempotent void userUpdated(UserInfo info);
                };
                
                ["amd"] interface ChannelMessageSender
                {
                  //TODO add exception here
                  	void startTransmission(Ticket sid, int channelId);
                   	void endTransmission(Ticket sid, int channelId, int userId, string timestamp);                	
                    void sendBuffer(Buffer msg, int channelId);
                    idempotent void channelUpdated(ChannelData channel);
                };
                
                interface MessageSender
                {
                	UserMessageSender* 		getUserSender();
                	ChannelMessageSender* 	getChannelSender();
                };
                
            };
        };
    };
};
#endif
