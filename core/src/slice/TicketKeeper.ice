#ifndef TICKET_API_ICE
#define TICKET_API_ICE

module ptt
{
    module api
    {
        module net
        {
            module slice
            {
                module exceptions
                {
                  exception GenericException
                  {
                    string message;
                  };

                  exception LoginExistsException extends GenericException
                  {
                  };
                  exception UserNotExistsException extends GenericException
                  {
                  };
                  exception BadUserInfoException extends GenericException
                  {
                  };

                  exception ChannelNameExistsException extends GenericException
                  {
                  };

                  exception ChannelNotExistsException extends GenericException
                  {
                  };

                  exception BadChannelDataException extends GenericException
                  {
                  };

                  exception AccessDeniedException extends GenericException
                  {
                  };

                };

                class Credential
                {
                    string login;
                    string password;
                };

                class Signature
                {
                    string hash;
                };
                class Ticket
                {
                    string sid;
                };

                ["amd"] interface TicketKeeper {
                     Ticket Get(Credential credentialInfo);
                     idempotent void Drop(Ticket sid);
                };
            };
        };
    };
};
#endif
