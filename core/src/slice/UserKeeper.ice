#ifndef USER_API_ICE
#define USER_API_ICE
#include "TicketKeeper.ice"
//"

module ptt
{
    module api
    {
        module net
        {
            module slice
            {
                class UserInfo
                {
                    int id;
                    string login;
                    string password;
                    string email;
                    int userState;
                    bool isOnline;
                    bool userAdded;
                };
                sequence<UserInfo> UserSeq;

                ["amd"] interface UserKeeper
                {
                    idempotent void Update(Ticket sid, UserInfo info) throws exceptions::BadUserInfoException, exceptions::UserNotExistsException;
                    idempotent void Drop(int id) throws exceptions::UserNotExistsException;
                    void Register(UserInfo info) throws exceptions::LoginExistsException,exceptions::BadUserInfoException;
                    UserInfo Get(Ticket sid) throws exceptions::UserNotExistsException;
                    UserInfo GetUser(int id) throws exceptions::UserNotExistsException;
                    UserSeq GetByWord(string info);
                    UserSeq GetContactList(Ticket sid);
                    void AddToList(Ticket sid, int id);
                    void RemoveFromList(Ticket sid, int id);
                    idempotent bool IsLoginUniq(string login);
                    idempotent void Mute(int userId, Ticket sid) throws exceptions::UserNotExistsException;
                    idempotent void UnMute(int userId, Ticket sid) throws exceptions::UserNotExistsException;
                };
            };
        };
    };
};
#endif
