package ptt.api.net;

import java.util.concurrent.ThreadPoolExecutor;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import ptt.api.net.utils.Client;

import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.GenericException;
import ptt.api.net.slice.exceptions.UserNotExistsException;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public abstract class BaseTest {

  protected KeeperFactoryPrx proxy;
  protected UserKeeperPrx userKeeper;
  protected TicketKeeperPrx ticketKpr;
  protected ChannelKeeperPrx channelKpr; 
  protected ChannelMessageSenderPrx msgSender;

  protected UserInfo user1;
  protected Ticket userSid1 = new Ticket();
  protected UserInfo user2;
  protected Ticket userSid2 = new Ticket();
  protected ChannelData channelData = new ChannelData();

	protected Client client = new Client();
//
	@Before
	public void init() {
//		this.client = clienteg;
		 System.out.println("Initialize start");
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				client.start();
			}
		});
		thread.start();

		while (!client.isRun()) {
			try {
				Thread.currentThread();
				Thread.sleep(1);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}

    proxy = client.getFactory();
    userKeeper = proxy.GetUser();
    ticketKpr = proxy.GetTicket();
    channelKpr = proxy.GetChanel();
    msgSender = proxy.GetChannelSender();

    user1 = initUser(new UserInfo(0, "test_channels", "2", "channel_test@mail.ru",1, true), userSid1);
    user2 = initUser(new UserInfo(0, "test_channels2", "2", "channel_test2@mail.ru",1, true), userSid2);
    System.out.println("Initialize end");
	}

	public final void waitTasksFinish(ThreadPoolExecutor threadPoolExecutor,
			long endCount, long liveTime) {
		long startTime = System.currentTimeMillis();
		boolean timeOut = false;
		while (threadPoolExecutor.getCompletedTaskCount() < endCount) {
			try {
				Thread.sleep(100);
				if (startTime + liveTime < System.currentTimeMillis()) {
					timeOut = true;
					break;
				}
			} catch (InterruptedException e) {
				e.printStackTrace(System.out);
			}
		}
		Assert.assertFalse(timeOut);
	}

	@After
	public void destroy() {

    userKeeper.begin_Drop(user1.id);		
    userKeeper.begin_Drop(user2.id);		
    client.stop();
  }

  private UserInfo initUser(UserInfo info, Ticket sid)
  {

    try
    {
      userKeeper.Register(info);
    }
    catch(GenericException ex) 
    {
      ex.printStackTrace();
    }

    Credential credentialInfo = new Credential();
    credentialInfo.login = info.login;
    credentialInfo.password = info.password;
    Ticket newSid = ticketKpr.Get(credentialInfo);		

    UserInfo infoHolder = null;
	try {
		infoHolder = userKeeper.Get(newSid);
	} catch (UserNotExistsException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    sid.sid = newSid.sid;
    return infoHolder;
  }
}
