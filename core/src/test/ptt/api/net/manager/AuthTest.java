package ptt.api.net.manager;

import org.junit.Assert;
import org.junit.Test;


import ptt.api.net.BaseTest;
import ptt.api.net.slice.Credential;
import ptt.api.net.slice.KeeperFactoryPrx;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.TicketKeeperPrx;
import ptt.api.net.slice.TicketHolder;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public final class AuthTest extends BaseTest {
	
	@Test
	public void test() {
		System.out.println("----------Test get ticket for user----------");
	  KeeperFactoryPrx proxy = client.getFactory();
		
    TicketKeeperPrx ticketKeeper = proxy.GetTicket();

		Credential credential = new Credential("danil", "1");
//		ticketKeeper.beg
		Ticket ticket = ticketKeeper.Get(credential);
//		System.out.println("New ticket" + ticket.value.sid);
		Assert.assertFalse(ticket.sid.isEmpty());
		
		ticketKeeper.Drop(ticket);		
		credential = new Credential("marina", "1");
		Ticket ticket2 = ticketKeeper.Get(credential);
		Assert.assertTrue(ticket2.sid.isEmpty());	
	}
}
