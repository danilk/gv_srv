package ptt.api.net.manager;

import org.junit.Assert;
import org.junit.Test;

import org.junit.After;
import org.junit.Before;

import ptt.api.net.BaseTest;
import ptt.api.net.slice.*;
import ptt.api.net.slice.exceptions.GenericException;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public final class ChannelTest extends BaseTest {

  @Before
  public void createChannel()
  {
		/// testing of channel API
		channelData.name = "test_ch1";
    channelData.typeOfBroadcast = 0;
		channelData.description = "testing purposes";
		boolean result = channelKpr.IsNameUniq(channelData.name);
		Assert.assertTrue(result);		
    try
    {
		  int id  = channelKpr.Create(channelData, userSid1);
      channelData.channelId = id;
    }
    catch(GenericException ex)
    {
      Assert.assertTrue(false);
    }
  }

  @After
  public void dropChannel()
  {
		channelKpr.begin_Drop(channelData.channelId, userSid1);
  }
	@Test
	public void test() {
		System.out.println("----------Test register user----------");
		
		/// testing of channel API
/*	  try
    {  
      boolean result = channelKpr.CanISpeak(channelData.channelId,userSid1);
      Assert.assertTrue(result);

      result = channelKpr.CanISpeak(channelData.channelId,userSid2);
      Assert.assertFalse(result);
    }
    catch(GenericException ex)
    {
      Assert.assertTrue(false);
    }
*/

		ChannelData[] list = channelKpr.GetAll(userSid1);
		Assert.assertTrue(list.length == 1 && list[0].name.equals(channelData.name)
				&& list[0].description.equals(channelData.description));
		list = channelKpr.GetByWordAndUser("test", userSid1);
		
		Assert.assertTrue(list.length == 1 && list[0].name.equals(channelData.name)
				&& list[0].description.equals(channelData.description));

    /// Checking canRead and canWrite
/*		Assert.assertTrue(list[0].canRead);
		Assert.assertTrue(list[0].canWrite);

    channelKpr.begin_Join(list[0].channelId, userSid2);
		list = channelKpr.GetAll(userSid2);

		Assert.assertTrue(list.length == 1);
		Assert.assertFalse(list[0].canRead);
		Assert.assertFalse(list[0].canWrite);
    channelKpr.begin_Leave(list[0].channelId, userSid2);
*/
    ///

		list = channelKpr.GetByWord("test");
		Assert.assertTrue(list.length == 1 && list[0].name.equals(channelData.name)
				&& list[0].description.equals(channelData.description));

/*    try
    {
		  boolean result = channelKpr.IsOwner(list[0].channelId, userSid1);
		  Assert.assertTrue(result);
    }
    catch(GenericException ex)
    {
      Assert.assertTrue(false);
    }
	*/	
		channelData.methodOfCommunication = 3;
		channelData.password = "qwerty";
		channelData.channelId = list[0].channelId;
		
		channelKpr.begin_Update(channelData, userSid1);
		list = channelKpr.GetAll(userSid1);
		Assert.assertTrue(list.length == 1 && list[0].name.equals(channelData.name)
				&& list[0].description.equals(channelData.description)
				&& list[0].methodOfCommunication == channelData.methodOfCommunication
				&& list[0].password.equals(channelData.password)
				&& list[0].subscribersCnt == 1);
		
    try
    {
	  	boolean result = channelKpr.Authorize(channelData.channelId, channelData.password);
		  Assert.assertTrue(result);
    }
    catch(GenericException ex)
    {
      Assert.assertTrue(false);
    }
		
    channelKpr.begin_Join(channelData.channelId, userSid2);
		
		list = channelKpr.GetAll(userSid2);
		Assert.assertTrue(list.length == 1 && list[0].name.equals(channelData.name)
				&& list[0].description.equals(channelData.description)
				&& list[0].methodOfCommunication == channelData.methodOfCommunication
				&& list[0].password.equals(channelData.password)
				&& list[0].subscribersCnt == 2);
		
		channelKpr.begin_Leave(channelData.channelId, userSid2);
		list = channelKpr.GetAll(userSid2);
		Assert.assertTrue(list.length == 0);
		
		list = channelKpr.GetAll(userSid1);
		Assert.assertTrue(list.length == 1 && list[0].name.equals(channelData.name)
				&& list[0].description.equals(channelData.description)
				&& list[0].methodOfCommunication == channelData.methodOfCommunication
				&& list[0].password.equals(channelData.password)
				&& list[0].subscribersCnt == 1);
		
		channelKpr.begin_Drop(channelData.channelId, userSid2);
		list = channelKpr.GetAll(userSid1);

		Assert.assertTrue(list.length == 1 && list[0].name.equals(channelData.name)
				&& list[0].description.equals(channelData.description)
				&& list[0].methodOfCommunication == channelData.methodOfCommunication
				&& list[0].password.equals(channelData.password)
				&& list[0].subscribersCnt == 1);
				
	}
}
