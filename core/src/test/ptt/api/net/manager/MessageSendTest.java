package ptt.api.net.manager;

import org.junit.Assert;
import org.junit.Test;
import java.lang.Thread;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import Ice.Current;

import ptt.api.net.BaseTest;
import ptt.api.net.ChannelMessageSenderI;
import ptt.api.net.MessageManager;
import ptt.api.net.UserMessageSenderI;

import org.junit.After;
import org.junit.Before;

import ptt.api.net.slice.exceptions.GenericException;
import ptt.api.net.slice.AMD_ChannelMessageSender_channelUpdated;
import ptt.api.net.slice.AMD_ChannelMessageSender_endTransmission;
import ptt.api.net.slice.AMD_ChannelMessageSender_sendBuffer;
import ptt.api.net.slice.AMD_ChannelMessageSender_startTransmission;
import ptt.api.net.slice.AMD_UserMessageSender_endTransmission;
import ptt.api.net.slice.AMD_UserMessageSender_sendBuffer;
import ptt.api.net.slice.AMD_UserMessageSender_startTransmission;
import ptt.api.net.slice.AMD_UserMessageSender_userUpdated;
import ptt.api.net.slice.Callback_ChannelMessageSender_endTransmission;
import ptt.api.net.slice.Callback_ChannelMessageSender_sendBuffer;
import ptt.api.net.slice.Callback_ChannelMessageSender_startTransmission;
import ptt.api.net.slice.ChannelMessageSender;
import ptt.api.net.slice.ChannelMessageSenderPrx;
import ptt.api.net.slice.ChannelMessageSenderPrxHelper;
import ptt.api.net.slice.Credential;
import ptt.api.net.slice.KeeperFactoryPrx;
import ptt.api.net.slice.TicketKeeperPrx;
import ptt.api.net.slice.SubscribeKeeperPrx;
import ptt.api.net.slice.MessageSenderPrx;
import ptt.api.net.slice.Buffer;
import ptt.api.net.slice.TicketHolder;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.ChannelData;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.UserMessageSender;
import ptt.api.net.slice.UserMessageSenderPrx;
import ptt.api.net.slice.UserMessageSenderPrxHelper;
import ptt.api.net.slice._ChannelMessageSenderDisp;
import ptt.api.net.slice._UserMessageSenderDisp;
import ptt.api.net.utils.Client;
import ptt.api.net.slice._MessageSenderDisp;
/**
 * 
 * @author Danil Krivopustov
 * 
 */
public final class MessageSendTest extends BaseTest {
  
	@SuppressWarnings("unused")
	private final Logger log = Logger.getLogger(this.getClass());
  public class Callback_ChannelMessageSender_sendBufferI extends Callback_ChannelMessageSender_sendBuffer
  {
    @Override
      public void response()
      {
        System.out.println("Done!!!");
      }
    @Override
      public void exception(Ice.LocalException ex)
      {
        ex.printStackTrace();
      }
  }  

  public class Callback_ChannelMessageSender_startTransmissionI extends Callback_ChannelMessageSender_startTransmission
  {
    @Override
      public void response()
      {
        System.out.println("Started");
      }
    @Override
      public void exception(Ice.LocalException ex)
      {
        ex.printStackTrace();
      }
  }  

  public class Callback_ChannelMessageSender_endTransmissionI extends Callback_ChannelMessageSender_endTransmission
  {
    @Override
      public void response()
      {
        System.out.println("Ended");
      }
    @Override
      public void exception(Ice.LocalException ex)
      {
        ex.printStackTrace();
      }
  }  
  class MessageReceiver extends Thread
  {
		@SuppressWarnings("serial")
		class MessageSenderI extends _MessageSenderDisp
		{
			  private Map<String, Ice.ObjectPrx> m_keepers = new HashMap<String,Ice.ObjectPrx>();
		      private volatile MessageReceiver m_receiver;
		      
		      public MessageSenderI(MessageReceiver rec)
		      {
		    	  m_receiver = rec;
		      }

			  private <I extends Ice.Object, T extends I> Ice.ObjectPrx obtainServant(String name, Class<T> clazz, Ice.Current current)
			  {
			    if (m_keepers.get(name) == null)
			    {

			      Ice.Communicator comm = current.adapter.getCommunicator();

			      try
			      {
			        I servant;
			        try
			        {
			          servant = clazz.getConstructor(MessageManager.class).newInstance(m_receiver);
			        }
			        catch(InvocationTargetException e)
			        {
			          servant = clazz.newInstance();
			        }
			        catch(NoSuchMethodException e)
			        {
			          servant = clazz.newInstance();
			        }

					Ice.ObjectPrx obj = current.adapter.add(servant, comm.stringToIdentity(name));
			        m_keepers.put(name, obj);
			      }
			      catch(InstantiationException e)
			      {

			      }
			      catch(IllegalAccessException e)
			      {
			      }

			    }
			    return m_keepers.get(name);
			  }

			@Override
			public ChannelMessageSenderPrx getChannelSender(Ice.Current current) {
				Ice.ObjectPrx obj = this
						.<ChannelMessageSender, ChannelMessageSenderI> obtainServant(
								"ChannelMessageSender",
								ChannelMessageSenderI.class, current);
		          System.out.print("Reg channel");

				return ChannelMessageSenderPrxHelper.uncheckedCast(obj);
			}

			@Override
			public UserMessageSenderPrx getUserSender(Ice.Current current) {
				Ice.ObjectPrx obj = this
						.<UserMessageSender, UserMessageSenderI> obtainServant(
								"UserMessageSender", UserMessageSenderI.class,
								current);
				System.out.print("Reg user");
				return UserMessageSenderPrxHelper.uncheckedCast(obj);
			}

		}

    @SuppressWarnings("serial")
    class ChannelMessageSenderI extends _ChannelMessageSenderDisp
    {
      private volatile MessageReceiver m_mon;
      ChannelMessageSenderI(MessageReceiver mon)
      {
        m_mon = mon;
      }

      @Override
        public void sendBuffer_async(AMD_ChannelMessageSender_sendBuffer cb, Buffer msg,  int channel,  Ice.Current current)
        {
          cb.ice_response();

          System.out.print("Received!!!");
          m_mon.Responded();
        }

	@Override
	public void startTransmission_async(
			AMD_ChannelMessageSender_startTransmission __cb, Ticket sid,  int channel, 
			Current __current) {
          __cb.ice_response();

          System.out.print("Ring the bell...");
		
	}

	@Override
	public void endTransmission_async(AMD_ChannelMessageSender_endTransmission __cb,
			Ticket sid, int channel, Current __current) {
          __cb.ice_response();

          System.out.print("Close the door...");
	}

	@Override
	public void channelUpdated_async(AMD_ChannelMessageSender_channelUpdated __cb,
			ChannelData channelData, Current __current) {
		// TODO Auto-generated method stub
		
	}
    }
    @SuppressWarnings("serial")
    class UserMessageSenderI extends _UserMessageSenderDisp
    {

		@Override
		public void startTransmission_async(
				AMD_UserMessageSender_startTransmission __cb, Ticket sid,
				int userId, Current __current) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void endTransmission_async(
				AMD_UserMessageSender_endTransmission __cb, Ticket sid,
				int userId, Current __current) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void sendBuffer_async(AMD_UserMessageSender_sendBuffer __cb,
				Buffer msg, int userId, Current __current) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void userUpdated_async(AMD_UserMessageSender_userUpdated __cb,
				UserInfo info, Current __current) {
			// TODO Auto-generated method stub
			
		}
    }
    private SubscribeKeeperPrx m_proxy;
    private Ticket m_ticket;
    private boolean m_done = false;
	  private volatile Ice.Identity m_ident;
    private MessageSenderI m_receiver;
    private int cnt = 0;

    public
      MessageReceiver(Client client, SubscribeKeeperPrx proxy, Ticket ticket)
      {
        m_proxy = proxy;
        m_ticket = ticket;
        m_receiver = new MessageSenderI(this);
        m_ident = client.AddServant(m_receiver);

        proxy.ice_getConnection().setAdapter(client.Adapter());
        m_proxy.Subscribe(m_ident, m_ticket);
      }
    
    public void setCounter(int cnt)
    {
    	this.cnt = cnt;
    }

    public Ice.Identity Ident()
    {
      return m_ident;
    }

    public synchronized 
      void run()
      {
        try
        {
          wait();
        }
        catch(java.lang.InterruptedException ex)
        {

        }
      }

    public synchronized
      void Responded()
      {
        System.out.print("Responded");
        if (--cnt == 0)
        {
        	notify();
        }
      }

    public synchronized
      void shutdown()
      {
        if(m_done)
        {
          return;
        }

        m_done = true;
//        m_proxy.Unsubscribe(m_ident, m_ticket);
        notify();
      }
  }

	@Test
	public void test() {
		System.out.println("----------Test get ticket for user----------");

    /*try
    {
      Thread.sleep(2000);
    }
    catch(java.lang.InterruptedException ex)
    {
    }*/

    Buffer buffer = new Buffer();
    buffer.sid = userSid1;
    buffer.size = 1000;

    for(int i = 1; i<=10;++i)
    {
    	msgSender.begin_startTransmission(userSid1, channelData.channelId, new Callback_ChannelMessageSender_startTransmissionI());
    }	
		
    for(int i = 1; i<=10;++i)
    {
    	msgSender.begin_endTransmission(userSid1, channelData.channelId, new Callback_ChannelMessageSender_endTransmissionI());
    }	
    for(int i = 1; i<=10;++i)
    {
    	msgSender.begin_sendBuffer(buffer, channelData.channelId, new Callback_ChannelMessageSender_sendBufferI());
    }	

/*
    /// Checking canRead and canWrite

    // Open
		ChannelData[] list = channelKpr.GetAll(userSid2);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertTrue(list[0].canWrite);

    //owner
		list = channelKpr.GetAll(userSid1);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertTrue(list[0].canWrite);

    // Open with auth
		channelData.typeOfBroadcast = 2;
		channelData.password = "qwerty";
		
		channelKpr.begin_Update(channelData, userSid1);

		list = channelKpr.GetAll(userSid2);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertFalse(list[0].canWrite);


    //owner
		list = channelKpr.GetAll(userSid1);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertTrue(list[0].canWrite);
    // Auth
		channelData.typeOfBroadcast = 3;
		
		channelKpr.begin_Update(channelData, userSid1);

		list = channelKpr.GetAll(userSid2);

		Assert.assertTrue(list.length == 1);
		Assert.assertFalse(list[0].canRead);
		Assert.assertFalse(list[0].canWrite);

    //owner
		list = channelKpr.GetAll(userSid1);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertTrue(list[0].canWrite);
    // Closed
		channelData.typeOfBroadcast = 4;
		
		channelKpr.begin_Update(channelData, userSid1);

		list = channelKpr.GetAll(userSid2);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertTrue(list[0].canWrite);

    //owner
		list = channelKpr.GetAll(userSid1);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertTrue(list[0].canWrite);
    // Close with auth
		channelData.typeOfBroadcast = 5;
		
		channelKpr.begin_Update(channelData, userSid1);

		list = channelKpr.GetAll(userSid2);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertFalse(list[0].canWrite);

    //owner
		list = channelKpr.GetAll(userSid1);

		Assert.assertTrue(list.length == 1);
		Assert.assertTrue(list[0].canRead);
		Assert.assertTrue(list[0].canWrite);
    ///
*/
	}

  @Before
  public void createChannel()
  {

    receiverThread1 = new MessageReceiver(client, proxy.GetSubscribe(), userSid1);
    receiverThread2 = new MessageReceiver(client, proxy.GetSubscribe(), userSid2);
    //TODO fix me!
    receiverThread2.setCounter(10);
    receiverThread2.start();
    
    receiverThread1.setCounter(10);
    receiverThread1.start();

		/// testing of channel API
		channelData.name = "test_ch1";
    channelData.typeOfBroadcast = 0;
		channelData.description = "testing purposes";
		boolean result = channelKpr.IsNameUniq(channelData.name);
		Assert.assertTrue(result);		
    try
    {
		  int id  = channelKpr.Create(channelData, userSid1);
      channelData.channelId = id;
    }
    catch(GenericException ex)
    {
      Assert.assertTrue(false);
    }

    channelKpr.begin_Join(channelData.channelId, userSid2);
  }

  @After
  public void dropChannel()
  {

    try
    {
      receiverThread1.join(5000);
      receiverThread2.join(5000);
    }
    catch(java.lang.InterruptedException ex)
    {
    }

    receiverThread1.shutdown();
    receiverThread2.shutdown();
		channelKpr.begin_Leave(channelData.channelId, userSid2);
		channelKpr.begin_Drop(channelData.channelId, userSid1);
  }
  private MessageReceiver receiverThread1;
  private MessageReceiver receiverThread2;

}
