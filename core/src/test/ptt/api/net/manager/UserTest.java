package ptt.api.net.manager;

import org.junit.Assert;
import org.junit.Test;


import ptt.api.net.BaseTest;
import ptt.api.net.slice.Credential;
import ptt.api.net.slice.KeeperFactoryPrx;
import ptt.api.net.slice.Ticket;
import ptt.api.net.slice.TicketHolder;
import ptt.api.net.slice.TicketKeeperPrx;
import ptt.api.net.slice.UserInfoHolder;
import ptt.api.net.slice.UserKeeperPrx;
import ptt.api.net.slice.UserInfo;
import ptt.api.net.slice.exceptions.UserNotExistsException;
import ptt.api.net.slice.exceptions.BadUserInfoException;
import ptt.api.net.slice.exceptions.GenericException;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public final class UserTest extends BaseTest {
	
	@Test
	public void test() {
		System.out.println("----------Test register user----------");
		KeeperFactoryPrx proxy = client.getFactory();
		
	    UserKeeperPrx userKeeper = proxy.GetUser();
	    
	    TicketKeeperPrx ticketKpr = proxy.GetTicket();

		UserInfo info = new UserInfo(0, "danilka", "2", "lavei@mail.ru",1, true);
    try
    {
		  userKeeper.Register(info);
    }
    catch(GenericException ex)
    {
		  Assert.assertTrue(false);
    }

		
		System.out.println("uniq");
		boolean result = userKeeper.IsLoginUniq(info.login);
		
		Assert.assertFalse(result);
		
		Credential credentialInfo = new Credential();
		credentialInfo.login = info.login;
		credentialInfo.password = info.password;
		Ticket newSid = ticketKpr.Get(credentialInfo);		
		System.out.println("Authorize");
		//Assert.assertTrue(result);

		UserInfo infoHolder = null;
    try
    {
    	infoHolder = userKeeper.Get(newSid);
		  System.out.println("Get info");
		  Assert.assertTrue(infoHolder != null);
		  Assert.assertTrue(infoHolder.login.equals(info.login));
    }
    catch(UserNotExistsException ex)
    {
      Assert.assertFalse(false);
    }

		info.email = "danilk@gmail.com";
		userKeeper.begin_Update(newSid, info);
		
    try
    {
    	infoHolder = userKeeper.Get(newSid);
		  System.out.println("Update info");
		  Assert.assertTrue(result);
		  Assert.assertTrue(infoHolder.email.equals(info.email));
    }
    catch(UserNotExistsException ex)
    {
      Assert.assertFalse(false);
    }

		userKeeper.begin_Drop(infoHolder.id);
	
    try
    {  
    	infoHolder = userKeeper.Get(newSid);
    }
    catch(UserNotExistsException ex)
    {
      // do nothing it's ok
    }
	}
}
