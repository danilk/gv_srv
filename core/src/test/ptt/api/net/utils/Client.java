package ptt.api.net.utils;

import ptt.api.net.slice.KeeperFactoryPrx;
import ptt.api.net.slice.KeeperFactoryPrxHelper;
import ptt.api.net.slice._MessageSenderDisp;
import ptt.api.net.slice.Ticket;

import ptt.api.net.slice.Buffer;

import java.io.File;
import java.net.URL;

import Ice.Application;
import Ice.Current;
import Ice.Identity;
import Ice.ObjectAdapter;
import Ice.ObjectPrx;

/**
 * 
 * @author Danil Krivopustov
 * 
 */
public class Client extends Application implements ClientI {

	private volatile KeeperFactoryPrx proxy;

	private volatile ObjectAdapter adapter;

	private volatile boolean run;
/*
    @SuppressWarnings("serial")
    class MessageSenderI extends _MessageSenderDisp
    {


	@Override
	public void startTransmission_async(
			AMD_MessageSender_startTransmission __cb, Ticket sid,
			int channelId, Current __current) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endTransmission_async(AMD_MessageSender_endTransmission __cb,
			Ticket sid, int channelId, Current __current) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendBuffer_async(AMD_MessageSender_sendBuffer __cb, Buffer msg,
			int channelId, Current __current) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void channelUpdated_async(AMD_MessageSender_channelUpdated __cb,
			int channelId, Current __current) {
		// TODO Auto-generated method stub
		
	}
    }*/
	@Override
	public int run(String[] arg0) {
		System.out.println("start running");
		
    ObjectPrx base = communicator().propertyToProxy("KeeperFactory.Proxy")
				.ice_twoway();

		proxy = KeeperFactoryPrxHelper.checkedCast(base);

		adapter = communicator().createObjectAdapter("");

    Identity ident = Client.getNext();
  //  adapter.add(new MessageSenderI(), ident);
    adapter.activate();
		System.out.println("added adapteraa");
    proxy.ice_getConnection().setAdapter(adapter);

//		proxy.GetSubscribe().Subscribe(ident, new Ticket("qqqq"));
		if (proxy == null) {
			System.out.println("Invalid proxy");
			return 1;
		} else {
			System.out.println("Proxy has been created");
		}
		run = true;
		System.out.println("waiting");
  	communicator().waitForShutdown();
		//proxy.GetSubscribe().Unsubscribe(ident);
		System.out.println("Destroy communicator");
		return 0;
	}

	public KeeperFactoryPrx getFactory() {
		return proxy;
	}

	public static synchronized Identity getNext() {
		Identity result = new Identity();
		result.name = Ice.Util.generateUUID();
		result.category = "";
		return result;
	}

  public synchronized ObjectAdapter Adapter()
  {
      return adapter;
  }

  public synchronized 
    Ice.Identity AddServant(Ice.Object servant)
  {
    Ice.Identity ident = Client.getNext();
    adapter.add(servant, ident);
    adapter.activate();
    return ident;
  }

	public void start() {
		URL url = this.getClass().getProtectionDomain().getCodeSource()
				.getLocation();
//		System.out.println(this.getClass().getPackage().getName().replace(".",
//				String.valueOf(File.separatorChar)));
		String confPath = url.getPath()
				+ this.getClass().getPackage().getName().replace(".",
						String.valueOf(File.separatorChar))
				+ File.separatorChar + "client.properties";
		main("Client", new String[0], confPath);
	}

	public void stop() {
			System.out.println("Somebody has stopped me!");
		run = false;
		communicator().shutdown();
	}

	public boolean isRun() {
		return run;
	}
}
