package ptt.api.net.utils;


/**
 * 
 * @author Danil Krivopustov
 * 
 */
public interface ClientI {

	public void start();
	public void stop();
	public boolean isRun();
}
