package gt.api.startup;

import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * 
 * @author Alexey Stolbovskikh
 * 
 */

final class DefaultLogger {

	static class MyFormatter extends Formatter {
		@Override
		public String format(LogRecord record) {
			StringBuilder builder = new StringBuilder();
//			builder.append("[");
//			builder.append(record.getLevel().getName());
//			builder.append("] ");
			builder.append(record.getMessage());
			builder.append("\n");
			return builder.toString();
		}
	}

	private final static Logger instance = LogManager.getLogManager()
			.getLogger("");

	static {
		ConsoleHandler ch = new ConsoleHandler();
		ch.setFormatter(new MyFormatter());
		Handler[] handlers = instance.getHandlers();
		for (Handler item : handlers) {
			instance.removeHandler(item);
		}
		instance.addHandler(ch);
	}

	private DefaultLogger() {
		super();
	}

	public static Logger getInstance() {
		return instance;
	}
}
