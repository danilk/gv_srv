package ptt.api.startup;

import ptt.api.net.KeeperFactoryImpl;
import ptt.api.spring.DefaultBeanFactory;
import org.apache.log4j.Logger;
import Ice.ObjectAdapter;
/**
 * 
 * @author Danil Krivopustov 
 * 
 */
public class FactoryServiceI implements IceBox.Service
{

  private Logger logger = Logger.getLogger(this.getClass());
  private Ice.ObjectAdapter m_adapter;
  private KeeperFactoryImpl m_manager;

	public void start(String name, Ice.Communicator communicator, String args[]) 
  {
    logger.info("Creating primary adapter...");
		// !!! IMPORTANT logger is initialized
		DefaultBeanFactory.getInstance();

    m_adapter  = communicator.createObjectAdapter(name);
    m_manager = new KeeperFactoryImpl();

    m_adapter.add(m_manager, communicator.stringToIdentity("KeeperFactory"));
    m_adapter.activate();

    logger.info("Adapter initialization has been completed.");
	}

	public void stop() 
  {
      logger.info("Trying to shutdown sevices...");
      m_manager.destroy();
      m_adapter.destroy();
      logger.info("All services have been stopped.");
	}

}
